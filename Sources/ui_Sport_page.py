from PySide6.QtCore import (QRect, QSize)
from PySide6.QtGui import (QPixmap)
from PySide6.QtWidgets import (QTableWidget, QLabel, QPushButton, QWidget, QMainWindow, QTableWidgetItem)

from random import randint
import os

from ui_Recette import lire_json,  adjust_widget_properties, factor
from tracking_sport import Exercise_Tracker

class Sport(QMainWindow):
    def __init__(self, echangeur):
        """Initialise l'instance Sport et charge l'interface utilisateur.

        Args:
            echangeur : C'est l'enplacement où l'instance Sport va se charger. (Dans ce cas c'est le QStackedWidget généré par Ui_MainWindow)
        """
        super().__init__()
        self.echangeur = echangeur # C'est la variable qui permet de passer d'une fenetre à l'autre, comme un echangeur d'autoroute. Elle renvoie au Qwidget "parent" qui stocke tous les autres Qwidgets (toutes les autres pages).
        self.initUI() #Initialise l'affichage des boutons, label, etc...
    
    def initUI(self):
        """
        Initialise l'instance et charge l'interface graphique (boutons, textes, etc.)
        """
        self.resize(1920, 1080)
        
        # Crée le wiget conteneur 
        self.widget_mon_programme = QWidget(self)
        self.widget_mon_programme.setGeometry(QRect(330, 100, 661, 781))
        self.widget_mon_programme.setStyleSheet(u"color: rgb(0, 0, 0);border-radius: 30px;width: 200px; height: 100px; background-color:rgb(37, 38, 81);")
        font = self.widget_mon_programme.font()
        font.setPointSize(30)
        self.widget_mon_programme.setFont(font)
        self._titre_mon_programme = QLabel(self.widget_mon_programme)
        self._titre_mon_programme.setGeometry(QRect(100, 10, 471, 91))
        self._titre_mon_programme.setStyleSheet(u"color: rgb(255, 255, 255);font:  \"Segoe UI\";")
        font = self._titre_mon_programme.font()
        font.setPointSize(40)
        self._titre_mon_programme.setFont(font)
        
        #Créer les Boutons / Labels
        self.pushButton_j1 = QPushButton(self.widget_mon_programme)
        self.pushButton_j1.setGeometry(QRect(40, 160, 581, 81))
        self.pushButton_j1.setStyleSheet(u"QPushButton {color: rgb(255, 255, 255);border-radius: 20px;background-color: rgb(5, 5, 56);border: 3px solid white;}""QPushButton:hover {background:  rgb(55, 55, 110);}")
        self.pushButton_j1.setIconSize(QSize(90, 90))
        font = self.pushButton_j1.font()
        font.setPointSize(30)
        self.pushButton_j1.setFont(font)
        self.pushButton_j2 = QPushButton(self.widget_mon_programme)
        self.pushButton_j2.setGeometry(QRect(40, 250, 581, 81))
        self.pushButton_j2.setStyleSheet(u"QPushButton {color: rgb(255, 255, 255);border-radius: 20px;background-color: rgb(5, 5, 56);border: 3px solid white;}""QPushButton:hover {background:  rgb(55, 55, 110);}")
        self.pushButton_j2.setIconSize(QSize(90, 90))
        font = self.pushButton_j2.font()
        font.setPointSize(30)
        self.pushButton_j2.setFont(font)
        self.pushButton_j3 = QPushButton(self.widget_mon_programme)
        self.pushButton_j3.setGeometry(QRect(40, 340, 581, 81))
        self.pushButton_j3.setStyleSheet(u"QPushButton {color: rgb(255, 255, 255);border-radius: 20px;background-color: rgb(5, 5, 56);border: 3px solid white;}""QPushButton:hover {background:  rgb(55, 55, 110);}")
        self.pushButton_j3.setIconSize(QSize(90, 90))
        font = self.pushButton_j3.font()
        font.setPointSize(30)
        self.pushButton_j3.setFont(font)
        self.pushButton_j4 = QPushButton(self.widget_mon_programme)
        self.pushButton_j4.setGeometry(QRect(40, 430, 581, 81))
        self.pushButton_j4.setStyleSheet(u"QPushButton {color: rgb(255, 255, 255);border-radius: 20px;background-color: rgb(5, 5, 56);border: 3px solid white;}""QPushButton:hover {background:  rgb(55, 55, 110);}")
        self.pushButton_j4.setIconSize(QSize(90, 90))
        font = self.pushButton_j4.font()
        font.setPointSize(30)
        self.pushButton_j4.setFont(font)
        self.pushButton_j5 = QPushButton(self.widget_mon_programme)
        self.pushButton_j5.setGeometry(QRect(40, 520, 581, 81))
        self.pushButton_j5.setStyleSheet(u"QPushButton {color: rgb(255, 255, 255);border-radius: 20px;background-color: rgb(5, 5, 56);border: 3px solid white;}""QPushButton:hover {background:  rgb(55, 55, 110);}")
        self.pushButton_j5.setIconSize(QSize(90, 90))
        font = self.pushButton_j5.font()
        font.setPointSize(30)
        self.pushButton_j5.setFont(font)
        self.pushButton_j6 = QPushButton(self.widget_mon_programme)
        self.pushButton_j6.setGeometry(QRect(40, 610, 581, 81))
        self.pushButton_j6.setStyleSheet(u"QPushButton {color: rgb(255, 255, 255);border-radius: 20px;background-color: rgb(5, 5, 56);border: 3px solid white;}""QPushButton:hover {background:  rgb(55, 55, 110);}")
        self.pushButton_j6.setIconSize(QSize(90, 90))
        self.widget_tous_programmes = QWidget(self)
        font = self.pushButton_j6.font()
        font.setPointSize(30)
        self.pushButton_j6.setFont(font)
        self.widget_tous_programmes.setGeometry(QRect(1090, 100, 661, 781))
        self.widget_tous_programmes.setStyleSheet(u"color: rgb(0, 0, 0);border-radius: 30px; width: 200px; height: 100px; background-color:rgb(47, 47, 68);")
        font = self.widget_tous_programmes.font()
        font.setPointSize(15)
        self.widget_tous_programmes.setFont(font)
        self._titre_tous_programme = QLabel(self.widget_tous_programmes)
        self._titre_tous_programme.setGeometry(QRect(30, 10, 601, 91))
        self._titre_tous_programme.setStyleSheet(u"color: rgb(255, 255, 255);font: \"Segoe UI\";")
        font = self._titre_tous_programme.font()
        font.setPointSize(40)
        self._titre_tous_programme.setFont(font)
        self.label_image_abdos = QLabel(self.widget_tous_programmes)
        self.label_image_abdos.setGeometry(QRect(30, 160, 291, 161))
        chemin_image_abdos = os.path.join(os.getcwd(), "Icone/abdos.png").replace("\\", "/")
        self.label_image_abdos.setPixmap(QPixmap(chemin_image_abdos))
        self.label_image_abdos.setScaledContents(True)
        self.label_image_poitrine = QLabel(self.widget_tous_programmes)
        self.label_image_poitrine.setGeometry(QRect(340, 160, 291, 161))
        chemin_image_poitrine = os.path.join(os.getcwd(), "Icone/poitrine.png").replace("\\", "/")
        self.label_image_poitrine.setPixmap(QPixmap(chemin_image_poitrine))
        self.label_image_poitrine.setScaledContents(True)
        self.label_image_jambes = QLabel(self.widget_tous_programmes)
        self.label_image_jambes.setGeometry(QRect(30, 340, 291, 161))
        chemin_image_jambes = os.path.join(os.getcwd(), "Icone/jambes.png").replace("\\", "/")
        self.label_image_jambes.setPixmap(QPixmap(chemin_image_jambes))
        self.label_image_jambes.setScaledContents(True)
        self.label_image_bras = QLabel(self.widget_tous_programmes)
        self.label_image_bras.setGeometry(QRect(340, 340, 291, 161))
        chemin_image_bras = os.path.join(os.getcwd(), "Icone/bras.png").replace("\\", "/")
        self.label_image_bras.setPixmap(QPixmap(chemin_image_bras))
        self.label_image_bras.setScaledContents(True)
        self.label_image_fesses = QLabel(self.widget_tous_programmes)
        self.label_image_fesses.setGeometry(QRect(30, 520, 291, 161))
        chemin_image_fesses = os.path.join(os.getcwd(), "Icone/fesses.png").replace("\\", "/")
        self.label_image_fesses.setPixmap(QPixmap(chemin_image_fesses))
        self.label_image_fesses.setScaledContents(True)
        self.label_image_dos = QLabel(self.widget_tous_programmes)
        self.label_image_dos.setGeometry(QRect(340, 520, 291, 161))
        chemin_image_dos = os.path.join(os.getcwd(), "Icone/dos.png").replace("\\", "/")
        self.label_image_dos.setPixmap(QPixmap(chemin_image_dos))
        self.label_image_dos.setScaledContents(True)
        self.pushButton_zone_cliquable_abdos = QPushButton(self.widget_tous_programmes)
        self.pushButton_zone_cliquable_abdos.setGeometry(QRect(30, 160, 291, 161))
        self.pushButton_zone_cliquable_abdos.setStyleSheet(u"QPushButton {color: rgb(255, 255, 255); border-radius: 0px;background-color: rgba(0, 0, 0, 0.4);}""QPushButton:hover {background:  rgba(60, 60, 60,0.4);}")
        font = self.pushButton_zone_cliquable_abdos.font()
        font.setPointSize(20)
        font.setBold(True)
        self.pushButton_zone_cliquable_abdos.setFont(font)
        self.pushButton_zone_cliquable_jambes = QPushButton(self.widget_tous_programmes)
        self.pushButton_zone_cliquable_jambes.setGeometry(QRect(30, 340, 291, 161))
        self.pushButton_zone_cliquable_jambes.setStyleSheet(u"QPushButton {color: rgb(255, 255, 255); border-radius: 0px;background-color: rgba(0, 0, 0, 0.4);}""QPushButton:hover {background:  rgba(60, 60, 60,0.4);}")
        font = self.pushButton_zone_cliquable_jambes.font()
        font.setPointSize(20)
        font.setBold(True)
        self.pushButton_zone_cliquable_jambes.setFont(font)
        self.pushButton_zone_cliquable_dos = QPushButton(self.widget_tous_programmes)
        self.pushButton_zone_cliquable_dos.setGeometry(QRect(340, 520, 291, 161))
        self.pushButton_zone_cliquable_dos.setStyleSheet(u"QPushButton {color: rgb(255, 255, 255);border-radius: 0px;background-color: rgba(0, 0, 0, 0.4);}""QPushButton:hover {background:  rgba(60, 60, 60,0.4);}")
        font = self.pushButton_zone_cliquable_dos.font()
        font.setPointSize(20)
        font.setBold(True)
        self.pushButton_zone_cliquable_dos.setFont(font)
        self.pushButton_zone_cliquable_bras = QPushButton(self.widget_tous_programmes)
        self.pushButton_zone_cliquable_bras.setGeometry(QRect(340, 340, 291, 161))
        self.pushButton_zone_cliquable_bras.setStyleSheet(u"QPushButton {color: rgb(255, 255, 255);border-radius: 0px;background-color: rgba(0, 0, 0, 0.4);}""QPushButton:hover {background:  rgba(60, 60, 60,0.4);}")
        font = self.pushButton_zone_cliquable_bras.font()
        font.setPointSize(20)
        font.setBold(True)
        self.pushButton_zone_cliquable_bras.setFont(font)
        self.pushButton_zone_cliquable_poitrine = QPushButton(self.widget_tous_programmes)
        self.pushButton_zone_cliquable_poitrine.setGeometry(QRect(340, 160, 291, 161))
        self.pushButton_zone_cliquable_poitrine.setStyleSheet(u"QPushButton {color: rgb(255, 255, 255);border-radius: 0px;background-color: rgba(0, 0, 0, 0.4);}""QPushButton:hover {background:  rgba(60, 60, 60,0.4);}")
        font = self.pushButton_zone_cliquable_poitrine.font()
        font.setPointSize(20)
        font.setBold(True)
        self.pushButton_zone_cliquable_poitrine.setFont(font)
        self.pushButton_zone_cliquable_fesses = QPushButton(self.widget_tous_programmes)
        self.pushButton_zone_cliquable_fesses.setGeometry(QRect(30, 520, 291, 161))
        self.pushButton_zone_cliquable_fesses.setStyleSheet(u"QPushButton {color: rgb(255, 255, 255);border-radius: 0px;background-color: rgba(0, 0, 0, 0.4);}""QPushButton:hover {background:  rgba(60, 60, 60,0.4);}")
        font = self.pushButton_zone_cliquable_fesses.font()
        font.setPointSize(20)
        font.setBold(True)
        self.pushButton_zone_cliquable_fesses.setFont(font)
        self.button_return = QPushButton("Retour", self)
        self.button_return.setGeometry(30, 20, 150, 40)
        self.button_return.setStyleSheet(u"QPushButton {border-radius: 10px; background-color: rgb(225, 225, 225);} QPushButton:hover {background: rgb(237, 255, 255);}")
        font = self.button_return.font()
        font.setPointSize(20)
        self.button_return.setFont(font)
        self.button_return.clicked.connect(self.allerpagedapres)
    
        #Connecter les boutons à une action
        
        self.buttons = [self.pushButton_j1, self.pushButton_j2, self.pushButton_j3, 
                    self.pushButton_j4, self.pushButton_j5, self.pushButton_j6]
        
        for button in self.buttons:
            sport = randint(1, 6)
            if sport == 1:
                button.clicked.connect(lambda: self.ouvrir_page_tracking(self.abdos)) 
            elif sport == 2:
                button.clicked.connect(lambda: self.ouvrir_page_tracking(self.poitrine_et_dos)) 
            elif sport == 3:
                button.clicked.connect(lambda: self.ouvrir_page_tracking(self.bras)) 
            elif sport == 4:
                button.clicked.connect(lambda: self.ouvrir_page_tracking(self.jambes)) 
            elif sport == 5:
                button.clicked.connect(lambda: self.ouvrir_page_tracking(self.épaules_et_dos)) 
            elif sport == 6:
                button.clicked.connect(lambda: self.ouvrir_page_tracking(self.fesses)) 

        #Associer les textes des Labels
        self.pushButton_j1.setText("JOUR 1")
        self.pushButton_j2.setText("JOUR 2")
        self.pushButton_j3.setText("JOUR 3")
        self.pushButton_j4.setText("JOUR 4")
        self.pushButton_j5.setText("JOUR 5")
        self.pushButton_j6.setText("JOUR 6")
        self._titre_mon_programme.setText("MON PROGRAMME")
        self._titre_tous_programme.setText("TOUS LES PROGRAMMES")
        self.pushButton_zone_cliquable_abdos.setText("ABDOS")
        self.pushButton_zone_cliquable_jambes.setText("JAMBES")
        self.pushButton_zone_cliquable_dos.setText("EPAULES ET DOS")
        self.pushButton_zone_cliquable_bras.setText("BRAS")
        self.pushButton_zone_cliquable_poitrine.setText("POITRINE")
        self.pushButton_zone_cliquable_fesses.setText("FESSES")
        
        #Connecter les boutons
        self.pushButton_zone_cliquable_abdos.clicked.connect(lambda: self.ouvrir_tous_programme(self.abdos))
        self.pushButton_zone_cliquable_poitrine.clicked.connect(lambda: self.ouvrir_tous_programme(self.poitrine_et_dos))
        self.pushButton_zone_cliquable_bras.clicked.connect(lambda: self.ouvrir_tous_programme(self.bras))
        self.pushButton_zone_cliquable_jambes.clicked.connect(lambda: self.ouvrir_tous_programme(self.jambes))
        self.pushButton_zone_cliquable_dos.clicked.connect(lambda: self.ouvrir_tous_programme(self.épaules_et_dos))
        self.pushButton_zone_cliquable_fesses.clicked.connect(lambda: self.ouvrir_tous_programme(self.fesses))
        self.a = 0
        self.b = 0
        
        # Contient une liste d'exercices permettant de travailler l'intégralité des muscles du corps, divisees en sections correspondant aux différentes parties du corps
        self.abdos = [["6min","jumping_jack","00:30","crunch","00:30","escalade","00:30","leve_de_jambes","00:30","gainage","00:30","crunch","00:30","escalade","00:30","leve_de_jambes","00:30","gainage","00:30"],
                      ["10min40","jumping_jack","00:30","crunch_croise","00:30","escalade","00:30","pont_lateral","00:30","pont_lateral","00:30","hip_thurst","00:30","crunch_type_velo","00:30","abdominaux_en_v","00:30","crunch","00:30","gainage","00:30","crunch_croise","00:30","leve_de_jambes","00:30","crunch_type_velo","00:30","pompes_rotation","00:30","planche_laterale","00:30","planche_laterale","00:30"],
                      ["14min40","jumping_jack","00:30","abdominaux","00:30","pont_lateral","00:30","leve_de_jambes","00:30","pont_lateral","00:30","crunch","00:30","escalade","00:30","crunch_type_velo","00:30","gainage","00:30","leve_de_jambes","00:30","planche_laterale","00:30","planche_laterale","00:30","escalade","00:30","abdominaux_en_v","00:30","pompes_rotation","00:30","gainage","00:30","crunch","00:30","hip_thurst","00:30","escalade","00:30","crunch_croise","00:30","abdominaux_en_v","00:30","gainage","00:30"]]
        self.poitrine_et_dos = [["5min20","jumping_jack","00:30","pompes_bras_ecartes","00:30","pompes","00:30","pompes_bras_ecartes","00:30","dips_chaise","00:30","pompes_bras_ecartes","00:30","pompes_piquees","00:30","dips_chaise","00:30"],
                                ["5min20","jumping_jack","00:30","pompes","00:30","pompes_piquees","00:30","pompes_bras_ecartes","00:30","pompes_diamant","00:30","pompes_rotation","00:30","pompes_spiderman","00:30","pompes","00:30","dips_chaise","00:30"],
                                ["5min20","jumping_jack","00:30","pompes","00:30","pompes_rotation","00:30","pompes diamant","00:30","pompes_bras_ecartes","00:30","pompes_spiderman","00:30","pompes","00:30","dips_sol","00:30"]]
        self.bras =[["8min","leve_de_bras","00:30","elevation_laterale_bras","00:30","dips_chaise","00:30","pompes_diamant","00:30","jumping_jack","00:30","curl_de_jambe","00:30","curl_de_jambe","00:30","planche_diagonale","00:30","coup_de_poing","00:30","pompes","00:30","chenille","00:30","dips_sol","00:30"],
                    ["12 min","dips_sol","00:30","pompes","00:30","coup_de_poing","00:30","pompes_rotation","00:30","curl_de_jambe","00:30","pompes_diamant","00:30","dips_sol","00:30","pompes_diamant","00:30","curl_de_jambe","00:30","dips_sol","00:30","pompes","00:30","coup_de_poing","00:30","pompes_rotation","00:30","curl_de_jambe","00:30","curl_de_jambe","00:30","pompes","00:30","ciseaux_bras","00:30","pompes","00:30"],
                    ["14min40","curl_de_jambe","00:30","pompes_bras_ecartes","00:30","curl_de_jambe","00:30","pompes_diamant","00:30","dips_sol","00:30","coup_de_poing","00:30","pompes_spiderman","00:30","souleve_halteres","00:30","epaules_crocodiles","00:30","kickback_triceps","00:30","dips_sol","00:30","pompes_triceps_sur_le_ventre","00:30","dips_sol","00:30","coup_de_poing","00:30","pompes_spiderman","00:30","epaules_crocodiles","00:30","kickback_triceps","00:30","curl_porte","00:30","dips_chaise","00:30","curl_porte","00:30","pompes_bras_ecartes","00:30","pompes_rotation","00:30"]]
        self.jambes=[["7min40","squat","00:30","leve_de_jambes_lateral_gauche","00:30","leve_de_jambes_lateral_droite","00:30","leve_de_jambes_lateral_gauche","00:30","leve_de_jambes_lateral_droite","00:30","fente","00:30","fente","00:30","donkey_kicks","00:30","donkey_kicks","00:30","etirement_quadriceps_contre_mur","00:30","etirement_genou_poitrine","00:30","extension_mollet_contre_le_mur","00:30","squat de sumo","00:30"],
                     ["14min","jumping_jack","00:30","squat","00:30","fire_hydrant","00:30","fire_hydrant","00:30","fente","00:30","squat","00:30","adducteurs_lateraux_couche","00:30","chaise","00:30","fente","00:30","squat_sumo","00:30","battements_de_jambes","00:30","leve_de_jambes_lateral_gauche","00:30","leve_de_jambes_lateral_droite","00:30","chaise","00:30","etirement_quadriceps_contre_mur","00:30","etirement_quadriceps_contre_mur","00:30","extension_mollet_contre_le_mur","00:30","squat_sumo","00:30","levee_genoux","00:30","donkey_kicks","00:30","battements_de_jambes","00:30"],
                     ["17min20","squat","00:30","adducteurs_lateraux_couche","00:30","adducteurs_lateraux_couche","00:30","donkey_kicks","00:30","squat_sumo","00:30","adducteurs_lateraux_couche","00:30","adducteurs_lateraux_couche","00:30","battements_de_jambes","00:30","fire_hydrant","00:30","adducteurs_lateraux_couche","00:30","adducteurs_lateraux_couche","00:30","fente_reverence","00:30","squat","00:30","levee_jambes_arriere","00:30","levee_jambes_arriere","00:30","fente","00:30","chaise","00:30","etirement_quatriceps_contre_mur","00:30","etirement_quatriceps_contre_mur","00:30","squat","00:30","extension_mollet_contre_le_mur","00:30","donkey_kicks","00:30","extension_mollet_contre_le_mur","00:30","chaise","00:30","leve_de_jambes_lateral_droite","00:30","leve_de_jambes_lateral_gauche","00:30"]]
        self.épaules_et_dos =[["6min40","jumping_jack","00:30","leve_de_bras","00:30","elevation_laterale_bras","00:30","pompes","00:30","ciseaux_bras","00:30","elevation_laterale_bras","00:30","pompes","00:30","pompes_triceps_sur_le_ventre","00:30","ciseaux_bras","00:30","elevation_laterale_bras","00:30"],
                              ["9min20","jumping_jack","00:30","kickback_triceps","00:30","pompes","00:30","souleve_halteres","00:30","dips_sol","00:30","superman","00:30","kickback_triceps","00:30","pompes_bras_ecartes","00:30","souleve_halteres","00:30","superman","00:30","flexion_avant","00:30","dips_sol","00:30","pompes_piquees","00:30","superman","00:30"], 
                              ["10min40","jumping_jack","00:30","pompes_piquees","00:30","souleve_halteres","00:30","kickback_triceps","00:30","pompes","00:30","chenille","00:30","ciseaux_bras","00:30","elevation_laterale_bras","00:30","pompes_spiderman","00:30","flexion_avant","00:30","dips_sol","00:30","pompes_triceps_sur_le_ventre","00:30","chenille","00:30","souleve_halteres","00:30","flexion_avant","00:30","pompes_diamant","00:30"]]
        self.fesses=[["10min40","levee_genoux","00:30","coups_de_pieds_fessier","00:30","squat","00:30","levee_jambes_arriere","00:30","levee_jambes_arriere","00:30","hip_thurst","00:30","escalade","00:30","donkey_kicks","00:30","donkey_kicks","00:30","squat","00:30","levee_jambes_arriere","00:30","levee_jambes_arriere","00:30","hip_thurst","00:30","escalade","00:30","donkey_kicks","00:30","donkey_kicks","00:30"],
                     ["14min40","levee_genoux","00:30","coups_de_pieds_fessier","00:30","fente","00:30","donkey_kicks","00:30","donkey_kicks","00:30","squat","00:30","hip_thurst","00:30","adducteurs_lateraux_couche","00:30","adducteurs_lateraux_couche","00:30","fire_hydrant","00:30","fire_hydrant","00:30","accroupie_avec_genou_flechi","00:30","accroupie_avec_genou_flechi","00:30","donkey_kicks","00:30","donkey_kicks","00:30","squat","00:30","hip_thurst","00:30","fente","00:30","fire_hydrant","00:30","fire_hydrant","00:30","accroupie_avec_genou_flechi","00:30","accroupie_avec_genou_flechi","00:30"],
                     ["16min40","levee_genoux","00:30","coups_de_pieds_fessier","00:30","squat","00:30","donkey_kicks","00:30","donkey_kicks","00:30","hip_thurst","00:30","fente_reverence","00:30","escalade","00:30","fente","00:30","fente","00:30","battements_de_jambes","00:30","squat_sumo","00:30","leve_de_jambes_lateral_gauche","00:30","leve_de_jambes_lateral_droite","00:30","squat","00:30","donkey_kicks","00:30","donkey_kicks","00:30","hip_thurst","00:30","fente_reverence","00:30","escalade","00:30","fente", "00:30", "levee_genoux","00:30","fente", "00:30", "levee_genoux","00:30","squat_sumo","00:30"]]
                
        # Liste des widgets à traiter
        widgets_to_adjust = [
            self.pushButton_j1,
            self.pushButton_j2,
            self.pushButton_j3,
            self.pushButton_j4,
            self.pushButton_j5,
            self.pushButton_j6,
            
            self.widget_mon_programme,
            self._titre_mon_programme,
            self.widget_tous_programmes,
            self._titre_tous_programme,
            self.label_image_abdos,
            self.label_image_poitrine,
            self.label_image_jambes,
            self.label_image_bras,
            self.label_image_fesses,
            self.label_image_dos,
            self.pushButton_zone_cliquable_abdos,
            self.pushButton_zone_cliquable_jambes,
            self.pushButton_zone_cliquable_dos,
            self.pushButton_zone_cliquable_bras,
            self.pushButton_zone_cliquable_poitrine,
            self.pushButton_zone_cliquable_fesses,
            self.button_return,  
        ]
        # Appel de la fonction pour ajuster les propriétés des widgets
        adjust_widget_properties(widgets_to_adjust, factor)
        
    def ouvrir_tous_programme(self, categorie):
        """Permet d'afficher dans une fenêtre pop-up la liste de tous les exercices disponible dans cette catégorie

        Args:
            categorie (list): Le tableau contenant le programme de tous les exercices
        """
        self.categorie=categorie # On definit la liste d'exercices
        # Créez le nouveau widget sans parent
        self.new_widget = QWidget()
        self.new_widget.setGeometry(0, 30, 1000, 1010)
        
        #Créer les boutons
        self.button_return = QPushButton("Retour", self.new_widget)
        self.button_return.setGeometry(30, 20, 150, 40)
        self.button_return.setStyleSheet(u"QPushButton {border-radius: 10px; background-color: rgb(225, 225, 225);} QPushButton:hover {background: rgb(237, 255, 255);}")
        font = self.button_return.font()
        font.setPointSize(20)
        self.button_return.setFont(font)
        
        #Créer les Widgets
        self.widget_container = QWidget(self.new_widget)
        self.widget_container.setGeometry(QRect(150, 100, 761, 781))
        self.widget_container.setStyleSheet("color: rgb(0, 0, 0); border-radius: 30px; background-color:rgb(37, 38, 81);")
        
        #Créer les boutons
        self.pushButton_calendrier = QPushButton(self.widget_container)
        self.pushButton_calendrier.setGeometry(QRect(210, 30, 350, 51))
        self.pushButton_calendrier.setStyleSheet(u"QPushButton { border-radius: 10px; background-color:rgb(30, 30, 75); color: rgb(255, 255, 255); } ")
        self.niveau = ["Débutant", "Confirmé", "Expert"]
        self.pushButton_calendrier.setText(self.niveau[0])
        font = self.pushButton_calendrier.font()
        font.setPointSize(30)
        self.pushButton_calendrier.setFont(font)
        
        self.pushButton_up = QPushButton(self.widget_container)
        self.pushButton_up.clicked.connect(self.aumenter_valeur_tuple)
        self.pushButton_up.setGeometry(QRect(610, 30, 51, 51))
        self.pushButton_up.setStyleSheet(u"QPushButton { border-radius: 10px; background-color:rgb(30, 30, 75); color: rgb(255, 255, 255); } QPushButton:hover { background:rgb(55, 55, 110) }")
        self.pushButton_up.setText(">")
        font = self.pushButton_up.font()
        font.setPointSize(30)
        self.pushButton_up.setFont(font)
        
        self.pushButton_down = QPushButton(self.widget_container)
        self.pushButton_down.clicked.connect(self.diminuer_valeur_tuple)
        self.pushButton_down.setGeometry(QRect(100, 30, 51, 51))
        self.pushButton_down.setStyleSheet(u"QPushButton { border-radius: 10px; background-color:rgb(30, 30, 75); color: rgb(255, 255, 255); } QPushButton:hover { background:rgb(55, 55, 110) }")
        self.pushButton_down.setText("<")
        font = self.pushButton_down.font()
        font.setPointSize(30)
        self.pushButton_down.setFont(font)

        self.button_return_in = QPushButton("Retour", self.widget_container)
        self.button_return_in.setGeometry(210, 680, 330, 50)
        self.button_return_in.setStyleSheet(u"QPushButton {border-radius: 10px; background-color: rgb(225, 225, 225);} QPushButton:hover {background: rgb(237, 255, 255);}")
        self.button_return_in.setText("LANCER CETTE SEANCE")
        font = self.button_return_in.font()
        font.setPointSize(18)
        self.button_return_in.setFont(font)
        #Connecter les boutons
        self.button_return.clicked.connect(self.close_new_widget)
        self.button_return_in.clicked.connect(lambda: self.ouvrir_page_tracking_individuelle(liste_exo=self.categorie, niveau_objectif=self.a+1))
        self.create_table_widget(self.widget_container, categorie[0])
        #rétablir l'indice du tableau à 0
        self.a = 0
        # Liste des widgets à traiter
        widgets_to_adjust = [
            self.new_widget,
            self.button_return,
            self.widget_container,
            self.pushButton_calendrier,
            self.pushButton_up,
            self.pushButton_down,
            self.button_return_in,
        ]
        
        # Appel de la fonction pour ajuster les propriétés des widgets
        adjust_widget_properties(widgets_to_adjust, factor)
        
        # Affichez le nouveau widget
        self.new_widget.show()   
    
    def create_table_widget(self, parent_widget, data_tuple):
        """Crée un tableau avec chaque exercice et chaque durée contenue dans le data_tuple

        Args:
            parent_widget : C'est l'enplacement où le tableau va se charger.
            data_tuple (tuple): contient le nom et la durée de tous les exercices de la série sélectionnée

        Returns:
            D'une part le widget parent ou le tableau va s'ouvir et d'autre part le tableau en lui même.
        """
        self.widget = QWidget(parent_widget)
        self.widget.setGeometry(25, 120, 710, 525)
        self.widget.setStyleSheet(u"background-color:rgb(255, 255, 255); border-radius: 0px;")
        font = self.widget.font()
        font.setPointSize(17)
        self.widget.setFont(font)
        
        self.tableWidget_2 = QTableWidget(self.widget)
        self.tableWidget_2.setGeometry(10, 10, 700, 510)
        self.tableWidget_2.setColumnCount(2)
        self.tableWidget_2.setHorizontalHeaderLabels(["EXERCICES", "DUREE DE L'EXERCICE"])
        
        # Ajuster la taille de la police des en-têtes de colonnes
        font = self.tableWidget_2.font()
        font.setPointSize(17)
        self.tableWidget_2.setFont(font)

        header_font = self.tableWidget_2.horizontalHeader().font()
        header_font.setPointSize(17*factor)
        self.tableWidget_2.horizontalHeader().setFont(header_font)
        font_size = 17
        self.tableWidget_2.horizontalHeader().setStyleSheet(f"QHeaderView::section {{ font-size: {font_size * factor}px; }}")

        #Ajuster la hauteur de chaque cellule 
        self.tableWidget_2.verticalHeader().setDefaultSectionSize(35 * factor)
        
        # Définir la taille des colonnes
        self.tableWidget_2.horizontalHeader().setDefaultSectionSize(318*factor)

        # Empêcher l'édition des cellules du tableau
        self.tableWidget_2.setEditTriggers(QTableWidget.NoEditTriggers)

        # Désactiver la sélection des cellules du tableau
        self.tableWidget_2.setSelectionMode(QTableWidget.NoSelection)

        # Calcul du nombre de lignes nécessaire
        num_rows = (len(data_tuple) - 1) // 2  # Ignorer la première valeur du tuple

        self.tableWidget_2.setRowCount(num_rows)
        for i in range(num_rows):
            exercice = QTableWidgetItem(data_tuple[i * 2 + 1].replace("_"," "))  # Commencer à l'index 1
            nb_reps = QTableWidgetItem(str(data_tuple[i * 2 + 2]))  # Commencer à l'index 2

            self.tableWidget_2.setItem(i, 0, exercice)
            self.tableWidget_2.setItem(i, 1, nb_reps)
            
        # Liste des widgets à traiter
        widgets_to_adjust = [
            self.widget,
            self.tableWidget_2,
        ]        
        # Appel de la fonction pour ajuster les propriétés des widgets
        adjust_widget_properties(widgets_to_adjust, factor)
        
        return self.widget, self.tableWidget_2
    
    def close_new_widget(self):
        """
        Ferme la fenêtre pop-up 
        """  
        # Fermez le nouveau widget
        self.new_widget.close()
        #rétablir l'indice du tableau à 0
        self.a = 0
        
    def aumenter_valeur_tuple(self):
        """
        Actualise le texte des Labels / Tableau pour le niveau suivant
        """
        if 0 <= self.a < 2: 
            self.a += 1
            self.pushButton_calendrier.setText(self.niveau[self.a])
            # Fermer le widget actuel
            self.widget.close()
            # Créer un nouveau tableau avec les nouveaux paramètres
            widget_container, table_widget = self.create_table_widget(self.widget_container, self.categorie[self.a])
            # Afficher le nouveau widget
            widget_container.show()

    def diminuer_valeur_tuple(self):
        """
        Actualise le texte des Labels / Tableau pour le niveau précédent
        """
        if self.a >0 : 
            self.a -= 1
            self.pushButton_calendrier.setText(self.niveau[self.a])
            # Fermer le widget actuel
            self.widget.close()
            
            # Créer un nouveau tableau avec les nouveaux paramètres
            widget_container, table_widget = self.create_table_widget(self.widget_container, self.categorie[self.a])
            
            # Afficher le nouveau widget
            widget_container.show()
    
    def allerpagedapres(self):
        """Permet de revenir à la page principale, en mettant l'index du QWidget parent
        (qui contient toutes les autres pages à 0). 
        Le QWidget "echangeur" regroupe toutes les pages indexees, de 0 à x, (x le nombre de page qui change au fil du temps, car l'utilisateur en ferme et ouvre en permanence)
        """
        self.echangeur.setCurrentIndex(0)
        # Fermez le nouveau widget
        self.new_widget.close()
        #rétablir l'indice du tableau à 0
        self.a = 0
    
    def ouvrir_page_tracking_individuelle(self, liste_exo, niveau_objectif):
        """Lance la série d'exercice choisi avec le tracking vidéo et les vidéos d'exemples

        Args:
            liste_exo (tuple): contient le nom et la durée de tous les exercices de la série sélectionnée
            niveau_objectif (int): c'est le niveau de difficulté choisi par l'utilisateur (allant de 1 à 3)
        """
        page_tracking = Exercise_Tracker(liste=liste_exo, niveau=niveau_objectif)
        page_tracking.init(liste=liste_exo, niveau=niveau_objectif)

    def ouvrir_page_tracking(self, liste_exo):
        """Lance la série d'exercice choisi avec le tracking vidéo et les vidéos d'exemples

        Args:
            liste_exo (tuple): contient le nom et la durée de tous les exercices de la série sélectionnée
        """
        param = lire_json("parametres.json")
        niveau_objectif = param[4]
        page_tracking = Exercise_Tracker(liste=liste_exo, niveau=niveau_objectif)
        page_tracking.init(liste=liste_exo, niveau=niveau_objectif)
