def poulet_roti_patates_douces(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "paprika", "huile d'olive", "poulet entier", "pommes de terre douces"]
    ingredient = ["sel", "poivre", "paprika", "huile d'olive", 1*multiplicateur_arrondi, " poulet entier", 500*multiplicateur_arrondi, "g de pommes de terre douces"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 200°C.\nÉTAPE 2\nAssaisonner le poulet avec du sel, du poivre et du paprika, puis le frotter avec de l'huile d'olive.\nÉTAPE 3\nPlacer le poulet dans un plat allant au four.\nÉTAPE 4\nCouper les pommes de terre douces en morceaux et les placer autour du poulet.\nÉTAPE 5\nEnfourner pendant environ 1 heure ou jusqu'à ce que le poulet soit doré et cuit à cœur.\nÉTAPE 6\nServir chaud avec les pommes de terre douces."]
    temps_de_preparation = 15
    temps_de_cuisson = 60
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poulet_roti_patates_douces
def steak_boeuf_grille_riz_brun(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "steak de bœuf", "riz brun"]
    ingredient = ["sel", "poivre", "huile d'olive", 1*multiplicateur_arrondi, " steak de bœuf", 100*multiplicateur_arrondi, "g de riz brun"]
    preparation = ["ÉTAPE 1\nFaire cuire le riz brun selon les instructions sur l'emballage.\nÉTAPE 2\nAssaisonner les steaks avec du sel et du poivre.\nÉTAPE 3\nFaire chauffer l'huile d'olive dans une poêle à feu moyen-élevé.\nÉTAPE 4\nFaire griller les steaks pendant 3 à 5 minutes de chaque côté pour une cuisson à point.\nÉTAPE 5\nServir les steaks chauds avec le riz brun."]
    temps_de_preparation = 10
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

steak_boeuf_grille_riz_brun
def lasagnes_epinards_ricotta(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "ail", "épinards", "ricotta", "pâte à lasagne", "sauce tomate", "mozzarella"]
    ingredient = ["sel", "poivre", "huile d'olive", 2*multiplicateur_arrondi, "gousse d'ail", 200*multiplicateur_arrondi, "g d'épinards", 250*multiplicateur_arrondi, "g de ricotta", 6*multiplicateur_arrondi, "feuilles de pâte à lasagne", 500*multiplicateur_arrondi, "ml de sauce tomate", 200*multiplicateur_arrondi, "g de mozzarella"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 180°C.\nÉTAPE 2\nFaire chauffer l'huile d'olive dans une poêle et faire revenir l'ail haché et les épinards jusqu'à ce qu'ils soient ramollis.\nÉTAPE 3\nDans un plat à gratin, alterner des couches de sauce tomate, de pâtes à lasagne, de ricotta, d'épinards et de mozzarella.\nÉTAPE 4\nRépéter jusqu'à ce que tous les ingrédients soient utilisés, en terminant par une couche de mozzarella.\nÉTAPE 5\nCuire au four pendant environ 30 minutes, ou jusqu'à ce que le fromage soit doré et bouillonnant.\nÉTAPE 6\nLaisser reposer quelques minutes avant de servir."]
    temps_de_preparation = 20
    temps_de_cuisson = 30
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

lasagnes_epinards_ricotta
def saumon_grille_quinoa(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "saumon", "quinoa"]
    ingredient = ["sel", "poivre", "huile d'olive", 1*multiplicateur_arrondi, " filet de saumon", 100*multiplicateur_arrondi, "g de quinoa"]
    preparation = ["ÉTAPE 1\nRincer le quinoa sous l'eau froide.\nÉTAPE 2\nFaire cuire le quinoa dans deux fois son volume d'eau salée pendant environ 15 minutes, jusqu'à ce qu'il soit tendre.\nÉTAPE 3\nAssaisonner le saumon avec du sel, du poivre et un filet d'huile d'olive.\nÉTAPE 4\nFaire chauffer une poêle à feu moyen-élevé et faire griller le saumon pendant environ 4 à 5 minutes de chaque côté, ou jusqu'à ce qu'il soit cuit à votre goût.\nÉTAPE 5\nServir le saumon chaud avec le quinoa cuit."]
    temps_de_preparation = 10
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

saumon_grille_quinoa
def omelette_legumes_fromage(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "légumes assortis", "œufs", "fromage râpé"]
    ingredient = ["sel", "poivre", "huile d'olive", 1*multiplicateur_arrondi, " tasse de légumes assortis", 2*multiplicateur_arrondi, " œufs", 30*multiplicateur_arrondi, "g de fromage râpé"]
    preparation = ["ÉTAPE 1\nFaire chauffer un peu d'huile d'olive dans une poêle antiadhésive à feu moyen.\nÉTAPE 2\nFaire revenir les légumes dans la poêle jusqu'à ce qu'ils soient tendres.\nÉTAPE 3\nBattre les œufs dans un bol avec du sel et du poivre.\nÉTAPE 4\nVerser les œufs battus sur les légumes dans la poêle.\nÉTAPE 5\nCuire l'omelette jusqu'à ce qu'elle soit prise sur le dessus, puis saupoudrer de fromage râpé.\nÉTAPE 6\nPlier l'omelette en deux et laisser cuire encore quelques instants jusqu'à ce que le fromage soit fondu.\nÉTAPE 7\nServir chaud."]
    temps_de_preparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

omelette_legumes_fromage
def spaghetti_sauce_bolognaise(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "ail", "oignon", "bœuf haché", "sauce tomate", "spaghetti"]
    ingredient = ["sel", "poivre", "huile d'olive", 2*multiplicateur_arrondi, " gousses d'ail", 1*multiplicateur_arrondi, " oignon", 200*multiplicateur_arrondi, "g de bœuf haché", 400*multiplicateur_arrondi, "g de sauce tomate", 100*multiplicateur_arrondi, "g de spaghetti"]
    preparation = ["ÉTAPE 1\nFaire cuire les spaghetti selon les instructions sur l'emballage.\nÉTAPE 2\nChauffer un peu d'huile d'olive dans une poêle à feu moyen.\nÉTAPE 3\nAjouter l'ail et l'oignon hachés dans la poêle et faire revenir jusqu'à ce qu'ils soient translucides.\nÉTAPE 4\nAjouter le bœuf haché dans la poêle et le faire cuire jusqu'à ce qu'il soit bien doré.\nÉTAPE 5\nVerser la sauce tomate dans la poêle et laisser mijoter pendant environ 10 minutes.\nÉTAPE 6\nAssaisonner avec du sel et du poivre selon votre goût.\nÉTAPE 7\nServir la sauce bolognaise chaude sur les spaghetti cuits."]
    temps_de_preparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

spaghetti_sauce_bolognaise
def chili_con_carne_riz(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "oignon", "ail", "bœuf haché", "poivron", "haricots rouges", "tomates concassées", "cumin", "paprika", "riz"]
    ingredient = ["sel", "poivre", "huile d'olive", 1*multiplicateur_arrondi, " oignon", 2*multiplicateur_arrondi, " gousses d'ail", 200*multiplicateur_arrondi, "g de bœuf haché", 1*multiplicateur_arrondi, " poivron", 400*multiplicateur_arrondi, "g de haricots rouges en conserve", 400*multiplicateur_arrondi, "g de tomates concassées en conserve", "cumin", "paprika", 100*multiplicateur_arrondi, "g de riz"]
    preparation = ["ÉTAPE 1\nFaire cuire le riz selon les instructions sur l'emballage.\nÉTAPE 2\nChauffer un peu d'huile d'olive dans une grande casserole à feu moyen.\nÉTAPE 3\nAjouter l'oignon et l'ail hachés dans la casserole et faire revenir jusqu'à ce qu'ils soient translucides.\nÉTAPE 4\nAjouter le bœuf haché dans la casserole et le faire cuire jusqu'à ce qu'il soit bien doré.\nÉTAPE 5\nIncorporer le poivron coupé en dés et laisser cuire quelques minutes de plus.\nÉTAPE 6\nAjouter les haricots rouges égouttés, les tomates concassées et les épices dans la casserole.\nÉTAPE 7\nLaisser mijoter pendant environ 20 minutes, en remuant de temps en temps.\nÉTAPE 8\nServir chaud sur le riz cuit."]
    temps_de_preparation = 15
    temps_de_cuisson = 25
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

chili_con_carne_riz
def poulet_curry_riz_basmati(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "oignon", "ail", "curry en poudre", "lait de coco", "poulet", "riz basmati"]
    ingredient = ["sel", "poivre", "huile d'olive", 1*multiplicateur_arrondi, " oignon", 2*multiplicateur_arrondi, " gousses d'ail", "curry en poudre", 400*multiplicateur_arrondi, "ml de lait de coco", 200*multiplicateur_arrondi, "g de poulet", 100*multiplicateur_arrondi, "g de riz basmati"]
    preparation = ["ÉTAPE 1\nFaire cuire le riz basmati selon les instructions sur l'emballage.\nÉTAPE 2\nChauffer un peu d'huile d'olive dans une grande poêle à feu moyen.\nÉTAPE 3\nAjouter l'oignon et l'ail hachés dans la poêle et faire revenir jusqu'à ce qu'ils soient translucides.\nÉTAPE 4\nAjouter le curry en poudre dans la poêle et laisser cuire quelques instants pour libérer les arômes.\nÉTAPE 5\nIncorporer le lait de coco dans la poêle et bien mélanger.\nÉTAPE 6\nAjouter le poulet coupé en dés dans la poêle et laisser mijoter pendant environ 15 minutes, ou jusqu'à ce que le poulet soit cuit.\nÉTAPE 7\nServir chaud sur le riz basmati cuit."]
    temps_de_preparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poulet_curry_riz_basmati
def casserole_pommes_terre_saucisses(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "pommes de terre", "saucisses", "fromage râpé"]
    ingredient = ["sel", "poivre", "huile d'olive", 500*multiplicateur_arrondi, "g de pommes de terre", 300*multiplicateur_arrondi, "g de saucisses", 100*multiplicateur_arrondi, "g de fromage râpé"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 200°C.\nÉTAPE 2\nÉplucher et couper les pommes de terre en tranches fines.\nÉTAPE 3\nFaire chauffer un peu d'huile d'olive dans une grande poêle à feu moyen.\nÉTAPE 4\nFaire dorer les tranches de pommes de terre dans la poêle pendant quelques minutes de chaque côté.\nÉTAPE 5\nDisposer les tranches de pommes de terre dans un plat à gratin légèrement huilé.\nÉTAPE 6\nCouper les saucisses en rondelles et les répartir sur les pommes de terre.\nÉTAPE 7\nSaupoudrer de fromage râpé sur le dessus.\nÉTAPE 8\nEnfourner au four préchauffé et cuire pendant environ 30 minutes, ou jusqu'à ce que le fromage soit doré et bouillonnant.\nÉTAPE 9\nServir chaud."]
    temps_de_preparation = 15
    temps_de_cuisson = 30
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

casserole_pommes_terre_saucisses
def poisson_four_patates_douces(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "filet de poisson", "patates douces"]
    ingredient = ["sel", "poivre", "huile d'olive", 1*multiplicateur_arrondi, " filet de poisson", 200*multiplicateur_arrondi, "g de patates douces"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 200°C.\nÉTAPE 2\nÉplucher et couper les patates douces en morceaux.\nÉTAPE 3\nBadigeonner les filets de poisson avec de l'huile d'olive et assaisonner avec du sel et du poivre.\nÉTAPE 4\nDisposer les patates douces sur une plaque de cuisson et placer les filets de poisson par-dessus.\nÉTAPE 5\nEnfourner au four préchauffé et cuire pendant environ 20 minutes, ou jusqu'à ce que le poisson soit cuit à cœur et les patates douces soient tendres.\nÉTAPE 6\nServir chaud."]
    temps_de_preparation = 10
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poisson_four_patates_douces
def tacos_poulet_salsa_avocat(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "poulet", "tortillas de maïs", "salsa", "avocat"]
    ingredient = ["sel", "poivre", "huile d'olive", 2*multiplicateur_arrondi, " filets de poulet", 4*multiplicateur_arrondi, " tortillas de maïs", " salsa", " avocat"]
    preparation = ["ÉTAPE 1\nChauffer un peu d'huile d'olive dans une poêle à feu moyen.\nÉTAPE 2\nAssaisonner les filets de poulet avec du sel et du poivre, puis les faire cuire dans la poêle jusqu'à ce qu'ils soient dorés et cuits à cœur.\nÉTAPE 3\nPendant ce temps, chauffer les tortillas de maïs dans une autre poêle ou au micro-ondes.\nÉTAPE 4\nCouper les filets de poulet cuits en morceaux.\nÉTAPE 5\nGarnir les tortillas chauffées avec les morceaux de poulet, la salsa et des tranches d'avocat.\nÉTAPE 6\nReplier les tortillas et servir chaud."]
    temps_de_preparation = 10
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

tacos_poulet_salsa_avocat
def tacos_poulet_salsa_avocat(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "poulet", "tortillas de maïs", "salsa", "avocat"]
    ingredient = ["sel", "poivre", "huile d'olive", 2*multiplicateur_arrondi, " filets de poulet", 4*multiplicateur_arrondi, " tortillas de maïs", " salsa", " avocat"]
    preparation = ["ÉTAPE 1\nChauffer un peu d'huile d'olive dans une poêle à feu moyen.\nÉTAPE 2\nAssaisonner les filets de poulet avec du sel et du poivre, puis les faire cuire dans la poêle jusqu'à ce qu'ils soient dorés et cuits à cœur.\nÉTAPE 3\nPendant ce temps, chauffer les tortillas de maïs dans une autre poêle ou au micro-ondes.\nÉTAPE 4\nCouper les filets de poulet cuits en morceaux.\nÉTAPE 5\nGarnir les tortillas chauffées avec les morceaux de poulet, la salsa et des tranches d'avocat.\nÉTAPE 6\nReplier les tortillas et servir chaud."]
    temps_de_preparation = 10
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

tacos_poulet_salsa_avocat

def risotto_champignons_poulet(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "oignon", "ail", "champignons", "riz arborio", "bouillon de poulet", "poulet", "parmesan râpé"]
    ingredient = ["sel", "poivre", "huile d'olive", 1*multiplicateur_arrondi, " oignon", 2*multiplicateur_arrondi, " gousses d'ail", 200*multiplicateur_arrondi, "g de champignons", 200*multiplicateur_arrondi, "g de riz arborio", 500*multiplicateur_arrondi, "ml de bouillon de poulet", 200*multiplicateur_arrondi, "g de poulet", 50*multiplicateur_arrondi, "g de parmesan râpé"]
    preparation = ["ÉTAPE 1\nFaire chauffer le bouillon de poulet dans une casserole et le maintenir chaud à feu doux.\nÉTAPE 2\nChauffer un peu d'huile d'olive dans une grande poêle à feu moyen.\nÉTAPE 3\nAjouter l'oignon et l'ail hachés dans la poêle et faire revenir jusqu'à ce qu'ils soient translucides.\nÉTAPE 4\nAjouter les champignons coupés en tranches dans la poêle et faire cuire jusqu'à ce qu'ils soient dorés.\nÉTAPE 5\nAjouter le riz arborio dans la poêle et le faire revenir pendant quelques minutes jusqu'à ce qu'il devienne translucide.\nÉTAPE 6\nVerser une louche de bouillon chaud dans la poêle et remuer jusqu'à ce que le liquide soit absorbé.\nÉTAPE 7\nContinuer à ajouter le bouillon, une louche à la fois, en remuant constamment, jusqu'à ce que le riz soit tendre et crémeux.\nÉTAPE 8\nPendant ce temps, assaisonner les morceaux de poulet avec du sel et du poivre, puis les faire cuire dans une autre poêle jusqu'à ce qu'ils soient dorés et cuits à cœur.\nÉTAPE 9\nIncorporer les morceaux de poulet cuits dans le risotto cuit.\nÉTAPE 10\nServir chaud avec du parmesan râpé sur le dessus."]
    temps_de_preparation = 20
    temps_de_cuisson = 30
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

risotto_champignons_poulet
def risotto_champignons_poulet(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "oignon", "ail", "champignons", "riz arborio", "bouillon de poulet", "poulet", "parmesan râpé"]
    ingredient = ["sel", "poivre", "huile d'olive", 1*multiplicateur_arrondi, " oignon", 2*multiplicateur_arrondi, " gousses d'ail", 200*multiplicateur_arrondi, "g de champignons", 200*multiplicateur_arrondi, "g de riz arborio", 500*multiplicateur_arrondi, "ml de bouillon de poulet", 200*multiplicateur_arrondi, "g de poulet", 50*multiplicateur_arrondi, "g de parmesan râpé"]
    preparation = ["ÉTAPE 1\nFaire chauffer le bouillon de poulet dans une casserole et le maintenir chaud à feu doux.\nÉTAPE 2\nChauffer un peu d'huile d'olive dans une grande poêle à feu moyen.\nÉTAPE 3\nAjouter l'oignon et l'ail hachés dans la poêle et faire revenir jusqu'à ce qu'ils soient translucides.\nÉTAPE 4\nAjouter les champignons coupés en tranches dans la poêle et faire cuire jusqu'à ce qu'ils soient dorés.\nÉTAPE 5\nAjouter le riz arborio dans la poêle et le faire revenir pendant quelques minutes jusqu'à ce qu'il devienne translucide.\nÉTAPE 6\nVerser une louche de bouillon chaud dans la poêle et remuer jusqu'à ce que le liquide soit absorbé.\nÉTAPE 7\nContinuer à ajouter le bouillon, une louche à la fois, en remuant constamment, jusqu'à ce que le riz soit tendre et crémeux.\nÉTAPE 8\nPendant ce temps, assaisonner les morceaux de poulet avec du sel et du poivre, puis les faire cuire dans une autre poêle jusqu'à ce qu'ils soient dorés et cuits à cœur.\nÉTAPE 9\nIncorporer les morceaux de poulet cuits dans le risotto cuit.\nÉTAPE 10\nServir chaud avec du parmesan râpé sur le dessus."]
    temps_de_preparation = 20
    temps_de_cuisson = 30
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

risotto_champignons_poulet
def pates_poulet_alfredo(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "poulet", "pâtes", "crème épaisse", "ail", "parmesan râpé"]
    ingredient = ["sel", "poivre", "huile d'olive", 2*multiplicateur_arrondi, " filets de poulet", 200*multiplicateur_arrondi, "g de pâtes", 200*multiplicateur_arrondi, "ml de crème épaisse", 2*multiplicateur_arrondi, " gousses d'ail", 50*multiplicateur_arrondi, "g de parmesan râpé"]
    preparation = ["ÉTAPE 1\nFaire cuire les pâtes selon les instructions sur l'emballage.\nÉTAPE 2\nCouper les filets de poulet en dés.\nÉTAPE 3\nChauffer un peu d'huile d'olive dans une grande poêle à feu moyen.\nÉTAPE 4\nAjouter les dés de poulet dans la poêle et les faire cuire jusqu'à ce qu'ils soient dorés et cuits à cœur.\nÉTAPE 5\nAjouter l'ail haché dans la poêle et faire revenir quelques instants jusqu'à ce qu'il soit parfumé.\nÉTAPE 6\nVerser la crème épaisse dans la poêle et laisser mijoter pendant quelques minutes jusqu'à ce qu'elle épaississe légèrement.\nÉTAPE 7\nIncorporer les pâtes cuites dans la poêle et bien mélanger.\nÉTAPE 8\nAjouter le parmesan râpé dans la poêle et remuer jusqu'à ce qu'il soit fondu.\nÉTAPE 9\nServir chaud."]
    temps_de_preparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

pates_poulet_alfredo
def wrap_boeuf_legumes_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "bœuf", "tortillas", "poivron", "courgette", "aubergine"]
    ingredient = ["sel", "poivre", "huile d'olive", 2*multiplicateur_arrondi, " steak de bœuf", 4*multiplicateur_arrondi, " tortillas", 1*multiplicateur_arrondi, " poivron", 1*multiplicateur_arrondi, " courgette", 1*multiplicateur_arrondi, " aubergine"]
    preparation = ["ÉTAPE 1\nAssaisonner les steaks de bœuf avec du sel, du poivre et un peu d'huile d'olive.\nÉTAPE 2\nFaire chauffer un grill ou une poêle à feu moyen-élevé.\nÉTAPE 3\nFaire griller les steaks de bœuf pendant 3 à 5 minutes de chaque côté, ou jusqu'à ce qu'ils soient cuits à votre goût.\nÉTAPE 4\nPendant ce temps, couper le poivron, la courgette et l'aubergine en fines lamelles.\nÉTAPE 5\nFaire chauffer un peu d'huile d'olive dans une autre poêle et faire griller les légumes jusqu'à ce qu'ils soient tendres et légèrement dorés.\nÉTAPE 6\nRéchauffer les tortillas de maïs dans une poêle ou au micro-ondes.\nÉTAPE 7\nCouper les steaks de bœuf cuits en fines tranches.\nÉTAPE 8\nGarnir chaque tortilla chauffée avec des tranches de bœuf et des légumes grillés.\nÉTAPE 9\nRouler les tortillas en wraps et servir chaud."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

wrap_boeuf_legumes_grilles
def pizza_garniture_au_choix(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "pâte à pizza", "sauce tomate", "fromage râpé", "garniture au choix"]
    ingredient = ["sel", "poivre", "huile d'olive", 1*multiplicateur_arrondi, " pâte à pizza", 200*multiplicateur_arrondi, "ml de sauce tomate", 100*multiplicateur_arrondi, "g de fromage râpé", " garniture au choix"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 220°C.\nÉTAPE 2\nÉtaler la pâte à pizza sur une plaque de cuisson légèrement huilée.\nÉTAPE 3\nÉtaler la sauce tomate sur la pâte à pizza.\nÉTAPE 4\nSaupoudrer généreusement de fromage râpé sur le dessus.\nÉTAPE 5\nAjouter la garniture de votre choix, comme des tranches de pepperoni, des légumes coupés en dés, ou des morceaux de poulet cuit.\nÉTAPE 6\nArroser d'un filet d'huile d'olive et assaisonner avec du sel et du poivre.\nÉTAPE 7\nEnfourner au four préchauffé et cuire pendant environ 15 minutes, ou jusqu'à ce que la croûte soit dorée et croustillante.\nÉTAPE 8\nDéguster chaud."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

pizza_garniture_au_choix
def burger_dinde_frites_patates_douces(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "escalopes de dinde", "pain à burger", "patates douces"]
    ingredient = ["sel", "poivre", "huile d'olive", 2*multiplicateur_arrondi, " escalopes de dinde", 2*multiplicateur_arrondi, " pains à burger", 400*multiplicateur_arrondi, "g de patates douces"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 200°C.\nÉTAPE 2\nÉplucher et couper les patates douces en bâtonnets pour les frites.\nÉTAPE 3\nDisposer les bâtonnets de patates douces sur une plaque de cuisson légèrement huilée.\nÉTAPE 4\nAssaisonner les escalopes de dinde avec du sel, du poivre et un peu d'huile d'olive.\nÉTAPE 5\nFaire cuire les escalopes de dinde dans une poêle chaude jusqu'à ce qu'elles soient dorées et cuites à cœur.\nÉTAPE 6\nEn même temps, mettre les patates douces au four et les faire cuire pendant environ 20 à 25 minutes, ou jusqu'à ce qu'elles soient dorées et croustillantes.\nÉTAPE 7\nPendant ce temps, faire griller les pains à burger au four ou dans un grille-pain.\nÉTAPE 8\nAssembler les burgers en plaçant les escalopes de dinde cuites entre les pains à burger.\nÉTAPE 9\nServir chaud avec les frites de patates douces."]
    temps_de_preparation = 15
    temps_de_cuisson = 25
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

burger_dinde_frites_patates_douces

def poulet_teriyaki_riz_blanc(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "poulet", "sauce teriyaki", "riz blanc"]
    ingredient = ["sel", "poivre", "huile d'olive", 2*multiplicateur_arrondi, " filets de poulet", 100*multiplicateur_arrondi, "ml de sauce teriyaki", 100*multiplicateur_arrondi, "g de riz blanc"]
    preparation = ["ÉTAPE 1\nFaire cuire le riz blanc selon les instructions sur l'emballage.\nÉTAPE 2\nAssaisonner les filets de poulet avec du sel, du poivre et un peu d'huile d'olive.\nÉTAPE 3\nFaire chauffer un peu d'huile d'olive dans une grande poêle à feu moyen-élevé.\nÉTAPE 4\nFaire dorer les filets de poulet des deux côtés dans la poêle.\nÉTAPE 5\nVerser la sauce teriyaki dans la poêle et laisser mijoter pendant quelques minutes jusqu'à ce que le poulet soit bien enrobé et cuit à cœur.\nÉTAPE 6\nServir le poulet teriyaki chaud sur le riz blanc cuit."]
    temps_de_preparation = 10
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poulet_teriyaki_riz_blanc
def burritos_haricots_noirs_avocat(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "haricots noirs", "avocat", "tortillas de blé", "fromage râpé", "salsa"]
    ingredient = ["sel", "poivre", "huile d'olive", 400*multiplicateur_arrondi, "g de haricots noirs en conserve", 2*multiplicateur_arrondi, " avocats mûrs", 4*multiplicateur_arrondi, " tortillas de blé", 100*multiplicateur_arrondi, "g de fromage râpé", 100*multiplicateur_arrondi, "ml de salsa"]
    preparation = ["ÉTAPE 1\nÉgoutter et rincer les haricots noirs en conserve sous l'eau froide.\nÉTAPE 2\nÉcraser les avocats dans un bol avec une fourchette et assaisonner avec du sel et du poivre.\nÉTAPE 3\nChauffer les tortillas de blé dans une poêle ou au micro-ondes jusqu'à ce qu'elles soient chaudes et souples.\nÉTAPE 4\nÉtaler une cuillerée de purée d'avocat sur chaque tortilla.\nÉTAPE 5\nRépartir les haricots noirs égouttés sur le dessus de la purée d'avocat.\nÉTAPE 6\nSaupoudrer de fromage râpé sur les haricots noirs.\nÉTAPE 7\nAjouter une cuillerée de salsa sur le dessus.\nÉTAPE 8\nEnrouler les tortillas garnies pour former des burritos.\nÉTAPE 9\nServir chaud avec du salsa supplémentaire si désiré."]
    temps_de_preparation = 10
    temps_de_cuisson = 5
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

burritos_haricots_noirs_avocat
def salade_quinoa_poulet_grille(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "quinoa", "poulet", "tomates", "concombre", "avocat", "vinaigrette"]
    ingredient = ["sel", "poivre", "huile d'olive", 100*multiplicateur_arrondi, "g de quinoa", 2*multiplicateur_arrondi, " filets de poulet", 2*multiplicateur_arrondi, " tomates", 1*multiplicateur_arrondi, " concombre", 2*multiplicateur_arrondi, " avocats", " vinaigrette"]
    preparation = ["ÉTAPE 1\nFaire cuire le quinoa selon les instructions sur l'emballage.\nÉTAPE 2\nAssaisonner les filets de poulet avec du sel, du poivre et un peu d'huile d'olive.\nÉTAPE 3\nFaire chauffer un grill ou une poêle à feu moyen-élevé.\nÉTAPE 4\nFaire griller les filets de poulet pendant 4 à 5 minutes de chaque côté, ou jusqu'à ce qu'ils soient cuits à cœur.\nÉTAPE 5\nLaisser reposer les filets de poulet cuits pendant quelques minutes, puis les trancher en morceaux.\nÉTAPE 6\nCouper les tomates, le concombre et les avocats en dés.\nÉTAPE 7\nDans un grand saladier, mélanger le quinoa cuit, les morceaux de poulet grillé, les dés de tomates, de concombre et d'avocat.\nÉTAPE 8\nArroser la salade de vinaigrette et bien mélanger pour enrober tous les ingrédients.\nÉTAPE 9\nServir immédiatement ou réfrigérer pour servir frais."]
    temps_de_preparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_quinoa_poulet_grille
def pain_viande_puree_pommes_terre(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "viande hachée", "oignon", "ail", "flocons d'avoine", "œufs", "ketchup", "moutarde", "pommes de terre", "lait", "beurre"]
    ingredient = ["sel", "poivre", "huile d'olive", 500*multiplicateur_arrondi, "g de viande hachée", 1*multiplicateur_arrondi, " oignon", 2*multiplicateur_arrondi, " gousses d'ail", 100*multiplicateur_arrondi, "g de flocons d'avoine", 2*multiplicateur_arrondi, " œufs", 50*multiplicateur_arrondi, "ml de ketchup", 50*multiplicateur_arrondi, "ml de moutarde", 500*multiplicateur_arrondi, "g de pommes de terre", 100*multiplicateur_arrondi, "ml de lait", 50*multiplicateur_arrondi, "g de beurre"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 180°C.\nÉTAPE 2\nHacher finement l'oignon et l'ail.\nÉTAPE 3\nDans un grand bol, mélanger la viande hachée, l'oignon, l'ail, les flocons d'avoine, les œufs, le ketchup et la moutarde.\nÉTAPE 4\nPresser le mélange dans un moule à pain graissé et former un pain.\nÉTAPE 5\nCuire au four préchauffé pendant environ 45 minutes à 1 heure, ou jusqu'à ce que le pain de viande soit bien cuit au centre.\nÉTAPE 6\nPendant ce temps, éplucher et couper les pommes de terre en morceaux.\nÉTAPE 7\nFaire cuire les pommes de terre dans une casserole d'eau bouillante jusqu'à ce qu'elles soient tendres.\nÉTAPE 8\nÉgoutter les pommes de terre cuites et les écraser avec un peu de lait chaud et de beurre jusqu'à obtenir une purée lisse et crémeuse.\nÉTAPE 9\nServir le pain de viande chaud avec la purée de pommes de terre."]
    temps_de_preparation = 20
    temps_de_cuisson = 60
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

pain_viande_puree_pommes_terre
def pates_bolognaise(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "viande hachée", "oignon", "ail", "sauce tomate", "spaghettis", "parmesan râpé"]
    ingredient = ["sel", "poivre", "huile d'olive", 300*multiplicateur_arrondi, "g de viande hachée", 1*multiplicateur_arrondi, " oignon", 2*multiplicateur_arrondi, " gousses d'ail", 400*multiplicateur_arrondi, "ml de sauce tomate", 200*multiplicateur_arrondi, "g de spaghettis", 50*multiplicateur_arrondi, "g de parmesan râpé"]
    preparation = ["ÉTAPE 1\nFaire cuire les spaghettis selon les instructions sur l'emballage.\nÉTAPE 2\nPendant ce temps, faire chauffer un peu d'huile d'olive dans une grande poêle à feu moyen.\nÉTAPE 3\nHacher finement l'oignon et l'ail et les faire revenir dans la poêle jusqu'à ce qu'ils soient tendres.\nÉTAPE 4\nAjouter la viande hachée dans la poêle et la faire cuire jusqu'à ce qu'elle soit dorée et cuite à cœur.\nÉTAPE 5\nVerser la sauce tomate dans la poêle et laisser mijoter pendant quelques minutes.\nÉTAPE 6\nAssaisonner avec du sel et du poivre selon votre goût.\nÉTAPE 7\nÉgoutter les spaghettis cuits et les mélanger avec la sauce bolognaise dans la poêle.\nÉTAPE 8\nServir chaud avec du parmesan râpé saupoudré sur le dessus."]
    temps_de_preparation = 10
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

pates_bolognaise
def salade_poulet_cajun_mais_grille(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "poulet", "maïs en épi", "poivron rouge", "poivron vert", "oignon rouge", "laitue", "vinaigrette cajun"]
    ingredient = ["sel", "poivre", "huile d'olive", 2*multiplicateur_arrondi, " filets de poulet", 2*multiplicateur_arrondi, " épis de maïs", 1*multiplicateur_arrondi, " poivron rouge", 1*multiplicateur_arrondi, " poivron vert", 1*multiplicateur_arrondi, " oignon rouge", 1*multiplicateur_arrondi, " laitue", " vinaigrette cajun"]
    preparation = ["ÉTAPE 1\nAssaisonner les filets de poulet avec du sel, du poivre et un peu d'huile d'olive.\nÉTAPE 2\nFaire chauffer un grill ou une poêle à feu moyen-élevé.\nÉTAPE 3\nFaire griller les filets de poulet pendant 4 à 5 minutes de chaque côté, ou jusqu'à ce qu'ils soient cuits à cœur.\nÉTAPE 4\nPendant ce temps, faire griller les épis de maïs dans le même grill jusqu'à ce qu'ils soient dorés et légèrement grillés.\nÉTAPE 5\nCouper les poivrons rouges et verts en dés et hacher finement l'oignon rouge.\nÉTAPE 6\nDéchirer la laitue en morceaux dans un grand saladier.\nÉTAPE 7\nCouper les filets de poulet grillés en tranches et les ajouter au saladier.\nÉTAPE 8\nRetirer les grains de maïs grillés des épis à l'aide d'un couteau et les ajouter au saladier.\nÉTAPE 9\nArroser la salade de vinaigrette cajun et bien mélanger pour enrober tous les ingrédients.\nÉTAPE 10\nServir immédiatement."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_poulet_cajun_mais_grille
def riz_frit_legumes_oeufs_brouilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "riz cuit", "œufs", "oignon", "carotte", "pois verts", "sauce soja"]
    ingredient = ["sel", "poivre", "huile d'olive", 200*multiplicateur_arrondi, "g de riz cuit", 2*multiplicateur_arrondi, " œufs", 1*multiplicateur_arrondi, " oignon", 1*multiplicateur_arrondi, " carotte", 50*multiplicateur_arrondi, "g de pois verts", " sauce soja"]
    preparation = ["ÉTAPE 1\nFaire chauffer un peu d'huile d'olive dans une grande poêle ou un wok à feu moyen.\nÉTAPE 2\nAjouter l'oignon finement haché dans la poêle et le faire revenir jusqu'à ce qu'il soit tendre et translucide.\nÉTAPE 3\nIncorporer les carottes coupées en dés dans la poêle et les faire sauter jusqu'à ce qu'elles soient tendres.\nÉTAPE 4\nAjouter les pois verts dans la poêle et les faire sauter pendant quelques minutes de plus.\nÉTAPE 5\nAjouter le riz cuit dans la poêle et le mélanger avec les légumes.\nÉTAPE 6\nPousser le riz et les légumes sur le côté de la poêle et verser les œufs battus dans l'espace vide.\nÉTAPE 7\nRemuer doucement les œufs jusqu'à ce qu'ils commencent à prendre et se solidifier, puis mélanger avec le riz et les légumes.\nÉTAPE 8\nArroser le riz frit de sauce soja et bien mélanger pour enrober tous les ingrédients.\nÉTAPE 9\nServir chaud avec des tranches de citron vert si désiré."]
    temps_de_preparation = 10
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

riz_frit_legumes_oeufs_brouilles
def tacos_poulet_salsa(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "poulet", "tortillas de maïs", "tomates", "oignon", "coriandre", "jalapeño", "jus de citron vert"]
    ingredient = ["sel", "poivre", "huile d'olive", 2*multiplicateur_arrondi, " filets de poulet", 8*multiplicateur_arrondi, " tortillas de maïs", 4*multiplicateur_arrondi, " tomates", 1*multiplicateur_arrondi, " oignon", 1*multiplicateur_arrondi, " bouquet de coriandre", 1*multiplicateur_arrondi, " jalapeño", 1*multiplicateur_arrondi, " jus de citron vert"]
    preparation = ["ÉTAPE 1\nAssaisonner les filets de poulet avec du sel, du poivre et un peu d'huile d'olive.\nÉTAPE 2\nFaire chauffer un grill ou une poêle à feu moyen-élevé.\nÉTAPE 3\nFaire griller les filets de poulet pendant 4 à 5 minutes de chaque côté, ou jusqu'à ce qu'ils soient cuits à cœur.\nÉTAPE 4\nLaisser reposer les filets de poulet cuits pendant quelques minutes, puis les trancher en morceaux.\nÉTAPE 5\nPendant ce temps, chauffer les tortillas de maïs dans une poêle ou au micro-ondes jusqu'à ce qu'elles soient chaudes et souples.\nÉTAPE 6\nCouper les tomates, l'oignon, la coriandre et le jalapeño en dés pour la salsa.\nÉTAPE 7\nMélanger les tomates, l'oignon, la coriandre et le jalapeño dans un bol avec le jus de citron vert pour faire la salsa.\nÉTAPE 8\nAssembler les tacos en plaçant des morceaux de poulet grillé sur les tortillas de maïs chaudes et en garnissant de salsa.\nÉTAPE 9\nServir immédiatement."]
    temps_de_preparation = 15
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

tacos_poulet_salsa
def boeuf_saute_legumes_riz_brun(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "bœuf", "poivron rouge", "poivron vert", "oignon", "brocoli", "sauce soja", "miel", "vinaigre de riz", "ail", "gingembre", "riz brun"]
    ingredient = ["sel", "poivre", "huile d'olive", 400*multiplicateur_arrondi, "g de bœuf en lanières", 1*multiplicateur_arrondi, " poivron rouge", 1*multiplicateur_arrondi, " poivron vert", 1*multiplicateur_arrondi, " oignon", 1*multiplicateur_arrondi, " brocoli", 50*multiplicateur_arrondi, "ml de sauce soja", 30*multiplicateur_arrondi, "ml de miel", 30*multiplicateur_arrondi, "ml de vinaigre de riz", 2*multiplicateur_arrondi, " gousses d'ail", 1*multiplicateur_arrondi, " morceau de gingembre frais", 100*multiplicateur_arrondi, "g de riz brun"]
    preparation = ["ÉTAPE 1\nFaire cuire le riz brun selon les instructions sur l'emballage.\nÉTAPE 2\nPendant ce temps, faire chauffer un peu d'huile d'olive dans une grande poêle ou un wok à feu moyen-élevé.\nÉTAPE 3\nAjouter les lanières de bœuf dans la poêle et les faire sauter jusqu'à ce qu'elles soient dorées et cuites à votre goût.\nÉTAPE 4\nRetirer le bœuf de la poêle et le mettre de côté.\nÉTAPE 5\nDans la même poêle, ajouter un peu plus d'huile d'olive si nécessaire et faire sauter les poivrons, l'oignon et le brocoli jusqu'à ce qu'ils soient tendres mais encore croquants.\nÉTAPE 6\nPendant la cuisson des légumes, préparer la sauce en mélangeant la sauce soja, le miel, le vinaigre de riz, l'ail émincé et le gingembre râpé dans un petit bol.\nÉTAPE 7\nUne fois les légumes tendres, remettre le bœuf dans la poêle et verser la sauce sur le tout.\nÉTAPE 8\nLaisser mijoter pendant quelques minutes jusqu'à ce que la sauce épaississe légèrement et enrobe la viande et les légumes.\nÉTAPE 9\nServir chaud sur le riz brun cuit."]
    temps_de_preparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

boeuf_saute_legumes_riz_brun
def poisson_grille_asperges_pommes_terre(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "filet de poisson blanc", "asperges", "pommes de terre", "ail", "persil", "citron", "beurre"]
    ingredient = ["sel", "poivre", "huile d'olive", 2*multiplicateur_arrondi, " filets de poisson blanc", 200*multiplicateur_arrondi, "g d'asperges", 300*multiplicateur_arrondi, "g de pommes de terre", 2*multiplicateur_arrondi, " gousses d'ail", 1*multiplicateur_arrondi, " bouquet de persil frais", 1*multiplicateur_arrondi, " citron", 50*multiplicateur_arrondi, "g de beurre"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 200°C.\nÉTAPE 2\nLaver et éplucher les pommes de terre, puis les couper en quartiers.\nÉTAPE 3\nDisposer les quartiers de pommes de terre sur une plaque de cuisson et les arroser d'huile d'olive, d'ail émincé, de persil haché, de sel et de poivre.\nÉTAPE 4\nFaire cuire au four préchauffé pendant environ 30 à 40 minutes, ou jusqu'à ce que les pommes de terre soient dorées et tendres.\nÉTAPE 5\nPendant ce temps, préparer les filets de poisson en les assaisonnant avec du sel, du poivre et un peu de jus de citron.\nÉTAPE 6\nLaver et éplucher les asperges, puis les disposer sur une autre plaque de cuisson.\nÉTAPE 7\nArroser les asperges d'huile d'olive et assaisonner avec du sel et du poivre.\nÉTAPE 8\nFaire griller les asperges au four préchauffé pendant environ 10 à 15 minutes, ou jusqu'à ce qu'elles soient tendres mais encore croquantes.\nÉTAPE 9\nPendant les dernières minutes de cuisson des pommes de terre et des asperges, faire chauffer un peu de beurre dans une poêle et y faire cuire les filets de poisson pendant 3 à 4 minutes de chaque côté, ou jusqu'à ce qu'ils soient cuits à cœur.\nÉTAPE 10\nServir chaud les filets de poisson grillés avec les asperges et les pommes de terre au four."]
    temps_de_preparation = 15
    temps_de_cuisson = 40
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poisson_grille_asperges_pommes_terre
def wrap_thon_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "thon en conserve", "tortillas de blé", "concombre", "tomate", "poivron rouge", "salade", "mayonnaise"]
    ingredient = ["sel", "poivre", "huile d'olive", 1*multiplicateur_arrondi, " boîte de thon en conserve", 1*multiplicateur_arrondi, " tortillas de blé", 0.5*multiplicateur_arrondi, " concombre", 1*multiplicateur_arrondi, " tomate", 0.5*multiplicateur_arrondi, " poivron rouge", 1*multiplicateur_arrondi, " poignée de salade", " mayonnaise"]
    preparation = ["ÉTAPE 1\nÉgoutter le thon en conserve et l'émietter dans un bol.\nÉTAPE 2\nCouper le concombre, la tomate et le poivron rouge en fines tranches.\nÉTAPE 3\nChauffer les tortillas de blé dans une poêle ou au micro-ondes jusqu'à ce qu'elles soient chaudes et souples.\nÉTAPE 4\nÉtaler une fine couche de mayonnaise sur chaque tortilla.\nÉTAPE 5\nRépartir le thon émietté sur les tortillas.\nÉTAPE 6\nGarnir de tranches de concombre, de tomate, de poivron rouge et de salade.\nÉTAPE 7\nSaler et poivrer selon votre goût.\nÉTAPE 8\nEnrouler les tortillas pour former des wraps.\nÉTAPE 9\nCouper les wraps en deux et servir immédiatement."]
    temps_de_preparation = 10
    temps_de_cuisson = 5
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

wrap_thon_legumes
def salade_crevettes_avocat(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "crevettes", "avocat", "concombre", "tomate", "oignon rouge", "salade", "vinaigrette"]
    ingredient = ["sel", "poivre", "huile d'olive", 200*multiplicateur_arrondi, "g de crevettes décortiquées", 2*multiplicateur_arrondi, " avocats mûrs", 0.5*multiplicateur_arrondi, " concombre", 1*multiplicateur_arrondi, " tomate", 0.5*multiplicateur_arrondi, " oignon rouge", 1*multiplicateur_arrondi, " poignée de salade", " vinaigrette"]
    preparation = ["ÉTAPE 1\nAssaisonner les crevettes décortiquées avec du sel, du poivre et un peu d'huile d'olive.\nÉTAPE 2\nFaire chauffer un grill ou une poêle à feu moyen-élevé.\nÉTAPE 3\nFaire griller les crevettes pendant 2 à 3 minutes de chaque côté, ou jusqu'à ce qu'elles soient roses et cuites à votre goût.\nÉTAPE 4\nPendant ce temps, préparer les légumes : couper l'avocat, le concombre, la tomate et l'oignon rouge en dés.\nÉTAPE 5\nDéposer une poignée de salade dans chaque assiette de service.\nÉTAPE 6\nRépartir les crevettes grillées sur la salade.\nÉTAPE 7\nAjouter les dés d'avocat, de concombre, de tomate et d'oignon rouge sur le dessus.\nÉTAPE 8\nArroser la salade de vinaigrette et servir immédiatement."]
    temps_de_preparation = 15
    temps_de_cuisson = 5
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_crevettes_avocat
def pates_primavera(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "pâtes", "asperges", "petits pois", "poivron rouge", "tomates cerises", "ail", "basilic frais", "parmesan râpé"]
    ingredient = ["sel", "poivre", "huile d'olive", 200*multiplicateur_arrondi, "g de pâtes", 1*multiplicateur_arrondi, " botte d'asperges", 100*multiplicateur_arrondi, "g de petits pois surgelés", 1*multiplicateur_arrondi, " poivron rouge", 100*multiplicateur_arrondi, "g de tomates cerises", 2*multiplicateur_arrondi, " gousses d'ail", 4*multiplicateur_arrondi, " feuilles de basilic frais", 50*multiplicateur_arrondi, "g de parmesan râpé"]
    preparation = ["ÉTAPE 1\nFaire cuire les pâtes selon les instructions sur l'emballage.\nÉTAPE 2\nPendant ce temps, préparer les légumes : couper les asperges en tronçons, émincer le poivron rouge, hacher l'ail et couper les tomates cerises en deux.\nÉTAPE 3\nFaire chauffer un peu d'huile d'olive dans une grande poêle à feu moyen.\nÉTAPE 4\nFaire sauter les asperges, les petits pois, le poivron rouge et l'ail dans la poêle pendant quelques minutes, jusqu'à ce qu'ils soient tendres mais encore croquants.\nÉTAPE 5\nAjouter les tomates cerises dans la poêle et les faire sauter pendant quelques minutes de plus, jusqu'à ce qu'elles commencent à ramollir.\nÉTAPE 6\nÉgoutter les pâtes cuites et les ajouter dans la poêle avec les légumes.\nÉTAPE 7\nMélanger les pâtes et les légumes avec un peu d'huile d'olive supplémentaire si nécessaire.\nÉTAPE 8\nIncorporer les feuilles de basilic frais hachées et le parmesan râpé dans les pâtes.\nÉTAPE 9\nAssaisonner avec du sel et du poivre selon votre goût.\nÉTAPE 10\nServir chaud, garni de plus de parmesan râpé si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

pates_primavera
def salade_quinoa_legumes_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "quinoa", "courgette", "aubergine", "poivron rouge", "poivron jaune", "tomates cerises", "épinards", "vinaigrette balsamique"]
    ingredient = ["sel", "poivre", "huile d'olive", 100*multiplicateur_arrondi, "g de quinoa", 1*multiplicateur_arrondi, " courgette", 1*multiplicateur_arrondi, " aubergine", 1*multiplicateur_arrondi, " poivron rouge", 1*multiplicateur_arrondi, " poivron jaune", 100*multiplicateur_arrondi, "g de tomates cerises", 1*multiplicateur_arrondi, " poignée d'épinards", " vinaigrette balsamique"]
    preparation = ["ÉTAPE 1\nRincer le quinoa à l'eau froide dans une passoire.\nÉTAPE 2\nFaire cuire le quinoa selon les instructions sur l'emballage.\nÉTAPE 3\nPendant ce temps, préparer les légumes : couper la courgette, l'aubergine et les poivrons en tranches.\nÉTAPE 4\nChauffer un grill ou une poêle à feu moyen-élevé.\nÉTAPE 5\nBadigeonner les tranches de légumes d'huile d'olive et les faire griller pendant quelques minutes de chaque côté, jusqu'à ce qu'ils soient tendres et marqués par le grill.\nÉTAPE 6\nCouper les tomates cerises en deux et hacher grossièrement les épinards.\nÉTAPE 7\nDans un grand saladier, mélanger le quinoa cuit avec les légumes grillés, les tomates cerises et les épinards.\nÉTAPE 8\nArroser la salade de vinaigrette balsamique et bien mélanger pour enrober tous les ingrédients.\nÉTAPE 9\nServir chaud ou à température ambiante."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_quinoa_legumes_grilles
def poulet_roti_pommes_terre_haricots_verts(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "poulet entier", "pommes de terre", "haricots verts", "ail", "thym", "romarin", "citron"]
    ingredient = ["sel", "poivre", "huile d'olive", 1*multiplicateur_arrondi, " poulet entier", 500*multiplicateur_arrondi, "g de pommes de terre", 200*multiplicateur_arrondi, "g de haricots verts", 4*multiplicateur_arrondi, " gousses d'ail", 2*multiplicateur_arrondi, " branches de thym frais", 2*multiplicateur_arrondi, " branches de romarin frais", 1*multiplicateur_arrondi, " citron"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 200°C.\nÉTAPE 2\nRincer le poulet à l'eau froide et l'assécher avec du papier absorbant.\nÉTAPE 3\nFrotter le poulet avec de l'huile d'olive, du sel, du poivre, de l'ail émincé, du thym et du romarin frais.\nÉTAPE 4\nCouper le citron en tranches et les glisser à l'intérieur du poulet.\nÉTAPE 5\nPlacer le poulet dans un plat allant au four.\nÉTAPE 6\nFaire cuire le poulet au four préchauffé pendant environ 1 heure et 15 minutes, ou jusqu'à ce qu'il soit bien doré et que les jus s'écoulent clairs lorsqu'il est piqué avec une fourchette.\nÉTAPE 7\nPendant ce temps, laver et couper les pommes de terre en quartiers.\nÉTAPE 8\nFaire bouillir les pommes de terre dans une casserole d'eau salée pendant 15 à 20 minutes, ou jusqu'à ce qu'elles soient tendres.\nÉTAPE 9\nPendant les 15 dernières minutes de cuisson du poulet, ajouter les pommes de terre et les haricots verts autour du poulet dans le plat et les faire cuire jusqu'à ce qu'ils soient tendres.\nÉTAPE 10\nServir le poulet rôti chaud avec les pommes de terre, les haricots verts et les tranches de citron."]
    temps_de_preparation = 20
    temps_de_cuisson = 75
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poulet_roti_pommes_terre_haricots_verts
def risotto_champignons(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "oignon", "ail", "champignons", "riz arborio", "vin blanc", "bouillon de légumes", "parmesan râpé", "beurre"]
    ingredient = ["sel", "poivre", "huile d'olive", 1*multiplicateur_arrondi, " oignon", 2*multiplicateur_arrondi, " gousses d'ail", 200*multiplicateur_arrondi, "g de champignons", 200*multiplicateur_arrondi, "g de riz arborio", 100*multiplicateur_arrondi, "ml de vin blanc", 500*multiplicateur_arrondi, "ml de bouillon de légumes chaud", 50*multiplicateur_arrondi, "g de parmesan râpé", 25*multiplicateur_arrondi, "g de beurre"]
    preparation = ["ÉTAPE 1\nDans une casserole, chauffer le bouillon de légumes et le maintenir chaud à feu doux.\nÉTAPE 2\nPendant ce temps, dans une grande poêle, chauffer un peu d'huile d'olive à feu moyen.\nÉTAPE 3\nAjouter l'oignon finement haché et l'ail émincé dans la poêle et faire revenir jusqu'à ce qu'ils soient tendres et translucides.\nÉTAPE 4\nAjouter les champignons tranchés dans la poêle et faire cuire jusqu'à ce qu'ils commencent à dorer.\nÉTAPE 5\nAjouter le riz arborio dans la poêle et mélanger pour enrober les grains d'huile.\nÉTAPE 6\nVerser le vin blanc dans la poêle et laisser mijoter jusqu'à ce qu'il soit presque entièrement absorbé.\nÉTAPE 7\nCommencer à ajouter une louche de bouillon chaud dans la poêle, en remuant constamment, jusqu'à ce que le liquide soit presque entièrement absorbé.\nÉTAPE 8\nContinuer à ajouter du bouillon une louche à la fois, en remuant constamment, jusqu'à ce que le riz soit crémeux et cuit al dente.\nÉTAPE 9\nRetirer la poêle du feu et incorporer le parmesan râpé et le beurre dans le risotto jusqu'à ce qu'ils soient fondus et bien mélangés.\nÉTAPE 10\nAssaisonner avec du sel et du poivre selon votre goût.\nÉTAPE 11\nServir chaud, garni de parmesan supplémentaire si désiré."]
    temps_de_preparation = 10
    temps_de_cuisson = 30
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

risotto_champignons
def poulet_teriyaki_riz_legumes_sautes(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "poulet", "sauce teriyaki", "riz", "brocoli", "carottes", "oignon", "ail", "gingembre"]
    ingredient = ["sel", "poivre", "huile d'olive", 2*multiplicateur_arrondi, " filets de poulet", 100*multiplicateur_arrondi, "ml de sauce teriyaki", 200*multiplicateur_arrondi, "g de riz", 1*multiplicateur_arrondi, " têtes de brocoli", 2*multiplicateur_arrondi, " carottes", 1*multiplicateur_arrondi, " oignon", 2*multiplicateur_arrondi, " gousses d'ail", 1*multiplicateur_arrondi, " morceau de gingembre frais"]
    preparation = ["ÉTAPE 1\nFaire cuire le riz selon les instructions sur l'emballage.\nÉTAPE 2\nPendant ce temps, couper les filets de poulet en dés et les faire mariner dans la sauce teriyaki pendant au moins 15 minutes.\nÉTAPE 3\nChauffer un peu d'huile d'olive dans une grande poêle ou un wok à feu moyen-élevé.\nÉTAPE 4\nAjouter les dés de poulet dans la poêle et les faire sauter jusqu'à ce qu'ils soient dorés et cuits à travers.\nÉTAPE 5\nRetirer le poulet de la poêle et le mettre de côté.\nÉTAPE 6\nDans la même poêle, ajouter un peu plus d'huile d'olive si nécessaire et faire sauter l'oignon émincé, l'ail émincé et le gingembre râpé jusqu'à ce qu'ils soient parfumés.\nÉTAPE 7\nAjouter les carottes coupées en julienne et les têtes de brocoli dans la poêle et les faire sauter jusqu'à ce qu'ils soient tendres mais encore croquants.\nÉTAPE 8\nRemettre le poulet dans la poêle et mélanger avec les légumes.\nÉTAPE 9\nServir chaud sur le riz cuit."]
    temps_de_preparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poulet_teriyaki_riz_legumes_sautes
def pizza_maison_viande_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "pâte à pizza", "sauce tomate", "mozzarella râpée", "oignon", "poivron rouge", "champignons", "jambon", "origan", "basilic frais"]
    ingredient = ["sel", "poivre", "huile d'olive", 1*multiplicateur_arrondi, " pâte à pizza", 100*multiplicateur_arrondi, "ml de sauce tomate", 100*multiplicateur_arrondi, "g de mozzarella râpée", 1*multiplicateur_arrondi, " oignon", 1*multiplicateur_arrondi, " poivron rouge", 100*multiplicateur_arrondi, "g de champignons", 100*multiplicateur_arrondi, "g de jambon", " origan", " basilic frais"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à la température recommandée pour la pâte à pizza.\nÉTAPE 2\nÉtaler la pâte à pizza sur une plaque de cuisson préalablement graissée ou recouverte de papier sulfurisé.\nÉTAPE 3\nÉtaler une fine couche de sauce tomate sur la pâte à pizza.\nÉTAPE 4\nRépartir la mozzarella râpée sur la sauce tomate.\nÉTAPE 5\nCouper l'oignon, le poivron rouge, les champignons et le jambon en dés ou en tranches fines selon vos préférences.\nÉTAPE 6\nDisposer les légumes et la viande sur la pizza en les répartissant uniformément.\nÉTAPE 7\nSaupoudrer d'origan séché et de feuilles de basilic frais ciselées.\nÉTAPE 8\nArroser d'un filet d'huile d'olive.\nÉTAPE 9\nEnfourner la pizza dans le four préchauffé et cuire selon les instructions de la pâte à pizza, jusqu'à ce que la croûte soit dorée et croustillante et que le fromage soit fondu et doré.\nÉTAPE 10\nRetirer la pizza du four, laisser refroidir légèrement, puis découper et servir."]
    temps_de_preparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

pizza_maison_viande_legumes
def tacos_poulet_grille(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "poulet", "mélange d'épices pour tacos", "tortillas de maïs", "avocat", "tomate", "oignon rouge", "coriandre fraîche", "yaourt grec"]
    ingredient = ["sel", "poivre", "huile d'olive", 2*multiplicateur_arrondi, " filets de poulet", " mélange d'épices pour tacos", 8*multiplicateur_arrondi, " tortillas de maïs", 2*multiplicateur_arrondi, " avocats mûrs", 2*multiplicateur_arrondi, " tomates", 1*multiplicateur_arrondi, " oignon rouge", 1*multiplicateur_arrondi, " bouquet de coriandre fraîche", 100*multiplicateur_arrondi, "g de yaourt grec"]
    preparation = ["ÉTAPE 1\nDans un bol, mélanger les filets de poulet avec un peu d'huile d'olive et le mélange d'épices pour tacos jusqu'à ce qu'ils soient bien enrobés.\nÉTAPE 2\nChauffer une poêle grill ou une poêle à feu moyen-élevé.\nÉTAPE 3\nFaire griller les filets de poulet dans la poêle pendant environ 6 à 8 minutes de chaque côté, ou jusqu'à ce qu'ils soient bien cuits et dorés.\nÉTAPE 4\nPendant ce temps, chauffer les tortillas de maïs dans une autre poêle ou au micro-ondes jusqu'à ce qu'elles soient chaudes et souples.\nÉTAPE 5\nCouper les avocats en tranches, les tomates en dés, l'oignon rouge en fines rondelles et hacher la coriandre fraîche.\nÉTAPE 6\nCouper les filets de poulet grillés en fines lanières.\nÉTAPE 7\nGarnir chaque tortilla de morceaux de poulet grillé, de tranches d'avocat, de dés de tomate, de rondelles d'oignon rouge et de feuilles de coriandre fraîche.\nÉTAPE 8\nArroser de yaourt grec et servir immédiatement."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

tacos_poulet_grille
def salade_poulet_cesar(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "citron", "poulet", "laitue romaine", "croûtons", "parmesan", "sauce César"]
    ingredient = ["sel", "poivre", "huile d'olive", 1*multiplicateur_arrondi, " citron", 2*multiplicateur_arrondi, " filets de poulet", 1*multiplicateur_arrondi, " laitue romaine", 100*multiplicateur_arrondi, "g de croûtons", 50*multiplicateur_arrondi, "g de parmesan râpé", 50*multiplicateur_arrondi, "ml de sauce César"]
    preparation = ["ÉTAPE 1\nPréparer la marinade pour le poulet en mélangeant le jus de citron avec un peu d'huile d'olive, du sel et du poivre.\nÉTAPE 2\nCouper les filets de poulet en lanières et les faire mariner dans la marinade pendant au moins 30 minutes.\nÉTAPE 3\nPendant ce temps, laver et couper la laitue romaine en morceaux et râper le parmesan si ce n'est pas déjà fait.\nÉTAPE 4\nChauffer un peu d'huile d'olive dans une poêle à feu moyen-élevé.\nÉTAPE 5\nFaire cuire les lanières de poulet mariné dans la poêle jusqu'à ce qu'elles soient bien dorées et cuites à travers.\nÉTAPE 6\nRetirer le poulet de la poêle et le laisser refroidir légèrement.\nÉTAPE 7\nAssembler la salade en disposant la laitue romaine dans un grand saladier et en ajoutant les lanières de poulet cuites, les croûtons et le parmesan râpé.\nÉTAPE 8\nArroser la salade de sauce César et mélanger pour enrober tous les ingrédients.\nÉTAPE 9\nServir frais, garni de quartiers de citron pour plus de saveur."]
    temps_de_preparation = 15
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_poulet_cesar
def wrap_saumon_avocat(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "citron", "saumon fumé", "tortillas de blé", "avocat", "concombre", "salade", "yaourt grec", "aneth frais"]
    ingredient = ["sel", "poivre", "citron", 100*multiplicateur_arrondi, "g de saumon fumé", 2*multiplicateur_arrondi, " tortillas de blé", 2*multiplicateur_arrondi, " avocats mûrs", 1*multiplicateur_arrondi, " concombre", 1*multiplicateur_arrondi, " poignée de salade", 100*multiplicateur_arrondi, "g de yaourt grec", 1*multiplicateur_arrondi, " bouquet d'aneth frais"]
    preparation = ["ÉTAPE 1\nCouper les avocats en tranches et les arroser de jus de citron pour éviter qu'ils ne brunissent.\nÉTAPE 2\nÉtaler les tortillas de blé sur une surface de travail propre.\nÉTAPE 3\nRépartir les tranches de saumon fumé sur les tortillas.\nÉTAPE 4\nDisposer les tranches d'avocat, les tranches de concombre et la salade sur le saumon fumé.\nÉTAPE 5\nDans un petit bol, mélanger le yaourt grec avec du sel, du poivre et de l'aneth frais haché.\nÉTAPE 6\nÉtaler une couche de sauce au yaourt sur les ingrédients dans chaque tortilla.\nÉTAPE 7\nRouler les tortillas fermement pour former des wraps.\nÉTAPE 8\nCouper les wraps en deux ou en tranches avant de les servir, si désiré.\nÉTAPE 9\nServir frais avec des quartiers de citron pour plus de saveur."]
    temps_de_preparation = 10
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous
wrap_saumon_avocat

def pain_perdu_fruits_cannelle(multiplicateur_arrondi):
    ingredient_pour_algo = ["pain rassis", "œufs", "lait", "sucre", "cannelle", "beurre", "banane", "fraises", "sirop d'érable"]
    ingredient = [4*multiplicateur_arrondi, " tranches de pain rassis", 2*multiplicateur_arrondi, " œufs", 100*multiplicateur_arrondi, "ml de lait", 2*multiplicateur_arrondi, " cuillères à soupe de sucre", " pincée de cannelle", 25*multiplicateur_arrondi, "g de beurre", 1*multiplicateur_arrondi, " banane", 100*multiplicateur_arrondi, "g de fraises", 2*multiplicateur_arrondi, " cuillères à soupe de sirop d'érable"]
    preparation = ["ÉTAPE 1\nDans un bol peu profond, fouetter les œufs avec le lait, le sucre et la cannelle.\nÉTAPE 2\nTremper les tranches de pain rassis dans le mélange d'œufs et de lait, en les retournant pour qu'elles soient bien imbibées.\nÉTAPE 3\nFaire fondre une partie du beurre dans une poêle à feu moyen.\nÉTAPE 4\nFaire cuire les tranches de pain imbibées dans la poêle chaude jusqu'à ce qu'elles soient dorées et croustillantes des deux côtés.\nÉTAPE 5\nPendant ce temps, couper la banane en rondelles et les fraises en quartiers.\nÉTAPE 6\nServir les tranches de pain perdu chaudes avec les rondelles de banane et les quartiers de fraises.\nÉTAPE 7\nArroser de sirop d'érable et saupoudrer de cannelle supplémentaire si désiré."]
    temps_de_preparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

pain_perdu_fruits_cannelle
def omelette_legumes_fromage(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs", "sel", "poivre", "beurre", "oignon", "poivron", "champignons", "tomate", "fromage râpé"]
    ingredient = [4*multiplicateur_arrondi, " œufs", " sel", " poivre", 25*multiplicateur_arrondi, "g de beurre", 1*multiplicateur_arrondi, " oignon", 1*multiplicateur_arrondi, " poivron", 100*multiplicateur_arrondi, "g de champignons", 1*multiplicateur_arrondi, " tomate", 50*multiplicateur_arrondi, "g de fromage râpé"]
    preparation = ["ÉTAPE 1\nDans un bol, battre les œufs avec du sel et du poivre.\nÉTAPE 2\nDans une poêle antiadhésive, faire fondre le beurre à feu moyen.\nÉTAPE 3\nAjouter l'oignon émincé, le poivron coupé en dés et les champignons tranchés dans la poêle et faire sauter jusqu'à ce qu'ils soient tendres.\nÉTAPE 4\nAjouter la tomate coupée en dés dans la poêle et cuire pendant quelques minutes de plus.\nÉTAPE 5\nVerser les œufs battus dans la poêle avec les légumes et remuer doucement pour répartir les légumes de manière uniforme.\nÉTAPE 6\nCuire l'omelette jusqu'à ce qu'elle soit prise sur le dessus mais encore légèrement baveuse au centre.\nÉTAPE 7\nSaupoudrer de fromage râpé sur le dessus de l'omelette et laisser fondre légèrement.\nÉTAPE 8\nPlier l'omelette en deux et laisser cuire encore quelques instants.\nÉTAPE 9\nTransférer l'omelette dans une assiette et servir chaud."]
    temps_de_preparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

omelette_legumes_fromage
def pancakes_myrtilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["farine", "sucre", "levure chimique", "sel", "lait", "œufs", "huile végétale", "myrtilles", "sirop d'érable"]
    ingredient = [150*multiplicateur_arrondi, "g de farine", 2*multiplicateur_arrondi, " cuillères à soupe de sucre", 2*multiplicateur_arrondi, " cuillères à café de levure chimique", " pincée de sel", 150*multiplicateur_arrondi, "ml de lait", 2*multiplicateur_arrondi, " œufs", 2*multiplicateur_arrondi, " cuillères à soupe d'huile végétale", 100*multiplicateur_arrondi, "g de myrtilles", " sirop d'érable"]
    preparation = ["ÉTAPE 1\nDans un grand bol, mélanger la farine, le sucre, la levure chimique et le sel.\nÉTAPE 2\nDans un autre bol, battre les œufs avec le lait et l'huile végétale.\nÉTAPE 3\nVerser les ingrédients liquides dans les ingrédients secs et remuer jusqu'à ce que la pâte soit lisse.\nÉTAPE 4\nIncorporer délicatement les myrtilles dans la pâte à pancakes.\nÉTAPE 5\nChauffer une poêle ou une plaque chauffante à feu moyen et y verser un peu de pâte à pancakes pour former des cercles.\nÉTAPE 6\nCuire les pancakes pendant environ 2 à 3 minutes de chaque côté, jusqu'à ce qu'ils soient dorés et gonflés.\nÉTAPE 7\nTransférer les pancakes cuits dans une assiette et les garder au chaud pendant que vous faites cuire le reste de la pâte.\nÉTAPE 8\nServir les pancakes chauds avec du sirop d'érable."]
    temps_de_preparation = 10
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

pancakes_myrtilles

def toast_avocat_oeufs_poches(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs", "vinaigre blanc", "avocat", "pain de blé entier", "citron", "sel", "poivre", "piment rouge en flocons"]
    ingredient = [2*multiplicateur_arrondi, " œufs", 1*multiplicateur_arrondi, " cuillère à soupe de vinaigre blanc", 1*multiplicateur_arrondi, " avocat mûr", 2*multiplicateur_arrondi, " tranches de pain de blé entier", 1*multiplicateur_arrondi, " citron", " sel", " poivre", " pincée de piment rouge en flocons"]
    preparation = ["ÉTAPE 1\nPorter une casserole d'eau à ébullition et ajouter le vinaigre blanc.\nÉTAPE 2\nCasser un œuf dans une tasse ou un bol.\nÉTAPE 3\nRemuer l'eau bouillante pour créer un tourbillon et déposer délicatement l'œuf dans le tourbillon pour former un œuf poché.\nÉTAPE 4\nLaisser cuire l'œuf poché pendant environ 3 à 4 minutes, jusqu'à ce que le blanc soit pris mais le jaune encore coulant.\nÉTAPE 5\nPendant ce temps, écraser l'avocat avec une fourchette et l'assaisonner avec du jus de citron, du sel, du poivre et du piment rouge en flocons selon vos goûts.\nÉTAPE 6\nFaire griller les tranches de pain de blé entier.\nÉTAPE 7\nÉtaler la purée d'avocat sur les tranches de pain grillé.\nÉTAPE 8\nDéposer délicatement l'œuf poché sur le dessus de l'avocat.\nÉTAPE 9\nServir chaud et déguster immédiatement."]
    temps_de_preparation = 10
    temps_de_cuisson = 5
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

toast_avocat_oeufs_poches
def muffins_pepites_chocolat_bananes(multiplicateur_arrondi):
    ingredient_pour_algo = ["bananes", "œufs", "huile de coco", "lait d'amande", "miel", "farine d'amande", "poudre à lever", "sel", "pépites de chocolat noir"]
    ingredient = [2*multiplicateur_arrondi, " bananes mûres", 2*multiplicateur_arrondi, " œufs", 50*multiplicateur_arrondi, "ml d'huile de coco fondue", 100*multiplicateur_arrondi, "ml de lait d'amande", 2*multiplicateur_arrondi, " cuillères à soupe de miel", 200*multiplicateur_arrondi, "g de farine d'amande", 1*multiplicateur_arrondi, " cuillère à café de poudre à lever", " pincée de sel", 100*multiplicateur_arrondi, "g de pépites de chocolat noir"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 180°C (350°F) et tapisser un moule à muffins de caissettes en papier.\nÉTAPE 2\nDans un grand bol, écraser les bananes à la fourchette jusqu'à obtenir une purée lisse.\nÉTAPE 3\nAjouter les œufs, l'huile de coco fondue, le lait d'amande et le miel dans le bol avec la purée de bananes et mélanger jusqu'à ce que le tout soit bien combiné.\nÉTAPE 4\nAjouter la farine d'amande, la poudre à lever et le sel dans le bol avec le mélange liquide et mélanger jusqu'à ce que la pâte à muffins soit homogène.\nÉTAPE 5\nIncorporer délicatement les pépites de chocolat noir dans la pâte à muffins.\nÉTAPE 6\nRépartir la pâte à muffins dans les caissettes en papier préparées, en les remplissant aux deux tiers.\nÉTAPE 7\nCuire les muffins au four préchauffé pendant environ 20 à 25 minutes, ou jusqu'à ce qu'ils soient dorés et qu'un cure-dent inséré au centre en ressorte propre.\nÉTAPE 8\nLaisser les muffins refroidir dans le moule pendant quelques minutes avant de les transférer sur une grille pour les laisser refroidir complètement.\nÉTAPE 9\nServir les muffins aux pépites de chocolat et aux bananes tièdes ou à température ambiante."]
    temps_de_preparation = 10
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

muffins_pepites_chocolat_bananes

def pain_bananes_noix(multiplicateur_arrondi):
    ingredient_pour_algo = ["bananes mûres", "œufs", "huile de coco", "sucre", "extrait de vanille", "farine tout usage", "bicarbonate de soude", "sel", "noix", "flocons de noix de coco"]
    ingredient = [3*multiplicateur_arrondi, " bananes mûres écrasées", 2*multiplicateur_arrondi, " œufs", 100*multiplicateur_arrondi, "ml d'huile de coco fondue", 200*multiplicateur_arrondi, "g de sucre", 1*multiplicateur_arrondi, " cuillère à café d'extrait de vanille", 200*multiplicateur_arrondi, "g de farine tout usage", 1*multiplicateur_arrondi, " cuillère à café de bicarbonate de soude", " pincée de sel", 100*multiplicateur_arrondi, "g de noix hachées", 50*multiplicateur_arrondi, "g de flocons de noix de coco"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 180°C (thermostat 6) et graisser légèrement un moule à pain.\nÉTAPE 2\nDans un grand bol, mélanger les bananes écrasées avec les œufs, l'huile de coco fondue, le sucre et l'extrait de vanille.\nÉTAPE 3\nAjouter la farine tout usage, le bicarbonate de soude et le sel, et mélanger jusqu'à ce que la pâte soit lisse.\nÉTAPE 4\nIncorporer les noix hachées et les flocons de noix de coco dans la pâte à pain.\nÉTAPE 5\nVerser la pâte dans le moule préparé et lisser le dessus avec une spatule.\nÉTAPE 6\nCuire au four préchauffé pendant environ 50 à 60 minutes, ou jusqu'à ce qu'un cure-dent inséré au centre en ressorte propre.\nÉTAPE 7\nLaisser refroidir le pain aux bananes et aux noix dans le moule pendant quelques minutes, puis le démouler et le laisser refroidir complètement sur une grille.\nÉTAPE 8\nTrancher et servir le pain aux bananes et aux noix une fois refroidi."]
    temps_de_preparation = 15
    temps_de_cuisson = 60
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

pain_bananes_noix
def crepes_pepites_chocolat(multiplicateur_arrondi):
    ingredient_pour_algo = ["farine tout usage", "sucre", "sel", "lait", "œufs", "beurre fondu", "pépites de chocolat"]
    ingredient = [150*multiplicateur_arrondi, "g de farine tout usage", 25*multiplicateur_arrondi, "g de sucre", " pincée de sel", 250*multiplicateur_arrondi, "ml de lait", 2*multiplicateur_arrondi, " œufs", 50*multiplicateur_arrondi, "g de beurre fondu", 100*multiplicateur_arrondi, "g de pépites de chocolat"]
    preparation = ["ÉTAPE 1\nDans un grand bol, mélanger la farine, le sucre et le sel.\nÉTAPE 2\nFaire un puits au centre du mélange de farine et y verser le lait, les œufs et le beurre fondu.\nÉTAPE 3\nMélanger jusqu'à ce que la pâte soit lisse et homogène.\nÉTAPE 4\nIncorporer les pépites de chocolat dans la pâte à crêpes.\nÉTAPE 5\nChauffer une poêle antiadhésive à feu moyen et y verser un peu de pâte à crêpes.\nÉTAPE 6\nCuire la crêpe pendant environ 2 minutes de chaque côté, jusqu'à ce qu'elle soit dorée et cuite à travers.\nÉTAPE 7\nRépéter avec le reste de la pâte à crêpes.\nÉTAPE 8\nServir les crêpes chaudes avec du sirop d'érable, de la crème fouettée ou des fruits frais si désiré."]
    temps_de_preparation = 10
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

crepes_pepites_chocolat
def omelette_epinards_fromage_feta(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs", "épinards frais", "huile d'olive", "sel", "poivre", "fromage feta"]
    ingredient = [4*multiplicateur_arrondi, " œufs", 100*multiplicateur_arrondi, "g d'épinards frais", 1*multiplicateur_arrondi, " cuillère à soupe d'huile d'olive", " sel", " poivre", 50*multiplicateur_arrondi, "g de fromage feta émietté"]
    preparation = ["ÉTAPE 1\nDans un bol, battre les œufs avec du sel et du poivre.\nÉTAPE 2\nLaver et égoutter les épinards frais, puis les hacher grossièrement.\nÉTAPE 3\nFaire chauffer l'huile d'olive dans une poêle antiadhésive à feu moyen.\nÉTAPE 4\nAjouter les épinards hachés dans la poêle et les faire sauter jusqu'à ce qu'ils soient légèrement ramollis.\nÉTAPE 5\nVerser les œufs battus dans la poêle avec les épinards et remuer doucement pour répartir les épinards de manière uniforme.\nÉTAPE 6\nCuire l'omelette jusqu'à ce qu'elle soit prise sur le dessus mais encore légèrement baveuse au centre.\nÉTAPE 7\nParsemer le fromage feta émietté sur le dessus de l'omelette.\nÉTAPE 8\nPlier l'omelette en deux et laisser cuire encore quelques instants jusqu'à ce que le fromage feta soit légèrement fondu.\nÉTAPE 9\nTransférer l'omelette dans une assiette et servir chaud."]
    temps_de_preparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

omelette_epinards_fromage_feta
def pancakes_banane_noix(multiplicateur_arrondi):
    ingredient_pour_algo = ["banane mûre", "œuf", "lait", "beurre fondu", "farine de blé", "sucre", "levure chimique", "sel", "noix hachées"]
    ingredient = [1*multiplicateur_arrondi, " banane mûre écrasée", 1*multiplicateur_arrondi, " œuf", 100*multiplicateur_arrondi, "ml de lait", 25*multiplicateur_arrondi, "g de beurre fondu", 100*multiplicateur_arrondi, "g de farine de blé", 1*multiplicateur_arrondi, " cuillère à soupe de sucre", 1*multiplicateur_arrondi, " cuillère à café de levure chimique", " pincée de sel", 50*multiplicateur_arrondi, "g de noix hachées"]
    preparation = ["ÉTAPE 1\nDans un bol, mélanger la banane écrasée avec l'œuf, le lait et le beurre fondu.\nÉTAPE 2\nAjouter la farine de blé, le sucre, la levure chimique et le sel dans le bol avec le mélange de banane et d'œuf, et mélanger jusqu'à ce que la pâte soit lisse.\nÉTAPE 3\nIncorporer les noix hachées dans la pâte à pancakes.\nÉTAPE 4\nChauffer une poêle ou une plaque chauffante à feu moyen et y verser un peu de pâte à pancakes pour former des cercles.\nÉTAPE 5\nCuire les pancakes pendant environ 2 à 3 minutes de chaque côté, jusqu'à ce qu'ils soient dorés et gonflés.\nÉTAPE 6\nTransférer les pancakes cuits dans une assiette et les garder au chaud pendant que vous faites cuire le reste de la pâte.\nÉTAPE 7\nServir les pancakes chauds avec du sirop d'érable, du miel ou du beurre si désiré."]
    temps_de_preparation = 10
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

pancakes_banane_noix
def muesli_fruits_noix(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine", "amandes", "noisettes", "noix de cajou", "noix de pécan", "abricots séchés", "raisins secs", "cranberries séchées", "banane", "yaourt grec", "miel"]
    ingredient = [50*multiplicateur_arrondi, "g de flocons d'avoine", 25*multiplicateur_arrondi, "g d'amandes effilées", 25*multiplicateur_arrondi, "g de noisettes concassées", 25*multiplicateur_arrondi, "g de noix de cajou hachées", 25*multiplicateur_arrondi, "g de noix de pécan concassées", 25*multiplicateur_arrondi, "g d'abricots séchés coupés en dés", 25*multiplicateur_arrondi, "g de raisins secs", 25*multiplicateur_arrondi, "g de cranberries séchées", 1*multiplicateur_arrondi, " banane tranchée", 100*multiplicateur_arrondi, "g de yaourt grec", 1*multiplicateur_arrondi, " cuillère à soupe de miel"]
    preparation = ["ÉTAPE 1\nDans un bol, mélanger les flocons d'avoine, les amandes effilées, les noisettes concassées, les noix de cajou hachées, les noix de pécan concassées, les abricots séchés coupés en dés, les raisins secs et les cranberries séchées.\nÉTAPE 2\nTrancher une banane et ajouter les tranches de banane au bol de muesli.\nÉTAPE 3\nAjouter le yaourt grec au bol de muesli.\nÉTAPE 4\nArroser le muesli de miel et mélanger le tout jusqu'à ce que tous les ingrédients soient bien combinés.\nÉTAPE 5\nServir le muesli aux fruits et aux noix dans des bols et déguster immédiatement."]
    temps_de_preparation = 10
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

muesli_fruits_noix

def omelette_champignons_fromage_suisse(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs", "champignons", "huile d'olive", "sel", "poivre", "fromage suisse"]
    ingredient = [4*multiplicateur_arrondi, " œufs", 100*multiplicateur_arrondi, "g de champignons tranchés", 1*multiplicateur_arrondi, " cuillère à soupe d'huile d'olive", " sel", " poivre", 50*multiplicateur_arrondi, "g de fromage suisse râpé"]
    preparation = ["ÉTAPE 1\nDans un bol, battre les œufs avec du sel et du poivre.\nÉTAPE 2\nNettoyer les champignons et les trancher finement.\nÉTAPE 3\nDans une poêle antiadhésive, faire chauffer l'huile d'olive à feu moyen.\nÉTAPE 4\nAjouter les champignons tranchés dans la poêle et les faire sauter jusqu'à ce qu'ils soient dorés et tendres.\nÉTAPE 5\nVerser les œufs battus dans la poêle avec les champignons et remuer doucement pour répartir les champignons de manière uniforme.\nÉTAPE 6\nCuire l'omelette jusqu'à ce qu'elle soit prise sur le dessus mais encore légèrement baveuse au centre.\nÉTAPE 7\nParsemer le fromage suisse râpé sur le dessus de l'omelette.\nÉTAPE 8\nPlier l'omelette en deux et laisser cuire encore quelques instants jusqu'à ce que le fromage soit fondu.\nÉTAPE 9\nTransférer l'omelette dans une assiette et servir chaud."]
    temps_de_preparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

omelette_champignons_fromage_suisse
def pancakes_patate_douce_cannelle(multiplicateur_arrondi):
    ingredient_pour_algo = ["patate douce", "œuf", "lait", "beurre fondu", "farine de blé", "sucre", "levure chimique", "cannelle"]
    ingredient = [1*multiplicateur_arrondi, " patate douce cuite et écrasée", 2*multiplicateur_arrondi, " œufs", 100*multiplicateur_arrondi, "ml de lait", 25*multiplicateur_arrondi, "g de beurre fondu", 100*multiplicateur_arrondi, "g de farine de blé", 1*multiplicateur_arrondi, " cuillère à soupe de sucre", 1*multiplicateur_arrondi, " cuillère à café de levure chimique", " pincée de cannelle"]
    preparation = ["ÉTAPE 1\nCuire une patate douce à la vapeur jusqu'à ce qu'elle soit tendre, puis l'écraser à la fourchette pour en faire de la purée.\nÉTAPE 2\nDans un bol, mélanger la purée de patate douce avec les œufs, le lait et le beurre fondu.\nÉTAPE 3\nAjouter la farine de blé, le sucre, la levure chimique et la cannelle dans le bol avec le mélange de patate douce et d'œuf, et mélanger jusqu'à ce que la pâte soit lisse.\nÉTAPE 4\nChauffer une poêle ou une plaque chauffante à feu moyen et y verser un peu de pâte à pancakes pour former des cercles.\nÉTAPE 5\nCuire les pancakes pendant environ 2 à 3 minutes de chaque côté, jusqu'à ce qu'ils soient dorés et gonflés.\nÉTAPE 6\nTransférer les pancakes cuits dans une assiette et les garder au chaud pendant que vous faites cuire le reste de la pâte.\nÉTAPE 7\nServir les pancakes chauds avec du sirop d'érable, du miel ou du beurre si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

pancakes_patate_douce_cannelle

def omelette_poivrons_fromage_cheddar(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs", "poivron rouge", "poivron vert", "poivron jaune", "huile d'olive", "sel", "poivre", "fromage cheddar"]
    ingredient = [4*multiplicateur_arrondi, " œufs", 50*multiplicateur_arrondi, "g de poivron rouge coupé en dés", 50*multiplicateur_arrondi, "g de poivron vert coupé en dés", 50*multiplicateur_arrondi, "g de poivron jaune coupé en dés", 1*multiplicateur_arrondi, " cuillère à soupe d'huile d'olive", " sel", " poivre", 50*multiplicateur_arrondi, "g de fromage cheddar râpé"]
    preparation = ["ÉTAPE 1\nDans un bol, battre les œufs avec du sel et du poivre.\nÉTAPE 2\nNettoyer les poivrons et les couper en dés.\nÉTAPE 3\nDans une poêle antiadhésive, faire chauffer l'huile d'olive à feu moyen.\nÉTAPE 4\nAjouter les dés de poivron dans la poêle et les faire sauter jusqu'à ce qu'ils soient tendres.\nÉTAPE 5\nVerser les œufs battus dans la poêle avec les poivrons et remuer doucement pour répartir les poivrons de manière uniforme.\nÉTAPE 6\nCuire l'omelette jusqu'à ce qu'elle soit prise sur le dessus mais encore légèrement baveuse au centre.\nÉTAPE 7\nParsemer le fromage cheddar râpé sur le dessus de l'omelette.\nÉTAPE 8\nPlier l'omelette en deux et laisser cuire encore quelques instants jusqu'à ce que le fromage soit fondu.\nÉTAPE 9\nTransférer l'omelette dans une assiette et servir chaud."]
    temps_de_preparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

omelette_poivrons_fromage_cheddar
def pancakes_pomme_cannelle(multiplicateur_arrondi):
    ingredient_pour_algo = ["pomme", "œuf", "lait", "beurre fondu", "farine de blé", "sucre", "levure chimique", "cannelle"]
    ingredient = [1*multiplicateur_arrondi, " pomme râpée", 2*multiplicateur_arrondi, " œufs", 100*multiplicateur_arrondi, "ml de lait", 25*multiplicateur_arrondi, "g de beurre fondu", 100*multiplicateur_arrondi, "g de farine de blé", 1*multiplicateur_arrondi, " cuillère à soupe de sucre", 1*multiplicateur_arrondi, " cuillère à café de levure chimique", " pincée de cannelle"]
    preparation = ["ÉTAPE 1\nÉplucher et râper une pomme.\nÉTAPE 2\nDans un bol, mélanger la pomme râpée avec les œufs, le lait et le beurre fondu.\nÉTAPE 3\nAjouter la farine de blé, le sucre, la levure chimique et la cannelle dans le bol avec le mélange de pomme et d'œuf, et mélanger jusqu'à ce que la pâte soit lisse.\nÉTAPE 4\nChauffer une poêle ou une plaque chauffante à feu moyen et y verser un peu de pâte à pancakes pour former des cercles.\nÉTAPE 5\nCuire les pancakes pendant environ 2 à 3 minutes de chaque côté, jusqu'à ce qu'ils soient dorés et gonflés.\nÉTAPE 6\nTransférer les pancakes cuits dans une assiette et les garder au chaud pendant que vous faites cuire le reste de la pâte.\nÉTAPE 7\nServir les pancakes chauds avec du sirop d'érable, du miel ou du beurre si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

pancakes_pomme_cannelle
def omelette_champignons_fromage_bleu(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs", "champignons", "huile d'olive", "sel", "poivre", "fromage bleu"]
    ingredient = [4*multiplicateur_arrondi, " œufs", 100*multiplicateur_arrondi, "g de champignons tranchés", 1*multiplicateur_arrondi, " cuillère à soupe d'huile d'olive", " sel", " poivre", 50*multiplicateur_arrondi, "g de fromage bleu émietté"]
    preparation = ["ÉTAPE 1\nDans un bol, battre les œufs avec du sel et du poivre.\nÉTAPE 2\nNettoyer les champignons et les trancher finement.\nÉTAPE 3\nDans une poêle antiadhésive, faire chauffer l'huile d'olive à feu moyen.\nÉTAPE 4\nAjouter les champignons tranchés dans la poêle et les faire sauter jusqu'à ce qu'ils soient dorés et tendres.\nÉTAPE 5\nVerser les œufs battus dans la poêle avec les champignons et remuer doucement pour répartir les champignons de manière uniforme.\nÉTAPE 6\nCuire l'omelette jusqu'à ce qu'elle soit prise sur le dessus mais encore légèrement baveuse au centre.\nÉTAPE 7\nParsemer le fromage bleu émietté sur le dessus de l'omelette.\nÉTAPE 8\nPlier l'omelette en deux et laisser cuire encore quelques instants jusqu'à ce que le fromage bleu soit légèrement fondu.\nÉTAPE 9\nTransférer l'omelette dans une assiette et servir chaud."]
    temps_de_preparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

omelette_champignons_fromage_bleu
def pancakes_myrtilles_citron(multiplicateur_arrondi):
    ingredient_pour_algo = ["myrtilles", "citron", "œuf", "lait", "beurre fondu", "farine de blé", "sucre", "levure chimique"]
    ingredient = [100*multiplicateur_arrondi, "g de myrtilles", " zeste de citron râpé", 2*multiplicateur_arrondi, " œufs", 100*multiplicateur_arrondi, "ml de lait", 25*multiplicateur_arrondi, "g de beurre fondu", 100*multiplicateur_arrondi, "g de farine de blé", 1*multiplicateur_arrondi, " cuillère à soupe de sucre", 1*multiplicateur_arrondi, " cuillère à café de levure chimique"]
    preparation = ["ÉTAPE 1\nDans un bol, mélanger les myrtilles avec le zeste de citron râpé.\nÉTAPE 2\nDans un autre bol, battre les œufs avec le lait et le beurre fondu.\nÉTAPE 3\nAjouter la farine de blé, le sucre et la levure chimique dans le bol avec le mélange d'œufs, de lait et de beurre fondu, et mélanger jusqu'à ce que la pâte soit lisse.\nÉTAPE 4\nIncorporer délicatement les myrtilles et le zeste de citron dans la pâte à pancakes.\nÉTAPE 5\nChauffer une poêle ou une plaque chauffante à feu moyen et y verser un peu de pâte à pancakes pour former des cercles.\nÉTAPE 6\nCuire les pancakes pendant environ 2 à 3 minutes de chaque côté, jusqu'à ce qu'ils soient dorés et gonflés.\nÉTAPE 7\nTransférer les pancakes cuits dans une assiette et les garder au chaud pendant que vous faites cuire le reste de la pâte.\nÉTAPE 8\nServir les pancakes chauds avec du sirop d'érable, du miel ou du beurre si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

pancakes_myrtilles_citron
def smoothie_ananas_noix_coco(multiplicateur_arrondi):
    ingredient_pour_algo = ["ananas", "banane", "yaourt grec", "lait de coco", "noix de coco râpée", "miel"]
    ingredient = [1*multiplicateur_arrondi, " ananas coupé en dés", 1*multiplicateur_arrondi, " banane", 150*multiplicateur_arrondi, "g de yaourt grec", 100*multiplicateur_arrondi, "ml de lait de coco", 1*multiplicateur_arrondi, " cuillère à soupe de noix de coco râpée", 1*multiplicateur_arrondi, " cuillère à soupe de miel"]
    preparation = ["ÉTAPE 1\nDans un blender, placer les dés d'ananas, la banane, le yaourt grec, le lait de coco, la noix de coco râpée et le miel.\nÉTAPE 2\nMixer jusqu'à obtention d'une consistance lisse et crémeuse.\nÉTAPE 3\nVerser le smoothie dans un verre.\nÉTAPE 4\nDécorer avec quelques morceaux d'ananas ou de noix de coco râpée si désiré.\nÉTAPE 5\nDéguster immédiatement."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

smoothie_ananas_noix_coco
def omelette_epinards_fromage_feta(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs", "épinards frais", "huile d'olive", "sel", "poivre", "fromage feta"]
    ingredient = [4*multiplicateur_arrondi, " œufs", 50*multiplicateur_arrondi, "g d'épinards frais", 1*multiplicateur_arrondi, " cuillère à soupe d'huile d'olive", " sel", " poivre", 50*multiplicateur_arrondi, "g de fromage feta émietté"]
    preparation = ["ÉTAPE 1\nDans un bol, battre les œufs avec du sel et du poivre.\nÉTAPE 2\nLaver les épinards frais et les égoutter.\nÉTAPE 3\nDans une poêle antiadhésive, faire chauffer l'huile d'olive à feu moyen.\nÉTAPE 4\nAjouter les épinards dans la poêle et les faire sauter jusqu'à ce qu'ils soient flétris.\nÉTAPE 5\nVerser les œufs battus dans la poêle avec les épinards et remuer doucement pour répartir les épinards de manière uniforme.\nÉTAPE 6\nCuire l'omelette jusqu'à ce qu'elle soit prise sur le dessus mais encore légèrement baveuse au centre.\nÉTAPE 7\nParsemer le fromage feta émietté sur le dessus de l'omelette.\nÉTAPE 8\nPlier l'omelette en deux et laisser cuire encore quelques instants jusqu'à ce que le fromage feta soit légèrement fondu.\nÉTAPE 9\nTransférer l'omelette dans une assiette et servir chaud."]
    temps_de_preparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

omelette_epinards_fromage_feta
def pancakes_chocolat_banane(multiplicateur_arrondi):
    ingredient_pour_algo = ["banane", "œuf", "lait", "beurre fondu", "farine de blé", "sucre", "levure chimique", "pépites de chocolat"]
    ingredient = [1*multiplicateur_arrondi, " banane écrasée", 2*multiplicateur_arrondi, " œufs", 100*multiplicateur_arrondi, "ml de lait", 25*multiplicateur_arrondi, "g de beurre fondu", 100*multiplicateur_arrondi, "g de farine de blé", 1*multiplicateur_arrondi, " cuillère à soupe de sucre", 1*multiplicateur_arrondi, " cuillère à café de levure chimique", 50*multiplicateur_arrondi, "g de pépites de chocolat"]
    preparation = ["ÉTAPE 1\nDans un bol, écraser une banane à la fourchette.\nÉTAPE 2\nAjouter les œufs, le lait et le beurre fondu dans le bol avec la banane écrasée, et mélanger jusqu'à ce que le mélange soit homogène.\nÉTAPE 3\nIncorporer la farine de blé, le sucre et la levure chimique dans le bol avec le mélange de banane et d'œuf, et mélanger jusqu'à ce que la pâte soit lisse.\nÉTAPE 4\nAjouter les pépites de chocolat dans la pâte à pancakes et mélanger doucement pour les répartir uniformément.\nÉTAPE 5\nChauffer une poêle ou une plaque chauffante à feu moyen et y verser un peu de pâte à pancakes pour former des cercles.\nÉTAPE 6\nCuire les pancakes pendant environ 2 à 3 minutes de chaque côté, jusqu'à ce qu'ils soient dorés et gonflés.\nÉTAPE 7\nTransférer les pancakes cuits dans une assiette et les garder au chaud pendant que vous faites cuire le reste de la pâte.\nÉTAPE 8\nServir les pancakes chauds avec des tranches de banane supplémentaires et du sirop d'érable si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

pancakes_chocolat_banane

def omelette_tomates_mozzarella(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs", "tomates cerises", "huile d'olive", "sel", "poivre", "fromage mozzarella"]
    ingredient = [4*multiplicateur_arrondi, " œufs", 100*multiplicateur_arrondi, "g de tomates cerises coupées en deux", 1*multiplicateur_arrondi, " cuillère à soupe d'huile d'olive", " sel", " poivre", 50*multiplicateur_arrondi, "g de fromage mozzarella coupé en dés"]
    preparation = ["ÉTAPE 1\nDans un bol, battre les œufs avec du sel et du poivre.\nÉTAPE 2\nCouper les tomates cerises en deux.\nÉTAPE 3\nDans une poêle antiadhésive, faire chauffer l'huile d'olive à feu moyen.\nÉTAPE 4\nAjouter les tomates cerises dans la poêle et les faire sauter jusqu'à ce qu'elles soient légèrement ramollies.\nÉTAPE 5\nVerser les œufs battus dans la poêle avec les tomates cerises et remuer doucement pour répartir les tomates de manière uniforme.\nÉTAPE 6\nCuire l'omelette jusqu'à ce qu'elle soit prise sur le dessus mais encore légèrement baveuse au centre.\nÉTAPE 7\nParsemer le fromage mozzarella coupé en dés sur le dessus de l'omelette.\nÉTAPE 8\nPlier l'omelette en deux et laisser cuire encore quelques instants jusqu'à ce que le fromage mozzarella soit légèrement fondu.\nÉTAPE 9\nTransférer l'omelette dans une assiette et servir chaud."]
    temps_de_preparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

omelette_tomates_mozzarella
def pancakes_chocolat_noix(multiplicateur_arrondi):
    ingredient_pour_algo = ["œuf", "lait", "beurre fondu", "farine de blé", "sucre", "levure chimique", "pépites de chocolat", "noix hachées"]
    ingredient = [2*multiplicateur_arrondi, " œufs", 100*multiplicateur_arrondi, "ml de lait", 25*multiplicateur_arrondi, "g de beurre fondu", 100*multiplicateur_arrondi, "g de farine de blé", 1*multiplicateur_arrondi, " cuillère à soupe de sucre", 1*multiplicateur_arrondi, " cuillère à café de levure chimique", 50*multiplicateur_arrondi, "g de pépites de chocolat", 50*multiplicateur_arrondi, "g de noix hachées"]
    preparation = ["ÉTAPE 1\nDans un bol, battre les œufs avec le lait et le beurre fondu.\nÉTAPE 2\nAjouter la farine de blé, le sucre et la levure chimique dans le bol avec le mélange d'œufs, de lait et de beurre fondu, et mélanger jusqu'à ce que la pâte soit lisse.\nÉTAPE 3\nIncorporer les pépites de chocolat et les noix hachées dans la pâte à pancakes.\nÉTAPE 4\nChauffer une poêle ou une plaque chauffante à feu moyen et y verser un peu de pâte à pancakes pour former des cercles.\nÉTAPE 5\nCuire les pancakes pendant environ 2 à 3 minutes de chaque côté, jusqu'à ce qu'ils soient dorés et gonflés.\nÉTAPE 6\nTransférer les pancakes cuits dans une assiette et les garder au chaud pendant que vous faites cuire le reste de la pâte.\nÉTAPE 7\nServir les pancakes chauds avec du sirop d'érable, du miel ou du beurre si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

pancakes_chocolat_noix
def smoothie_mangue_noix_coco(multiplicateur_arrondi):
    ingredient_pour_algo = ["mangue", "banane", "yaourt grec", "lait de coco", "noix de coco râpée", "miel"]
    ingredient = [1*multiplicateur_arrondi, " mangue coupée en dés", 1*multiplicateur_arrondi, " banane", 150*multiplicateur_arrondi, "g de yaourt grec", 100*multiplicateur_arrondi, "ml de lait de coco", 1*multiplicateur_arrondi, " cuillère à soupe de noix de coco râpée", 1*multiplicateur_arrondi, " cuillère à soupe de miel"]
    preparation = ["ÉTAPE 1\nDans un blender, placer les dés de mangue et la banane.\nÉTAPE 2\nAjouter le yaourt grec, le lait de coco, la noix de coco râpée et le miel dans le blender.\nÉTAPE 3\nMixer jusqu'à obtention d'une consistance lisse et crémeuse.\nÉTAPE 4\nVerser le smoothie dans un verre.\nÉTAPE 5\nDécorer avec des morceaux de mangue supplémentaires si désiré.\nÉTAPE 6\nDéguster immédiatement."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

smoothie_mangue_noix_coco
def omelette_poivrons_cheddar(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs", "poivron rouge", "poivron vert", "huile d'olive", "sel", "poivre", "fromage cheddar"]
    ingredient = [4*multiplicateur_arrondi, " œufs", 1*multiplicateur_arrondi, " poivron rouge coupé en dés", 1*multiplicateur_arrondi, " poivron vert coupé en dés", 1*multiplicateur_arrondi, " cuillère à soupe d'huile d'olive", " sel", " poivre", 50*multiplicateur_arrondi, "g de fromage cheddar râpé"]
    preparation = ["ÉTAPE 1\nDans un bol, battre les œufs avec du sel et du poivre.\nÉTAPE 2\nCouper les poivrons rouges et verts en dés.\nÉTAPE 3\nDans une poêle antiadhésive, faire chauffer l'huile d'olive à feu moyen.\nÉTAPE 4\nAjouter les dés de poivron dans la poêle et les faire sauter jusqu'à ce qu'ils soient tendres.\nÉTAPE 5\nVerser les œufs battus dans la poêle avec les poivrons et remuer doucement pour répartir les poivrons de manière uniforme.\nÉTAPE 6\nCuire l'omelette jusqu'à ce qu'elle soit prise sur le dessus mais encore légèrement baveuse au centre.\nÉTAPE 7\nParsemer le fromage cheddar râpé sur le dessus de l'omelette.\nÉTAPE 8\nPlier l'omelette en deux et laisser cuire encore quelques instants jusqu'à ce que le fromage cheddar soit légèrement fondu.\nÉTAPE 9\nTransférer l'omelette dans une assiette et servir chaud."]
    temps_de_preparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

omelette_poivrons_cheddar
def pancakes_framboises_vanille(multiplicateur_arrondi):
    ingredient_pour_algo = ["framboises", "œuf", "lait", "beurre fondu", "farine de blé", "sucre", "levure chimique", "extrait de vanille"]
    ingredient = [100*multiplicateur_arrondi, "g de framboises", 2*multiplicateur_arrondi, " œufs", 100*multiplicateur_arrondi, "ml de lait", 25*multiplicateur_arrondi, "g de beurre fondu", 100*multiplicateur_arrondi, "g de farine de blé", 1*multiplicateur_arrondi, " cuillère à soupe de sucre", 1*multiplicateur_arrondi, " cuillère à café de levure chimique", " quelques gouttes d'extrait de vanille"]
    preparation = ["ÉTAPE 1\nDans un bol, mélanger les framboises avec les œufs battus.\nÉTAPE 2\nAjouter le lait et le beurre fondu dans le bol avec le mélange de framboises et d'œufs, et mélanger jusqu'à ce que le mélange soit homogène.\nÉTAPE 3\nIncorporer la farine de blé, le sucre, la levure chimique et l'extrait de vanille dans le bol avec le mélange de framboises, d'œufs, de lait et de beurre fondu, et mélanger jusqu'à ce que la pâte soit lisse.\nÉTAPE 4\nChauffer une poêle ou une plaque chauffante à feu moyen et y verser un peu de pâte à pancakes pour former des cercles.\nÉTAPE 5\nDisposer quelques framboises sur le dessus de chaque pancake.\nÉTAPE 6\nCuire les pancakes pendant environ 2 à 3 minutes de chaque côté, jusqu'à ce qu'ils soient dorés et gonflés.\nÉTAPE 7\nTransférer les pancakes cuits dans une assiette et les garder au chaud pendant que vous faites cuire le reste de la pâte.\nÉTAPE 8\nServir les pancakes chauds avec du sirop d'érable ou du yaourt à la vanille si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

pancakes_framboises_vanille
def smoothie_myrtilles_banane(multiplicateur_arrondi):
    ingredient_pour_algo = ["myrtilles", "banane", "yaourt grec", "lait", "miel"]
    ingredient = [150*multiplicateur_arrondi, "g de myrtilles", 1*multiplicateur_arrondi, " banane", 150*multiplicateur_arrondi, "g de yaourt grec", 100*multiplicateur_arrondi, "ml de lait", 1*multiplicateur_arrondi, " cuillère à soupe de miel"]
    preparation = ["ÉTAPE 1\nDans un blender, placer les myrtilles et la banane.\nÉTAPE 2\nAjouter le yaourt grec, le lait et le miel dans le blender.\nÉTAPE 3\nMixer jusqu'à obtention d'une consistance lisse et crémeuse.\nÉTAPE 4\nVerser le smoothie dans un verre.\nÉTAPE 5\nDécorer avec quelques myrtilles fraîches si désiré.\nÉTAPE 6\nDéguster immédiatement."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

smoothie_myrtilles_banane
def omelette_champignons_fromage_suisse(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs", "champignons", "huile d'olive", "sel", "poivre", "fromage suisse"]
    ingredient = [4*multiplicateur_arrondi, " œufs", 100*multiplicateur_arrondi, "g de champignons tranchés", 1*multiplicateur_arrondi, " cuillère à soupe d'huile d'olive", " sel", " poivre", 50*multiplicateur_arrondi, "g de fromage suisse râpé"]
    preparation = ["ÉTAPE 1\nDans un bol, battre les œufs avec du sel et du poivre.\nÉTAPE 2\nCouper les champignons en tranches.\nÉTAPE 3\nDans une poêle antiadhésive, faire chauffer l'huile d'olive à feu moyen.\nÉTAPE 4\nAjouter les champignons dans la poêle et les faire sauter jusqu'à ce qu'ils soient dorés et tendres.\nÉTAPE 5\nVerser les œufs battus dans la poêle avec les champignons et remuer doucement pour répartir les champignons de manière uniforme.\nÉTAPE 6\nCuire l'omelette jusqu'à ce qu'elle soit prise sur le dessus mais encore légèrement baveuse au centre.\nÉTAPE 7\nParsemer le fromage suisse râpé sur le dessus de l'omelette.\nÉTAPE 8\nPlier l'omelette en deux et laisser cuire encore quelques instants jusqu'à ce que le fromage suisse soit légèrement fondu.\nÉTAPE 9\nTransférer l'omelette dans une assiette et servir chaud."]
    temps_de_preparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

omelette_champignons_fromage_suisse
def pancakes_chocolat_bananes(multiplicateur_arrondi):
    ingredient_pour_algo = ["œuf", "lait", "beurre fondu", "farine de blé", "sucre", "levure chimique", "pépites de chocolat", "bananes tranchées"]
    ingredient = [2*multiplicateur_arrondi, " œufs", 100*multiplicateur_arrondi, "ml de lait", 25*multiplicateur_arrondi, "g de beurre fondu", 100*multiplicateur_arrondi, "g de farine de blé", 1*multiplicateur_arrondi, " cuillère à soupe de sucre", 1*multiplicateur_arrondi, " cuillère à café de levure chimique", 50*multiplicateur_arrondi, "g de pépites de chocolat", 1*multiplicateur_arrondi, " banane tranchée"]
    preparation = ["ÉTAPE 1\nDans un bol, battre les œufs avec le lait et le beurre fondu.\nÉTAPE 2\nAjouter la farine de blé, le sucre, la levure chimique dans le bol avec le mélange d'œufs, de lait et de beurre fondu, et mélanger jusqu'à ce que la pâte soit lisse.\nÉTAPE 3\nIncorporer les pépites de chocolat dans la pâte à pancakes.\nÉTAPE 4\nChauffer une poêle ou une plaque chauffante à feu moyen et y verser un peu de pâte à pancakes pour former des cercles.\nÉTAPE 5\nDisposer quelques tranches de banane sur le dessus de chaque pancake.\nÉTAPE 6\nCuire les pancakes pendant environ 2 à 3 minutes de chaque côté, jusqu'à ce qu'ils soient dorés et gonflés.\nÉTAPE 7\nTransférer les pancakes cuits dans une assiette et les garder au chaud pendant que vous faites cuire le reste de la pâte.\nÉTAPE 8\nServir les pancakes chauds avec du sirop d'érable, du miel ou du beurre si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

pancakes_chocolat_bananes
def smoothie_ananas_mangue(multiplicateur_arrondi):
    ingredient_pour_algo = ["ananas", "mangue", "yaourt grec", "lait", "miel"]
    ingredient = [150*multiplicateur_arrondi, "g d'ananas coupé en dés", 1*multiplicateur_arrondi, " mangue coupée en dés", 150*multiplicateur_arrondi, "g de yaourt grec", 100*multiplicateur_arrondi, "ml de lait", 1*multiplicateur_arrondi, " cuillère à soupe de miel"]
    preparation = ["ÉTAPE 1\nDans un blender, placer les dés d'ananas et de mangue.\nÉTAPE 2\nAjouter le yaourt grec, le lait et le miel dans le blender.\nÉTAPE 3\nMixer jusqu'à obtention d'une consistance lisse et crémeuse.\nÉTAPE 4\nVerser le smoothie dans un verre.\nÉTAPE 5\nDécorer avec des morceaux d'ananas ou de mangue supplémentaires si désiré.\nÉTAPE 6\nDéguster immédiatement."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

smoothie_ananas_mangue
def omelette_epinards_feta(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs", "épinards", "huile d'olive", "sel", "poivre", "fromage feta"]
    ingredient = [4*multiplicateur_arrondi, " œufs", 100*multiplicateur_arrondi, "g d'épinards frais", 1*multiplicateur_arrondi, " cuillère à soupe d'huile d'olive", " sel", " poivre", 50*multiplicateur_arrondi, "g de fromage feta émietté"]
    preparation = ["ÉTAPE 1\nDans un bol, battre les œufs avec du sel et du poivre.\nÉTAPE 2\nLaver et égoutter les épinards.\nÉTAPE 3\nDans une poêle antiadhésive, faire chauffer l'huile d'olive à feu moyen.\nÉTAPE 4\nAjouter les épinards dans la poêle et les faire sauter jusqu'à ce qu'ils soient ramollis.\nÉTAPE 5\nVerser les œufs battus dans la poêle avec les épinards et remuer doucement pour répartir les épinards de manière uniforme.\nÉTAPE 6\nCuire l'omelette jusqu'à ce qu'elle soit prise sur le dessus mais encore légèrement baveuse au centre.\nÉTAPE 7\nParsemer le fromage feta émietté sur le dessus de l'omelette.\nÉTAPE 8\nPlier l'omelette en deux et laisser cuire encore quelques instants jusqu'à ce que le fromage feta soit légèrement fondu.\nÉTAPE 9\nTransférer l'omelette dans une assiette et servir chaud."]
    temps_de_preparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

omelette_epinards_feta
def pancakes_myrtilles_sirop_erable(multiplicateur_arrondi):
    ingredient_pour_algo = ["œuf", "lait", "beurre fondu", "farine de blé", "sucre", "levure chimique", "myrtilles", "sirop d'érable"]
    ingredient = [2*multiplicateur_arrondi, " œufs", 100*multiplicateur_arrondi, "ml de lait", 25*multiplicateur_arrondi, "g de beurre fondu", 100*multiplicateur_arrondi, "g de farine de blé", 1*multiplicateur_arrondi, " cuillère à soupe de sucre", 1*multiplicateur_arrondi, " cuillère à café de levure chimique", 100*multiplicateur_arrondi, "g de myrtilles fraîches", " sirop d'érable pour servir"]
    preparation = ["ÉTAPE 1\nDans un bol, battre les œufs avec le lait et le beurre fondu.\nÉTAPE 2\nAjouter la farine de blé, le sucre, la levure chimique dans le bol avec le mélange d'œufs, de lait et de beurre fondu, et mélanger jusqu'à ce que la pâte soit lisse.\nÉTAPE 3\nIncorporer les myrtilles fraîches dans la pâte à pancakes.\nÉTAPE 4\nChauffer une poêle ou une plaque chauffante à feu moyen et y verser un peu de pâte à pancakes pour former des cercles.\nÉTAPE 5\nCuire les pancakes pendant environ 2 à 3 minutes de chaque côté, jusqu'à ce qu'ils soient dorés et gonflés.\nÉTAPE 6\nTransférer les pancakes cuits dans une assiette et les garder au chaud pendant que vous faites cuire le reste de la pâte.\nÉTAPE 7\nServir les pancakes chauds avec du sirop d'érable."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

pancakes_myrtilles_sirop_erable
def smoothie_fraise_banane(multiplicateur_arrondi):
    ingredient_pour_algo = ["fraises", "banane", "yaourt grec", "lait", "miel"]
    ingredient = [150*multiplicateur_arrondi, "g de fraises coupées en morceaux", 1*multiplicateur_arrondi, " banane", 150*multiplicateur_arrondi, "g de yaourt grec", 100*multiplicateur_arrondi, "ml de lait", 1*multiplicateur_arrondi, " cuillère à soupe de miel"]
    preparation = ["ÉTAPE 1\nDans un blender, placer les morceaux de fraises et de banane.\nÉTAPE 2\nAjouter le yaourt grec, le lait et le miel dans le blender.\nÉTAPE 3\nMixer jusqu'à obtention d'une consistance lisse et crémeuse.\nÉTAPE 4\nVerser le smoothie dans un verre.\nÉTAPE 5\nDécorer avec des fraises tranchées si désiré.\nÉTAPE 6\nDéguster immédiatement."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

smoothie_fraise_banane
def omelette_epinards_cheddar(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs", "épinards", "huile d'olive", "sel", "poivre", "fromage cheddar"]
    ingredient = [4*multiplicateur_arrondi, " œufs", 100*multiplicateur_arrondi, "g d'épinards frais", 1*multiplicateur_arrondi, " cuillère à soupe d'huile d'olive", " sel", " poivre", 50*multiplicateur_arrondi, "g de fromage cheddar râpé"]
    preparation = ["ÉTAPE 1\nDans un bol, battre les œufs avec du sel et du poivre.\nÉTAPE 2\nLaver et égoutter les épinards.\nÉTAPE 3\nDans une poêle antiadhésive, faire chauffer l'huile d'olive à feu moyen.\nÉTAPE 4\nAjouter les épinards dans la poêle et les faire sauter jusqu'à ce qu'ils soient ramollis.\nÉTAPE 5\nVerser les œufs battus dans la poêle avec les épinards et remuer doucement pour répartir les épinards de manière uniforme.\nÉTAPE 6\nCuire l'omelette jusqu'à ce qu'elle soit prise sur le dessus mais encore légèrement baveuse au centre.\nÉTAPE 7\nParsemer le fromage cheddar râpé sur le dessus de l'omelette.\nÉTAPE 8\nPlier l'omelette en deux et laisser cuire encore quelques instants jusqu'à ce que le fromage cheddar soit légèrement fondu.\nÉTAPE 9\nTransférer l'omelette dans une assiette et servir chaud."]
    temps_de_preparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

omelette_epinards_cheddar
def pancakes_chocolat_noix(multiplicateur_arrondi):
    ingredient_pour_algo = ["œuf", "lait", "beurre fondu", "farine de blé", "sucre", "levure chimique", "pépites de chocolat", "noix concassées"]
    ingredient = [2*multiplicateur_arrondi, " œufs", 100*multiplicateur_arrondi, "ml de lait", 25*multiplicateur_arrondi, "g de beurre fondu", 100*multiplicateur_arrondi, "g de farine de blé", 1*multiplicateur_arrondi, " cuillère à soupe de sucre", 1*multiplicateur_arrondi, " cuillère à café de levure chimique", 50*multiplicateur_arrondi, "g de pépites de chocolat", 50*multiplicateur_arrondi, "g de noix concassées"]
    preparation = ["ÉTAPE 1\nDans un bol, battre les œufs avec le lait et le beurre fondu.\nÉTAPE 2\nAjouter la farine de blé, le sucre, la levure chimique dans le bol avec le mélange d'œufs, de lait et de beurre fondu, et mélanger jusqu'à ce que la pâte soit lisse.\nÉTAPE 3\nIncorporer les pépites de chocolat et les noix concassées dans la pâte à pancakes.\nÉTAPE 4\nChauffer une poêle ou une plaque chauffante à feu moyen et y verser un peu de pâte à pancakes pour former des cercles.\nÉTAPE 5\nCuire les pancakes pendant environ 2 à 3 minutes de chaque côté, jusqu'à ce qu'ils soient dorés et gonflés.\nÉTAPE 6\nTransférer les pancakes cuits dans une assiette et les garder au chaud pendant que vous faites cuire le reste de la pâte.\nÉTAPE 7\nServir les pancakes chauds avec du sirop d'érable, du miel ou du beurre si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

pancakes_chocolat_noix
def smoothie_fraises_kiwi(multiplicateur_arrondi):
    ingredient_pour_algo = ["fraises", "kiwi", "yaourt grec", "lait", "miel"]
    ingredient = [150*multiplicateur_arrondi, "g de fraises coupées en morceaux", 1*multiplicateur_arrondi, " kiwi pelé et coupé en morceaux", 150*multiplicateur_arrondi, "g de yaourt grec", 100*multiplicateur_arrondi, "ml de lait", 1*multiplicateur_arrondi, " cuillère à soupe de miel"]
    preparation = ["ÉTAPE 1\nDans un blender, placer les morceaux de fraises et de kiwi.\nÉTAPE 2\nAjouter le yaourt grec, le lait et le miel dans le blender.\nÉTAPE 3\nMixer jusqu'à obtention d'une consistance lisse et crémeuse.\nÉTAPE 4\nVerser le smoothie dans un verre.\nÉTAPE 5\nDécorer avec des morceaux de kiwi frais si désiré.\nÉTAPE 6\nDéguster immédiatement."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

smoothie_fraises_kiwi
def omelette_epinards_suisse(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs", "épinards", "huile d'olive", "sel", "poivre", "fromage suisse"]
    ingredient = [4*multiplicateur_arrondi, " œufs", 100*multiplicateur_arrondi, "g d'épinards frais", 1*multiplicateur_arrondi, " cuillère à soupe d'huile d'olive", " sel", " poivre", 50*multiplicateur_arrondi, "g de fromage suisse râpé"]
    preparation = ["ÉTAPE 1\nDans un bol, battre les œufs avec du sel et du poivre.\nÉTAPE 2\nLaver et égoutter les épinards.\nÉTAPE 3\nDans une poêle antiadhésive, faire chauffer l'huile d'olive à feu moyen.\nÉTAPE 4\nAjouter les épinards dans la poêle et les faire sauter jusqu'à ce qu'ils soient ramollis.\nÉTAPE 5\nVerser les œufs battus dans la poêle avec les épinards et remuer doucement pour répartir les épinards de manière uniforme.\nÉTAPE 6\nCuire l'omelette jusqu'à ce qu'elle soit prise sur le dessus mais encore légèrement baveuse au centre.\nÉTAPE 7\nParsemer le fromage suisse râpé sur le dessus de l'omelette.\nÉTAPE 8\nPlier l'omelette en deux et laisser cuire encore quelques instants jusqu'à ce que le fromage suisse soit légèrement fondu.\nÉTAPE 9\nTransférer l'omelette dans une assiette et servir chaud."]
    temps_de_preparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

omelette_epinards_suisse
def pancakes_myrtilles_noix(multiplicateur_arrondi):
    ingredient_pour_algo = ["œuf", "lait", "beurre fondu", "farine de blé", "sucre", "levure chimique", "myrtilles", "noix concassées"]
    ingredient = [2*multiplicateur_arrondi, " œufs", 100*multiplicateur_arrondi, "ml de lait", 25*multiplicateur_arrondi, "g de beurre fondu", 100*multiplicateur_arrondi, "g de farine de blé", 1*multiplicateur_arrondi, " cuillère à soupe de sucre", 1*multiplicateur_arrondi, " cuillère à café de levure chimique", 100*multiplicateur_arrondi, "g de myrtilles fraîches", 50*multiplicateur_arrondi, "g de noix concassées"]
    preparation = ["ÉTAPE 1\nDans un bol, battre les œufs avec le lait et le beurre fondu.\nÉTAPE 2\nAjouter la farine de blé, le sucre, la levure chimique dans le bol avec le mélange d'œufs, de lait et de beurre fondu, et mélanger jusqu'à ce que la pâte soit lisse.\nÉTAPE 3\nIncorporer les myrtilles fraîches et les noix concassées dans la pâte à pancakes.\nÉTAPE 4\nChauffer une poêle ou une plaque chauffante à feu moyen et y verser un peu de pâte à pancakes pour former des cercles.\nÉTAPE 5\nCuire les pancakes pendant environ 2 à 3 minutes de chaque côté, jusqu'à ce qu'ils soient dorés et gonflés.\nÉTAPE 6\nTransférer les pancakes cuits dans une assiette et les garder au chaud pendant que vous faites cuire le reste de la pâte.\nÉTAPE 7\nServir les pancakes chauds avec du sirop d'érable, du miel ou du beurre si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

pancakes_myrtilles_noix
def smoothie_epinards_banane(multiplicateur_arrondi):
    ingredient_pour_algo = ["épinards", "banane", "yaourt grec", "lait", "miel"]
    ingredient = [100*multiplicateur_arrondi, "g d'épinards frais", 1*multiplicateur_arrondi, " banane", 150*multiplicateur_arrondi, "g de yaourt grec", 100*multiplicateur_arrondi, "ml de lait", 1*multiplicateur_arrondi, " cuillère à soupe de miel"]
    preparation = ["ÉTAPE 1\nDans un blender, placer les épinards frais et la banane.\nÉTAPE 2\nAjouter le yaourt grec, le lait et le miel dans le blender.\nÉTAPE 3\nMixer jusqu'à obtention d'une consistance lisse et crémeuse.\nÉTAPE 4\nVerser le smoothie dans un verre.\nÉTAPE 5\nDécorer avec des tranches de banane si désiré.\nÉTAPE 6\nDéguster immédiatement."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

smoothie_epinards_banane
def omelette_champignons_fromage(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs", "champignons", "huile d'olive", "sel", "poivre", "fromage râpé"]
    ingredient = [4*multiplicateur_arrondi, " œufs", 100*multiplicateur_arrondi, "g de champignons tranchés", 1*multiplicateur_arrondi, " cuillère à soupe d'huile d'olive", " sel", " poivre", 50*multiplicateur_arrondi, "g de fromage râpé"]
    preparation = ["ÉTAPE 1\nDans un bol, battre les œufs avec du sel et du poivre.\nÉTAPE 2\nFaire chauffer l'huile d'olive dans une poêle antiadhésive à feu moyen.\nÉTAPE 3\nAjouter les champignons tranchés dans la poêle et les faire sauter jusqu'à ce qu'ils soient dorés et tendres.\nÉTAPE 4\nVerser les œufs battus dans la poêle avec les champignons et remuer doucement pour répartir les champignons de manière uniforme.\nÉTAPE 5\nCuire l'omelette jusqu'à ce qu'elle soit prise sur le dessus mais encore légèrement baveuse au centre.\nÉTAPE 6\nParsemer le fromage râpé sur le dessus de l'omelette.\nÉTAPE 7\nPlier l'omelette en deux et laisser cuire encore quelques instants jusqu'à ce que le fromage soit fondu.\nÉTAPE 8\nTransférer l'omelette dans une assiette et servir chaud."]
    temps_de_preparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

omelette_champignons_fromage
def pancakes_chocolat_bananes(multiplicateur_arrondi):
    ingredient_pour_algo = ["œuf", "lait", "beurre fondu", "farine de blé", "sucre", "levure chimique", "pépites de chocolat", "bananes"]
    ingredient = [2*multiplicateur_arrondi, " œufs", 100*multiplicateur_arrondi, "ml de lait", 25*multiplicateur_arrondi, "g de beurre fondu", 100*multiplicateur_arrondi, "g de farine de blé", 1*multiplicateur_arrondi, " cuillère à soupe de sucre", 1*multiplicateur_arrondi, " cuillère à café de levure chimique", 50*multiplicateur_arrondi, "g de pépites de chocolat", 2*multiplicateur_arrondi, " bananes"]
    preparation = ["ÉTAPE 1\nDans un bol, battre les œufs avec le lait et le beurre fondu.\nÉTAPE 2\nAjouter la farine de blé, le sucre, la levure chimique dans le bol avec le mélange d'œufs, de lait et de beurre fondu, et mélanger jusqu'à ce que la pâte soit lisse.\nÉTAPE 3\nIncorporer les pépites de chocolat dans la pâte à pancakes.\nÉTAPE 4\nChauffer une poêle ou une plaque chauffante à feu moyen et y verser un peu de pâte à pancakes pour former des cercles.\nÉTAPE 5\nDisposer des tranches de banane sur le dessus de chaque pancake.\nÉTAPE 6\nCuire les pancakes pendant environ 2 à 3 minutes de chaque côté, jusqu'à ce qu'ils soient dorés et gonflés.\nÉTAPE 7\nTransférer les pancakes cuits dans une assiette et les garder au chaud pendant que vous faites cuire le reste de la pâte.\nÉTAPE 8\nServir les pancakes chauds avec du sirop d'érable, du miel ou du beurre si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

pancakes_chocolat_bananes
def smoothie_myrtilles_banane(multiplicateur_arrondi):
    ingredient_pour_algo = ["myrtilles", "banane", "yaourt grec", "lait", "miel"]
    ingredient = [150*multiplicateur_arrondi, "g de myrtilles", 1*multiplicateur_arrondi, " banane", 150*multiplicateur_arrondi, "g de yaourt grec", 100*multiplicateur_arrondi, "ml de lait", 1*multiplicateur_arrondi, " cuillère à soupe de miel"]
    preparation = ["ÉTAPE 1\nDans un blender, placer les myrtilles et la banane.\nÉTAPE 2\nAjouter le yaourt grec, le lait et le miel dans le blender.\nÉTAPE 3\nMixer jusqu'à obtention d'une consistance lisse et crémeuse.\nÉTAPE 4\nVerser le smoothie dans un verre.\nÉTAPE 5\nDécorer avec des myrtilles fraîches si désiré.\nÉTAPE 6\nDéguster immédiatement."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

smoothie_myrtilles_banane
def omelette_poivrons_feta(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs", "poivrons", "huile d'olive", "sel", "poivre", "fromage feta"]
    ingredient = [4*multiplicateur_arrondi, " œufs", 100*multiplicateur_arrondi, "g de poivrons coupés en dés", 1*multiplicateur_arrondi, " cuillère à soupe d'huile d'olive", " sel", " poivre", 50*multiplicateur_arrondi, "g de fromage feta émietté"]
    preparation = ["ÉTAPE 1\nDans un bol, battre les œufs avec du sel et du poivre.\nÉTAPE 2\nFaire chauffer l'huile d'olive dans une poêle antiadhésive à feu moyen.\nÉTAPE 3\nAjouter les dés de poivrons dans la poêle et les faire sauter jusqu'à ce qu'ils soient tendres.\nÉTAPE 4\nVerser les œufs battus dans la poêle avec les poivrons et remuer doucement pour répartir les poivrons de manière uniforme.\nÉTAPE 5\nCuire l'omelette jusqu'à ce qu'elle soit prise sur le dessus mais encore légèrement baveuse au centre.\nÉTAPE 6\nParsemer le fromage feta émietté sur le dessus de l'omelette.\nÉTAPE 7\nPlier l'omelette en deux et laisser cuire encore quelques instants jusqu'à ce que le fromage feta soit légèrement fondu.\nÉTAPE 8\nTransférer l'omelette dans une assiette et servir chaud."]
    temps_de_preparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

omelette_poivrons_feta
def pancakes_fraises_sirop_erable(multiplicateur_arrondi):
    ingredient_pour_algo = ["œuf", "lait", "beurre fondu", "farine de blé", "sucre", "levure chimique", "fraises", "sirop d'érable"]
    ingredient = [2*multiplicateur_arrondi, " œufs", 100*multiplicateur_arrondi, "ml de lait", 25*multiplicateur_arrondi, "g de beurre fondu", 100*multiplicateur_arrondi, "g de farine de blé", 1*multiplicateur_arrondi, " cuillère à soupe de sucre", 1*multiplicateur_arrondi, " cuillère à café de levure chimique", 100*multiplicateur_arrondi, "g de fraises coupées en morceaux", 50*multiplicateur_arrondi, "ml de sirop d'érable"]
    preparation = ["ÉTAPE 1\nDans un bol, battre les œufs avec le lait et le beurre fondu.\nÉTAPE 2\nAjouter la farine de blé, le sucre, la levure chimique dans le bol avec le mélange d'œufs, de lait et de beurre fondu, et mélanger jusqu'à ce que la pâte soit lisse.\nÉTAPE 3\nIncorporer les fraises coupées en morceaux dans la pâte à pancakes.\nÉTAPE 4\nChauffer une poêle ou une plaque chauffante à feu moyen et y verser un peu de pâte à pancakes pour former des cercles.\nÉTAPE 5\nCuire les pancakes pendant environ 2 à 3 minutes de chaque côté, jusqu'à ce qu'ils soient dorés et gonflés.\nÉTAPE 6\nTransférer les pancakes cuits dans une assiette et les garder au chaud pendant que vous faites cuire le reste de la pâte.\nÉTAPE 7\nServir les pancakes chauds avec du sirop d'érable."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

pancakes_fraises_sirop_erable
def smoothie_epinards_avocat_mangue(multiplicateur_arrondi):
    ingredient_pour_algo = ["épinards", "avocat", "mangue", "yaourt grec", "lait", "miel"]
    ingredient = [100*multiplicateur_arrondi, "g d'épinards frais", 1*multiplicateur_arrondi, " avocat pelé et dénoyauté", 1*multiplicateur_arrondi, " mangue pelée et coupée en dés", 150*multiplicateur_arrondi, "g de yaourt grec", 100*multiplicateur_arrondi, "ml de lait", 1*multiplicateur_arrondi, " cuillère à soupe de miel"]
    preparation = ["ÉTAPE 1\nDans un blender, placer les épinards, l'avocat et la mangue.\nÉTAPE 2\nAjouter le yaourt grec, le lait et le miel dans le blender.\nÉTAPE 3\nMixer jusqu'à obtention d'une consistance lisse et crémeuse.\nÉTAPE 4\nVerser le smoothie dans un verre.\nÉTAPE 5\nDécorer avec des tranches de mangue si désiré.\nÉTAPE 6\nDéguster immédiatement."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

smoothie_epinards_avocat_mangue
def omelette_epinards_tomates_mozzarella(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs", "épinards", "tomates séchées", "huile d'olive", "sel", "poivre", "mozzarella"]
    ingredient = [4*multiplicateur_arrondi, " œufs", 100*multiplicateur_arrondi, "g d'épinards frais", 50*multiplicateur_arrondi, "g de tomates séchées hachées", 1*multiplicateur_arrondi, " cuillère à soupe d'huile d'olive", " sel", " poivre", 100*multiplicateur_arrondi, "g de mozzarella coupée en dés"]
    preparation = ["ÉTAPE 1\nDans un bol, battre les œufs avec du sel et du poivre.\nÉTAPE 2\nLaver et égoutter les épinards.\nÉTAPE 3\nDans une poêle antiadhésive, faire chauffer l'huile d'olive à feu moyen.\nÉTAPE 4\nAjouter les épinards dans la poêle et les faire sauter jusqu'à ce qu'ils soient ramollis.\nÉTAPE 5\nAjouter les tomates séchées hachées dans la poêle avec les épinards et remuer pendant 1 minute.\nÉTAPE 6\nVerser les œufs battus dans la poêle avec les épinards et les tomates séchées.\nÉTAPE 7\nParsemer les dés de mozzarella sur le dessus de l'omelette.\nÉTAPE 8\nCuire l'omelette jusqu'à ce qu'elle soit prise sur le dessus mais encore légèrement baveuse au centre.\nÉTAPE 9\nPlier l'omelette en deux et laisser cuire encore quelques instants jusqu'à ce que la mozzarella soit légèrement fondue.\nÉTAPE 10\nTransférer l'omelette dans une assiette et servir chaud."]
    temps_de_preparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

omelette_epinards_tomates_mozzarella
def pancakes_banane_chocolat(multiplicateur_arrondi):
    ingredient_pour_algo = ["œuf", "lait", "beurre fondu", "farine de blé", "sucre", "levure chimique", "banane", "pépites de chocolat"]
    ingredient = [2*multiplicateur_arrondi, " œufs", 100*multiplicateur_arrondi, "ml de lait", 25*multiplicateur_arrondi, "g de beurre fondu", 100*multiplicateur_arrondi, "g de farine de blé", 1*multiplicateur_arrondi, " cuillère à soupe de sucre", 1*multiplicateur_arrondi, " cuillère à café de levure chimique", 1*multiplicateur_arrondi, " banane écrasée", 50*multiplicateur_arrondi, "g de pépites de chocolat"]
    preparation = ["ÉTAPE 1\nDans un bol, battre les œufs avec le lait et le beurre fondu.\nÉTAPE 2\nAjouter la farine de blé, le sucre, la levure chimique dans le bol avec le mélange d'œufs, de lait et de beurre fondu, et mélanger jusqu'à ce que la pâte soit lisse.\nÉTAPE 3\nIncorporer la banane écrasée et les pépites de chocolat dans la pâte à pancakes.\nÉTAPE 4\nChauffer une poêle ou une plaque chauffante à feu moyen et y verser un peu de pâte à pancakes pour former des cercles.\nÉTAPE 5\nCuire les pancakes pendant environ 2 à 3 minutes de chaque côté, jusqu'à ce qu'ils soient dorés et gonflés.\nÉTAPE 6\nTransférer les pancakes cuits dans une assiette et les garder au chaud pendant que vous faites cuire le reste de la pâte.\nÉTAPE 7\nServir les pancakes chauds avec du sirop d'érable ou du miel si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

pancakes_banane_chocolat
def smoothie_fraises_vanille(multiplicateur_arrondi):
    ingredient_pour_algo = ["fraises", "yaourt grec", "lait d'amande", "vanille", "miel"]
    ingredient = [150*multiplicateur_arrondi, "g de fraises fraîches", 150*multiplicateur_arrondi, "g de yaourt grec", 150*multiplicateur_arrondi, "ml de lait d'amande", 1*multiplicateur_arrondi, "cuillère à café d'extrait de vanille", 1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    preparation = ["ÉTAPE 1\nDans un blender, placer les fraises, le yaourt grec, le lait d'amande, l'extrait de vanille et le miel.\nÉTAPE 2\nMixer jusqu'à obtenir une consistance lisse et crémeuse.\nÉTAPE 3\nVerser le smoothie dans un verre.\nÉTAPE 4\nDéguster immédiatement."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

smoothie_fraises_vanille
def omelette_epinards_champignons_cheddar(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs", "épinards", "champignons", "huile d'olive", "sel", "poivre", "fromage cheddar"]
    ingredient = [4*multiplicateur_arrondi, "œufs", 100*multiplicateur_arrondi, "g d'épinards frais", 100*multiplicateur_arrondi, "g de champignons tranchés", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre", 50*multiplicateur_arrondi, "g de fromage cheddar râpé"]
    preparation = ["ÉTAPE 1\nDans un bol, battre les œufs avec du sel et du poivre.\nÉTAPE 2\nFaire chauffer l'huile d'olive dans une poêle antiadhésive à feu moyen.\nÉTAPE 3\nAjouter les épinards et les champignons dans la poêle et les faire sauter jusqu'à ce qu'ils soient tendres.\nÉTAPE 4\nVerser les œufs battus dans la poêle avec les épinards et les champignons.\nÉTAPE 5\nParsemer le fromage cheddar râpé sur le dessus de l'omelette.\nÉTAPE 6\nCuire l'omelette jusqu'à ce qu'elle soit prise sur le dessus mais encore légèrement baveuse au centre.\nÉTAPE 7\nPlier l'omelette en deux et laisser cuire encore quelques instants jusqu'à ce que le fromage cheddar soit fondu.\nÉTAPE 8\nTransférer l'omelette dans une assiette et servir chaud."]
    temps_de_preparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

omelette_epinards_champignons_cheddar
def pancakes_noix_coco_chocolat(multiplicateur_arrondi):
    ingredient_pour_algo = ["œuf", "lait de coco", "beurre fondu", "farine de blé", "sucre", "levure chimique", "noix de coco râpée", "pépites de chocolat"]
    ingredient = [2*multiplicateur_arrondi, "œufs", 100*multiplicateur_arrondi, "ml de lait de coco", 25*multiplicateur_arrondi, "g de beurre fondu", 100*multiplicateur_arrondi, "g de farine de blé", 1*multiplicateur_arrondi, "cuillère à soupe de sucre", 1*multiplicateur_arrondi, "cuillère à café de levure chimique", 25*multiplicateur_arrondi, "g de noix de coco râpée", 50*multiplicateur_arrondi, "g de pépites de chocolat"]
    preparation = ["ÉTAPE 1\nDans un bol, battre les œufs avec le lait de coco et le beurre fondu.\nÉTAPE 2\nAjouter la farine de blé, le sucre, la levure chimique dans le bol avec le mélange d'œufs, de lait de coco et de beurre fondu, et mélanger jusqu'à ce que la pâte soit lisse.\nÉTAPE 3\nIncorporer la noix de coco râpée et les pépites de chocolat dans la pâte à pancakes.\nÉTAPE 4\nChauffer une poêle ou une plaque chauffante à feu moyen et y verser un peu de pâte à pancakes pour former des cercles.\nÉTAPE 5\nCuire les pancakes pendant environ 2 à 3 minutes de chaque côté, jusqu'à ce qu'ils soient dorés et gonflés.\nÉTAPE 6\nTransférer les pancakes cuits dans une assiette et les garder au chaud pendant que vous faites cuire le reste de la pâte.\nÉTAPE 7\nServir les pancakes chauds avec du sirop d'érable ou du miel si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

pancakes_noix_coco_chocolat
def smoothie_banane_amandes(multiplicateur_arrondi):
    ingredient_pour_algo = ["banane", "lait d'amande", "beurre d'amande", "miel"]
    ingredient = [1*multiplicateur_arrondi, "banane", 200*multiplicateur_arrondi, "ml de lait d'amande", 1*multiplicateur_arrondi, "cuillère à soupe de beurre d'amande", 1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    preparation = ["ÉTAPE 1\nDans un blender, placer la banane coupée en morceaux.\nÉTAPE 2\nAjouter le lait d'amande, le beurre d'amande et le miel dans le blender.\nÉTAPE 3\nMixer jusqu'à obtention d'une consistance lisse et crémeuse.\nÉTAPE 4\nVerser le smoothie dans un verre.\nÉTAPE 5\nDéguster immédiatement."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

smoothie_banane_amandes

def omelette_champignons_poivrons_suisse(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs", "champignons", "poivrons", "huile d'olive", "sel", "poivre", "fromage suisse"]
    ingredient = [4*multiplicateur_arrondi, "œufs", 100*multiplicateur_arrondi, "g de champignons tranchés", 100*multiplicateur_arrondi, "g de poivrons coupés en dés", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre", 50*multiplicateur_arrondi, "g de fromage suisse râpé"]
    preparation = ["ÉTAPE 1\nDans un bol, battre les œufs avec du sel et du poivre.\nÉTAPE 2\nDans une poêle antiadhésive, faire chauffer l'huile d'olive à feu moyen.\nÉTAPE 3\nAjouter les champignons et les poivrons dans la poêle et les faire sauter jusqu'à ce qu'ils soient tendres.\nÉTAPE 4\nVerser les œufs battus dans la poêle avec les champignons et les poivrons.\nÉTAPE 5\nParsemer le fromage suisse râpé sur le dessus de l'omelette.\nÉTAPE 6\nCuire l'omelette jusqu'à ce qu'elle soit prise sur le dessus mais encore légèrement baveuse au centre.\nÉTAPE 7\nPlier l'omelette en deux et laisser cuire encore quelques instants jusqu'à ce que le fromage suisse soit fondu.\nÉTAPE 8\nTransférer l'omelette dans une assiette et servir chaud."]
    temps_de_preparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous
omelette_champignons_poivrons_suisse

def pancakes_pomme_cannelle(multiplicateur_arrondi):
    ingredient_pour_algo = ["œuf", "lait", "beurre fondu", "farine de blé", "sucre", "levure chimique", "pomme", "cannelle"]
    ingredient = [2*multiplicateur_arrondi, "œufs", 100*multiplicateur_arrondi, "ml de lait", 25*multiplicateur_arrondi, "g de beurre fondu", 100*multiplicateur_arrondi, "g de farine de blé", 1*multiplicateur_arrondi, "cuillère à soupe de sucre", 1*multiplicateur_arrondi, "cuillère à café de levure chimique", 1*multiplicateur_arrondi, "pomme coupée en petits dés", 1*multiplicateur_arrondi, "cuillère à café de cannelle"]
    preparation = ["ÉTAPE 1\nDans un bol, battre les œufs avec le lait et le beurre fondu.\nÉTAPE 2\nAjouter la farine de blé, le sucre, la levure chimique dans le bol avec le mélange d'œufs, de lait et de beurre fondu, et mélanger jusqu'à ce que la pâte soit lisse.\nÉTAPE 3\nIncorporer les dés de pomme et la cannelle dans la pâte à pancakes.\nÉTAPE 4\nChauffer une poêle ou une plaque chauffante à feu moyen et y verser un peu de pâte à pancakes pour former des cercles.\nÉTAPE 5\nCuire les pancakes pendant environ 2 à 3 minutes de chaque côté, jusqu'à ce qu'ils soient dorés et gonflés.\nÉTAPE 6\nTransférer les pancakes cuits dans une assiette et les garder au chaud pendant que vous faites cuire le reste de la pâte.\nÉTAPE 7\nServir les pancakes chauds avec du sirop d'érable ou du miel si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous
pancakes_pomme_cannelle

tuplerepaspourmidi_prise_de_mas=[boeuf_saute_legumes_riz_brun,burger_dinde_frites_patates_douces,burritos_haricots_noirs_avocat,casserole_pommes_terre_saucisses,chili_con_carne_riz,crepes_pepites_chocolat,lasagnes_epinards_ricotta,muesli_fruits_noix,muffins_pepites_chocolat_bananes,omelette_champignons_fromage,omelette_champignons_fromage_bleu,omelette_champignons_fromage_suisse,omelette_champignons_poivrons_suisse,omelette_epinards_champignons_cheddar,omelette_epinards_cheddar,omelette_epinards_feta,omelette_epinards_fromage_feta,omelette_epinards_suisse,omelette_epinards_tomates_mozzarella,omelette_legumes_fromage,omelette_poivrons_cheddar,omelette_poivrons_feta,omelette_poivrons_fromage_cheddar,omelette_tomates_mozzarella,pain_bananes_noix,pain_perdu_fruits_cannelle,pain_viande_puree_pommes_terre,pancakes_banane_chocolat,pancakes_banane_noix,pancakes_chocolat_banane,pancakes_chocolat_bananes,pancakes_chocolat_noix,pancakes_fraises_sirop_erable,pancakes_framboises_vanille,pancakes_myrtilles,pancakes_myrtilles_citron,pancakes_myrtilles_noix,pancakes_myrtilles_sirop_erable,pancakes_noix_coco_chocolat,pancakes_patate_douce_cannelle,pancakes_pomme_cannelle,pates_bolognaise,pates_poulet_alfredo,pates_primavera,pizza_garniture_au_choix,pizza_maison_viande_legumes,poisson_four_patates_douces,poisson_grille_asperges_pommes_terre,poulet_curry_riz_basmati,poulet_roti_patates_douces,poulet_roti_pommes_terre_haricots_verts,poulet_teriyaki_riz_blanc,poulet_teriyaki_riz_legumes_sautes,risotto_champignons,risotto_champignons_poulet,riz_frit_legumes_oeufs_brouilles,salade_crevettes_avocat,salade_poulet_cajun_mais_grille,salade_poulet_cesar,salade_quinoa_legumes_grilles,salade_quinoa_poulet_grille,saumon_grille_quinoa,spaghetti_sauce_bolognaise,steak_boeuf_grille_riz_brun,tacos_poulet_grille,tacos_poulet_salsa,tacos_poulet_salsa_avocat,toast_avocat_oeufs_poches,wrap_boeuf_legumes_grilles,wrap_saumon_avocat,wrap_thon_legumes]

