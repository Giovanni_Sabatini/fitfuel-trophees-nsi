from PySide6.QtCore import (QRect, QSize, QTimer, Qt)
from PySide6.QtWidgets import (QApplication, QLabel, QPushButton, QMainWindow, QVBoxLayout)
from PySide6.QtGui import (QFont, QPixmap, QImage)

import sys
import cv2
import mediapipe as mp
import math
import os

from ui_Recette import lire_json

mp_drawing = mp.solutions.drawing_utils
mp_pose = mp.solutions.pose

class Exercise_Tracker(QMainWindow):
    
    def __init__(self, liste, niveau):
        """ Initialise la classe du tracking caméra

        Args:
            liste (list): La liste d'exercices que l'utilisateur souhaite pratiquer
            niveau (int, optional): Le niveau de l'utilisateur. Par défaut à zero s'il ne le change jamais.
        """
        super().__init__()
        #Sélectionner la bonne caméra
        info = lire_json("parametres.json")
        self.num_camera = info[7]
        # Liste des sports
        liste_exercice = liste
        self.liste_exercice_tracking = [liste_exercice[niveau - 1][i] for i in range(1, len(liste_exercice[niveau - 1]), 2)] #On transforme le tableau de tableau contenant les trois niveaux d'exercices, en un seul tableau contenant le niveau voulu seulement
        self.indice_exercice = 0 # Au fur et à mesure que l'utilisateur change d'exercice, on augmente cette variable

        self.initUI()
    def initUI(self):
        self.resize(1920, 1080)
        font = QFont()
        font.setPointSize(16)
        font.setBold(True)
        self.setFont(font)
        self.setGeometry(100, 100, 800, 600)

        # Wiget central, c'est le widget "parent" dans lequel on met tous le reste
        central_widget = QLabel(self)
        central_widget.setAlignment(Qt.AlignCenter)
        self.setCentralWidget(central_widget)
        self.video_label = QLabel(self)
        self.video_label.setAlignment(Qt.AlignCenter)
        central_layout = QVBoxLayout()
        central_layout.addWidget(self.video_label)
        central_widget.setLayout(central_layout)

        #Revenir à l'accueil
        self.button_return = QPushButton("Retour", self)
        self.button_return.setGeometry(30, 20, 150, 40)
        self.button_return.setStyleSheet(u"QPushButton {border-radius: 20px; background-color: rgb(225, 225, 225); font-size: 20px;} QPushButton:hover {background: rgb(237, 255, 255);}")
        self.button_return.clicked.connect(self.close_window)

        #Premier label affichant le temps
        self.label = QLabel(self)
        self.label.setGeometry(QRect(30, 100, 171, 61))
        self.label.setFont(font)
        self.label.setStyleSheet(u"QLabel {border-radius: 20px;background-color: rgb(17, 182, 91);} QLabel:hover {background:rgb(17, 142, 61)}")
        self.label.setAlignment(Qt.AlignCenter)
        
        #Second label affichant le score
        self.label_2 = QLabel(self)
        self.label_2.setGeometry(QRect(30, 180, 171, 61))
        self.label_2.setFont(font)
        self.label_2.setStyleSheet(u"QLabel {border-radius: 20px;background-color: rgb(206, 75, 43);} QLabel:hover {background:rgb(176, 55, 23)}")
        self.label_2.setAlignment(Qt.AlignCenter)
        
        #Ce bouton servira à montrer, ou non, la démarche de l'algorithme, en affichant les points du corps tels qu'il les interprètent
        self.button_nerd = QPushButton("Nerd Stats",self)
        self.button_nerd.setGeometry(QRect(30, 260, 171, 61))
        self.button_nerd.setFont(font)
        self.button_nerd.setStyleSheet(u"QPushButton {border-radius: 20px; background-color: rgb(106, 75, 143);} QPushButton:hover {background:rgb(86, 55, 123)}")
        self.button_nerd.clicked.connect(self.show_nerd_stats)
        self.can_show_nerd_stats = False
        
        # Creer le bouton qui démarre l'exercice
        push_up_btn = QPushButton('Commencer', self)
        push_up_btn.clicked.connect(self.start_tracking)
        central_layout.addWidget(push_up_btn)
        
        # Initialiser la capture video
        self.cap = cv2.VideoCapture(self.num_camera)
        self.timer = QTimer(self) # Un timer permet d'envoyer toutes les x millisecondes, un signal qui va automatiquement executer la fonction.
        self.timer.timeout.connect(self.update_frame) # Ce timer est charge d'afficher chaque 30 ms la caméra sur l'ecran
        self.is_tracking = False

        #Initialiser le chrono et le score
        self.temps_chrono = 0
        self.chronometre = QTimer(self) # Ce deuxieme timer envoie un signal chaque seconde pour augmenter le chronometre de 1
        self.chronometre.timeout.connect(self._update)

        self.score = 0
        self.tracking_a_commence = False
        self.compteur_de_video = 0 # Permet de tenir cmopte de l'avancée de l'utilisateur et de montrer les bonnes vidéos

        # Creation d'une instance de Pose pour détectter les parties du corps. Ces parametres sont optimaux et s'adaptent à toutes les situations
        self.pose = mp_pose.Pose(min_detection_confidence=0.5, min_tracking_confidence=0.5) 

        # Ces deux lignes sont nécessaires à Pyside, le module d'interface graphique, pour afficher les bons textes, les chaines de caractères appropriees
        self.label.setText("TEMPS")
        self.label_2.setText("SCORE")
        # Le parametre "Maximized" permet d'occuper toute la taille de l'écran
        self.showMaximized()

    def update_chronometre(self):
        """Permet d'augmenter le temps du chronometre de 1 chaque seconde. Avantage : c'est indépendant de la fréquence d'execution du processeur, dépend plutot d'une horloge
        """
        self.temps_chrono += 1
        self.label.setText(f"0:{self.temps_chrono}")
    
    def update_score(self):
        """Si l'utilisateur effectue correctement le mouvemment, cette fonction est appelée pour le recompenser. Cela permet de donner un aspect plus ludique à l'exercice physique
        """
        self.score += 10
        self.label_2.setText(f'{self.score}')

    def start_tracking(self):
        """Permet d'initialiser, charger, et lancer toute la procédure de tracking
        """
        if not self.is_tracking:
            self.show_video()
            self.tracking_a_commence = True
            self.compteur_de_video = 1
            self.timer.start(30)  # Update toutes les 30 millisecondes
            self.chronometre.start(1000) # Update toutes les secondes
            self.is_tracking = True # Permet de dire au programme qu'il peut afficher la caméra et commencer le tracking corporel
    
    def _update(self):
        self.update_chronometre()
        if self.temps_chrono >= 30: # Lorsque le temps d'effort dépasse 20 secondes, on passe à l'exercice d'après
            if self.indice_exercice == len(self.liste_exercice_tracking)-1: # On vérifier si l'utilisateur en est au dernier exercice
                if self.temps_chrono >= 30:
                    self.timer.stop()
                    self.is_tracking = False
                    chemin_image_workout = os.path.join(os.getcwd(), "Icone/workout_finished.png").replace("\\", "/")
                    image_fin = QPixmap(chemin_image_workout)
                    self.video_label.setPixmap(image_fin.scaled(self.video_label.size() - QSize(50, 50), Qt.KeepAspectRatio))
                    self.chronometre.stop()
            else: # Quannd il arrive à la fin du temps d'effort, on arrête la caméra, c'est à dire on arrête le signal "timer" qui affiche la caméra, et on montre la vidéo de l'exercice suivant
                self.temps_chrono = 0
                self.timer.stop()
                self.is_tracking = False
                self.show_video()
            
    def show_video(self):
        """Permet d'afficher la video d'animation illustrant la bonne pratique de l'exercice
        """
        if not self.is_tracking:
            self.cap.release() # On arrête de solliciter la caméra
            video_path = f'animations/{self.liste_exercice_tracking[self.indice_exercice+1*self.compteur_de_video]}.mp4'
            self.video_player = cv2.VideoCapture(video_path) # On charge la vidéo
            while True:
                ret, frame = self.video_player.read()
                if ret:
                    # Convertir BGR to RGB, OpenCV et Pyside utilisent des formats d'images différents
                    rgb_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                    
                    # De même
                    height, width, channel = rgb_frame.shape
                    bytes_per_line = 3 * width
                    video_img = QImage(rgb_frame.data, width, height, bytes_per_line, QImage.Format_RGB888)
                    
                    # Convertir QImage à QPixmap
                    video_pixmap = QPixmap.fromImage(video_img)
                    self.video_label.setPixmap(video_pixmap.scaled(self.video_label.size() - QSize(100, 100), Qt.KeepAspectRatio))
                    
                    if cv2.waitKey(25) & 0xFF == ord('q'): # Si l'utilisateur veut arrêter le tracking
                        break
                else:
                    self.video_player.release() # Si la video est terminee, on arrête de solliciter le chargement de la video 
                    break
            # On libère toutes les ressources d'images
            cv2.destroyAllWindows()
            self.restart_tracking()

    def restart_tracking(self):
        """Si la video d'animation de l'exercice est terminee, on appelle cette fonction pour remettre en marche la camera et reprenndre le tracking corporel
        """
        self.cap = cv2.VideoCapture(self.num_camera)
        self.is_tracking = True
        self.timer.start(30)
        if self.indice_exercice < len(self.liste_exercice_tracking)-1 and self.tracking_a_commence: # On verifie qu'on puisse augmenter l'indice d'exercice sans avoir une erreur IndexERROR
            self.indice_exercice += 1

    def update_frame(self):
        """Cette fonction permet de récuperer ce que voit la camera pour analyser l'image et detecter les points du corps
        """
        if self.is_tracking:
            ret, frame = self.cap.read()

            if not ret: # Si une ressource video, camera ou animation, est deja engagee dans le processus d'affichage, ne rien faire
                self.cap.release()
                return

            frame = cv2.flip(frame, 1) # Récupérer l'image de la caméra

            # Convertir BGR à RGB
            rgb_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

            results = self.pose.process(rgb_frame) # On analyse l'image obtenue

            if results.pose_landmarks and self.is_tracking:
                landmarks = results.pose_landmarks.landmark # On détecte les points du corps

                if self.can_show_nerd_stats: # Si l'utilisateur le souhaite, il peut afficher ce que voit l'algorithme pour mieux comprendre son fonctionnement
                    mp_drawing.draw_landmarks(rgb_frame, results.pose_landmarks, mp_pose.POSE_CONNECTIONS)

                # Informer l'utillisateur de la bonne ou mauvaise execution du mouvemment 
                cv2.putText(rgb_frame, f'Position: {"Bonne" if self.epaule_hanche_genou(landmarks, f"{self.liste_exercice_tracking[self.indice_exercice]}") or self.epaule_coude_poignet(landmarks, f"{self.liste_exercice_tracking[self.indice_exercice]}")  else "Mauvaise"}', (10, 30),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0) if self.epaule_hanche_genou(landmarks, f"{self.liste_exercice_tracking[self.indice_exercice]}") or self.epaule_coude_poignet(landmarks, f"{self.liste_exercice_tracking[self.indice_exercice]}") else (0, 0, 255), 2, cv2.LINE_AA)
                # Augmenter le score
                try:
                    if self.epaule_hanche_genou(landmarks, self.liste_exercice_tracking[self.indice_exercice]):
                        self.update_score()
                    elif self.epaule_coude_poignet(landmarks, self.liste_exercice_tracking[self.indice_exercice]):
                        self.update_score()
                except:
                    pass

            # Convertir les paramtres d'images pour les afficher sans distortion
            h, w, c = frame.shape
            bytes_per_line = c * w
            img = QImage(rgb_frame.data, w, h, bytes_per_line, QImage.Format_RGB888)

            # Convertir QImage en QPixmap
            pixmap = QPixmap.fromImage(img)
            self.video_label.setPixmap(pixmap.scaled(self.video_label.size(), Qt.KeepAspectRatio))

    def epaule_hanche_genou(self, landmarks, exercice):
        """Cette fonction regroupe plusieurs exercices, et calcule les parametres associes à leur bonne execution, pour renvoyer bon ou mauvais

        Args:
            landmarks (list) : Une liste des points du corps, et leurs coordonnees dans l'espace
            exercice : L'exercice à analyser

        Returns:
            bool: La qualité de l'exercice
        """
        epaule_gauche = [landmarks[mp_pose.PoseLandmark.LEFT_SHOULDER.value].x,
                         landmarks[mp_pose.PoseLandmark.LEFT_SHOULDER.value].y]

        hanche_gauche = [landmarks[mp_pose.PoseLandmark.LEFT_HIP.value].x,
                    landmarks[mp_pose.PoseLandmark.LEFT_HIP.value].y]

        genou_gauche = [landmarks[mp_pose.PoseLandmark.LEFT_KNEE.value].x,
                     landmarks[mp_pose.PoseLandmark.LEFT_KNEE.value].y]

        epaule_droite = [landmarks[mp_pose.PoseLandmark.RIGHT_SHOULDER.value].x,
                          landmarks[mp_pose.PoseLandmark.RIGHT_SHOULDER.value].y]

        hanche_droite = [landmarks[mp_pose.PoseLandmark.RIGHT_HIP.value].x,
                     landmarks[mp_pose.PoseLandmark.RIGHT_HIP.value].y]

        genou_droit = [landmarks[mp_pose.PoseLandmark.RIGHT_KNEE.value].x,
                      landmarks[mp_pose.PoseLandmark.RIGHT_KNEE.value].y]

        coude_gauche = [landmarks[mp_pose.PoseLandmark.LEFT_ELBOW.value].x,
                        landmarks[mp_pose.PoseLandmark.LEFT_ELBOW.value].y]

        coude_droit = [landmarks[mp_pose.PoseLandmark.RIGHT_ELBOW.value].x,
                       landmarks[mp_pose.PoseLandmark.RIGHT_ELBOW.value].y]

        cheville_gauche = [landmarks[mp_pose.PoseLandmark.LEFT_ANKLE.value].x,
                           landmarks[mp_pose.PoseLandmark.LEFT_ANKLE.value].y]

        cheville_droite = [landmarks[mp_pose.PoseLandmark.RIGHT_ANKLE.value].x,
                           landmarks[mp_pose.PoseLandmark.RIGHT_ANKLE.value].y]

        index_pied_gauche = [landmarks[mp_pose.PoseLandmark.LEFT_FOOT_INDEX.value].x,
                           landmarks[mp_pose.PoseLandmark.LEFT_FOOT_INDEX.value].y]

        index_pied_droit = [landmarks[mp_pose.PoseLandmark.RIGHT_FOOT_INDEX.value].x,
                             landmarks[mp_pose.PoseLandmark.RIGHT_FOOT_INDEX.value].y]
        # crunch, hip thurst, abdo

        if "abdominaux" in exercice:

            left_abdo_angle = self.calculate_angle(epaule_gauche, hanche_gauche, genou_gauche)
            right_abdo_angle = self.calculate_angle(epaule_droite, hanche_droite, genou_droit)

            is_good_abdo = (left_abdo_angle < 65) or (right_abdo_angle < 65)
            if exercice == "abdominaux_en_v":
                jambe_gauche, jambe_droite = self.calculate_angle(hanche_gauche, genou_gauche, cheville_gauche), self.calculate_angle(hanche_droite, genou_droit, cheville_droite)
                return is_good_abdo and (jambe_gauche > 130 or jambe_droite > 130)
            return is_good_abdo
        elif "crunch" in exercice:
            left_crunch_angle = self.calculate_angle(epaule_gauche, hanche_gauche, genou_gauche)
            right_crunch_angle = self.calculate_angle(epaule_droite, hanche_droite, genou_droit)
    
            is_good_crunch = (left_crunch_angle < 90) or (right_crunch_angle < 90)

            if exercice == "crunch_type_velo":
                distance_coude_genou_gauche, distance_coude_genou_droit = self.calculate_distance(coude_droit, genou_gauche), self.calculate_distance(coude_gauche, genou_droit)
                return distance_coude_genou_gauche < 0.08 or distance_coude_genou_droit < 0.08

            elif exercice == "crunch_croise":
                distance_coude_genou_gauche, distance_coude_genou_droit = self.calculate_distance(coude_droit,genou_gauche), self.calculate_distance(coude_gauche, genou_droit)
                return is_good_crunch and (distance_coude_genou_gauche < 0.08 or distance_coude_genou_droit < 0.08)
            return is_good_crunch

        elif exercice == "planche_laterale" or exercice == "pont_lateral":
            ligne_du_corps_gauche, ligne_du_corps_droit = self.calculate_angle(epaule_gauche, hanche_gauche,genou_gauche), self.calculate_angle(epaule_droite, hanche_droite, genou_droit)
            angle_planche_lateral_gauche = self.calculate_angle(coude_gauche, epaule_gauche, hanche_gauche)
            angle_planche_lateral_droit = self.calculate_angle(coude_droit, epaule_droite, hanche_droite)

            is_good_planche_lateral = (angle_planche_lateral_gauche > 70 and 160 < ligne_du_corps_gauche < 200) or (angle_planche_lateral_droit > 70 and 160 < ligne_du_corps_droit < 200)

            return is_good_planche_lateral

        elif exercice == "chenille":
            taille_gauche, taille_droite = self.calculate_distance(cheville_gauche, epaule_gauche), self.calculate_distance(cheville_droite, epaule_droite)
            chenille_gauche, chenille_droite = self.calculate_distance(cheville_gauche, coude_gauche), self.calculate_distance(cheville_droite, coude_droit)
            return chenille_droite > taille_droite or chenille_gauche > taille_gauche

        elif exercice == "adducteurs_lateraux_couche":
            adducteur_gauche, adducteur_droit = self.calculate_angle(hanche_droite, hanche_gauche, genou_gauche), self.calculate_angle(hanche_gauche, hanche_droite, genou_droit)
            return adducteur_gauche < 80 or adducteur_droit < 50

        elif exercice == "escalade":
            escalade_gauche, escalade_droite = self.calculate_angle(epaule_gauche, hanche_gauche, genou_gauche), self.calculate_angle(epaule_droite, hanche_droite, genou_droit)
            return escalade_droite < 70 or escalade_gauche < 70

        elif exercice == "ciseaux_bras":
            ciseaux_gauche, ciseaux_droit = self.calculate_angle(epaule_droite, epaule_gauche, coude_gauche), self.calculate_angle(epaule_gauche, epaule_droite, coude_droit)
            return ciseaux_gauche < 80 or ciseaux_droit < 90

        elif exercice == "hip_thurst":
            left_hip_thurst_angle = self.calculate_angle(epaule_gauche, hanche_gauche, genou_gauche)
            right_hip_thurst_angle = self.calculate_angle(epaule_droite, hanche_droite, genou_droit)

            is_good_hip_thurst = (170 < left_hip_thurst_angle < 220) or (150 < right_hip_thurst_angle < 200)

            return is_good_hip_thurst

        elif exercice == "planche_diagonale":
            planche_diag_gauche, planche_diag_droite = self.calculate_angle(epaule_gauche, hanche_gauche, genou_gauche),self.calculate_angle(epaule_droite, hanche_droite, genou_droit)
            return (165 < planche_diag_gauche < 190) or (165 < planche_diag_droite < 190)

        elif exercice == "levee_genoux":
            levee_genoux_gauche, levee_genoux_droit = self.calculate_angle(epaule_gauche, hanche_gauche, genou_gauche), self.calculate_angle(epaule_droite, hanche_droite, genou_droit)
            return (levee_genoux_gauche < 100) or (levee_genoux_droit < 100)
        elif exercice == "levee_jambes_arriere":
            levee_jambe_arriere_gauche, levee_jambe_arrier_droit = self.calculate_angle(epaule_gauche, hanche_gauche, genou_gauche), self.calculate_angle(epaule_droite, hanche_droite, genou_droit)
            return levee_jambe_arriere_gauche < 160 or levee_jambe_arrier_droit > 190

        elif "squat" in exercice:
            squat_gauche, squat_droit = self.calculate_angle(hanche_gauche, genou_gauche, cheville_gauche), self.calculate_angle(hanche_droite, genou_droit, cheville_droite)

            if exercice == "squat_sumo":
                distance_pieds = self.calculate_distance(cheville_gauche, cheville_droite)
                return (squat_gauche > 215 or squat_droit < 110) and distance_pieds > 0.25

            return squat_gauche > 190 and squat_droit < 150

        elif exercice == "accroupie_avec_genou_flechi":
            accroupie_gauche, accroupie_droite = self.calculate_angle(hanche_gauche, genou_gauche, cheville_gauche), self.calculate_angle(hanche_droite, genou_droit, cheville_droite)
            return (70 < accroupie_droite < 110) or (70 < accroupie_gauche < 110)
        elif "fente" in exercice:
            left_hip_knee_ankle_angle = self.calculate_angle(hanche_gauche, genou_gauche, cheville_gauche)
            right_hip_knee_ankle_angle = self.calculate_angle(hanche_droite, genou_droit, cheville_droite)

            is_good_lunge = (left_hip_knee_ankle_angle < 90) or (right_hip_knee_ankle_angle < 90)

            if exercice == "fente_reverence":
                profondeur_pied_gauche, profondeur_pied_droit = landmarks[mp_pose.PoseLandmark.LEFT_ANKLE.value].z, landmarks[mp_pose.PoseLandmark.RIGHT_ANKLE.value].z
                return (60 < left_hip_knee_ankle_angle < 110 and profondeur_pied_gauche > profondeur_pied_droit + 0.2) or (60 < right_hip_knee_ankle_angle < 110 and profondeur_pied_droit > profondeur_pied_gauche + 0.2) 

            return is_good_lunge

        elif exercice == "fire_hydrant":
            fire_hydrant_gauche, fire_hydrant_droit = self.calculate_angle(genou_gauche, hanche_gauche, genou_droit), self.calculate_angle(genou_droit, hanche_droite, genou_gauche)
            return fire_hydrant_gauche > 70 or fire_hydrant_droit > 70

        elif "leve_de_jambes" in exercice:
            leve_gauche, leve_droit = self.calculate_angle(epaule_gauche, hanche_gauche, cheville_gauche), self.calculate_angle(epaule_droite, hanche_droite, cheville_droite)
            if exercice == "leve_de_jambes_lateral_gauche":
                lever_jambes_gauche = self.calculate_angle(cheville_gauche, hanche_gauche, cheville_droite)
                return lever_jambes_gauche > 55
            elif exercice == "leve_de_jambes_lateral_droite":
                lever_jambes_droite = self.calculate_angle(cheville_droite, hanche_droite, cheville_gauche)
                return 15 < lever_jambes_droite < 315

            return leve_gauche < 100 or leve_droit < 100

        elif exercice == "battements_de_jambes":
            battement_gauche, battement_droit = self.calculate_angle(epaule_gauche, hanche_gauche, cheville_gauche), self.calculate_angle(epaule_droite, hanche_droite, cheville_droite)
            return battement_droit < 170 or battement_gauche < 170

        elif exercice == 'chaise':
            chaise_gauche, chaise_droite = self.calculate_angle(hanche_gauche, genou_gauche, cheville_gauche), self.calculate_angle(hanche_droite, genou_droit, cheville_droite)
            return (75 < chaise_gauche < 95) or (75 < chaise_droite < 95)

        elif exercice == "superman":
            superman_gauche, superman_droit = self.calculate_angle(epaule_gauche, hanche_gauche, cheville_gauche), self.calculate_angle(epaule_droite, hanche_droite, cheville_droite)
            return superman_droit < 170 or superman_gauche < 170

        elif exercice == 'etirement_quadriceps_contre_mur':
            quadriceps_gauche, quadriceps_droit = self.calculate_angle(cheville_gauche, genou_gauche, hanche_gauche), self.calculate_angle(cheville_droite, genou_droit, hanche_droite)
            return quadriceps_gauche < 40 or quadriceps_droit < 40
        elif exercice == "etirement_genou_poitrine":
            distance_poitrine_genou_gauche, distance_poitrine_genou_droit = self.calculate_distance(epaule_gauche, genou_gauche), self.calculate_distance(epaule_droite, genou_droit)
            return distance_poitrine_genou_gauche < 0.2 or distance_poitrine_genou_droit < 0.2
        elif exercice == "extension_mollet_contre_le_mur":
            mollet_gauche, mollet_droit = self.calculate_angle(index_pied_gauche, cheville_gauche, genou_gauche), self.calculate_angle(index_pied_droit, cheville_droite, genou_droit)
            return mollet_droit < 100 or mollet_gauche < 100
        elif exercice == "coups_de_pieds_fessier":
            return self.calculate_distance(cheville_droite, hanche_droite) < 0.3 or self.calculate_distance(cheville_gauche, hanche_gauche) < 0.3
        elif exercice == "donkey_kicks":
            levee_jambe_arriere_gauche, levee_jambe_arrier_droit = self.calculate_angle(epaule_gauche, hanche_gauche, genou_gauche), self.calculate_angle(epaule_droite, hanche_droite, genou_droit)
            angle_droit_jambe_gauche, angle_droit_jambe_droite = self.calculate_angle(hanche_gauche, genou_gauche, cheville_gauche), self.calculate_angle(hanche_droite, genou_droit, cheville_droite)
            return (levee_jambe_arriere_gauche > 170 or levee_jambe_arrier_droit > 170) and (angle_droit_jambe_gauche < 100 or angle_droit_jambe_droite < 100)

    def epaule_coude_poignet(self, landmarks, exercice):
        """Cette fonction regroupe plusieurs exercices, et calcule les parametres associes à leur bonne execution, pour renvoyer bon ou mauvais

        Args:
            landmarks (list) : Une liste des points du corps, et leurs coordonnees dans l'espace
            exercice : L'exercice à analyser

        Returns:
            bool: La qualité de l'exercice
        """
        epaule_gauche = [landmarks[mp_pose.PoseLandmark.LEFT_SHOULDER.value].x,
                         landmarks[mp_pose.PoseLandmark.LEFT_SHOULDER.value].y]

        coude_gauche = [landmarks[mp_pose.PoseLandmark.LEFT_ELBOW.value].x,
                        landmarks[mp_pose.PoseLandmark.LEFT_ELBOW.value].y]

        poignet_gauche = [landmarks[mp_pose.PoseLandmark.LEFT_WRIST.value].x,
                          landmarks[mp_pose.PoseLandmark.LEFT_WRIST.value].y]

        epaule_droite = [landmarks[mp_pose.PoseLandmark.RIGHT_SHOULDER.value].x,
                         landmarks[mp_pose.PoseLandmark.RIGHT_SHOULDER.value].y]

        coude_droit = [landmarks[mp_pose.PoseLandmark.RIGHT_ELBOW.value].x,
                       landmarks[mp_pose.PoseLandmark.RIGHT_ELBOW.value].y]

        poignet_droit = [landmarks[mp_pose.PoseLandmark.RIGHT_WRIST.value].x,
                         landmarks[mp_pose.PoseLandmark.RIGHT_WRIST.value].y]

        genou_gauche = [landmarks[mp_pose.PoseLandmark.LEFT_KNEE.value].x,
                        landmarks[mp_pose.PoseLandmark.LEFT_KNEE.value].y]

        genou_droit = [landmarks[mp_pose.PoseLandmark.RIGHT_KNEE.value].x,
                       landmarks[mp_pose.PoseLandmark.RIGHT_KNEE.value].y]

        hanche_gauche = [landmarks[mp_pose.PoseLandmark.LEFT_HIP.value].x,
                         landmarks[mp_pose.PoseLandmark.LEFT_HIP.value].y]

        hanche_droite = [landmarks[mp_pose.PoseLandmark.RIGHT_HIP.value].x,
                         landmarks[mp_pose.PoseLandmark.RIGHT_HIP.value].y]
        # pompes, jumping jack, souleve halteres, dips, gainage
        if "pompes" in exercice:
            left_shoulder_elbow_angle, right_shoulder_elbow_angle = self.calculate_angle(epaule_gauche, coude_gauche, poignet_gauche), self.calculate_angle(epaule_droite, coude_droit, poignet_droit)
            is_good_push_up = (left_shoulder_elbow_angle > 200 and left_shoulder_elbow_angle < 290) or (right_shoulder_elbow_angle > 200 and right_shoulder_elbow_angle < 290)

            if exercice == "pompes_bras_ecartes":
                distance_entre_les_mains = self.calculate_distance(poignet_droit, poignet_gauche)
                return is_good_push_up and distance_entre_les_mains > 0.72
            elif exercice == "pompes_diamant":
                distance_entre_les_mains = self.calculate_distance(poignet_droit, poignet_gauche)
                return is_good_push_up and distance_entre_les_mains < 0.2

            elif exercice == "pompes_rotation":
                bras_horizontaux = self.calculate_angle(coude_gauche, epaule_gauche, coude_droit)
                return 180 < bras_horizontaux < 210
            elif exercice == "pompes_spiderman":
                toucher_genou_coude_gauche, toucher_genou_coude_droit = self.calculate_distance(coude_gauche, genou_gauche), self.calculate_distance(coude_droit, genou_droit)
                return is_good_push_up and (toucher_genou_coude_droit < 0.1 or toucher_genou_coude_gauche < 0.1)

            elif exercice == "pompes_triceps_sur_le_ventre":
                triceps_gauche, triceps_droit = self.calculate_angle(epaule_gauche, hanche_gauche, genou_gauche), self.calculate_angle(epaule_droite, hanche_droite, genou_droit)
                return triceps_droit < 160 or triceps_gauche < 160

            elif exercice == "pompes_piquees":
                left_shoulder_elbow_angle, right_shoulder_elbow_angle = self.calculate_angle(epaule_gauche,coude_gauche,poignet_gauche), self.calculate_angle(epaule_droite, coude_droit, poignet_droit)
                angle_hanche_gauche, angle_hanche_droite = self.calculate_angle(epaule_gauche, hanche_gauche, genou_gauche), self.calculate_angle(epaule_droite, hanche_droite, genou_droit)
                return (is_good_push_up and angle_hanche_gauche < 110) or (is_good_push_up and angle_hanche_droite < 110)

            return is_good_push_up

        elif exercice == "epaules_crocodiles":
            return self.calculate_distance(coude_droit, coude_gauche) < 0.1

        elif exercice == "jumping_jack":
            left_shoulder_elbow_angle = self.calculate_angle(epaule_gauche, coude_gauche, poignet_gauche)
            right_shoulder_elbow_angle = self.calculate_angle(epaule_droite, coude_droit, poignet_droit)
            jambe_gauche = self.calculate_angle(genou_gauche, hanche_gauche, genou_droit)

            is_good_jumping_jacks = (left_shoulder_elbow_angle > 235 and jambe_gauche > 40) or (right_shoulder_elbow_angle > 235 and jambe_gauche > 40)

            return is_good_jumping_jacks

        elif exercice == "souleve_halteres":
            left_shoulder_elbow_angle = self.calculate_angle(epaule_gauche, coude_gauche, poignet_gauche)
            right_shoulder_elbow_angle = self.calculate_angle(epaule_droite, coude_droit, poignet_droit)

            is_good_souleve_halteres = (left_shoulder_elbow_angle) < 30 or (right_shoulder_elbow_angle < 30)

            return is_good_souleve_halteres

        elif exercice == "flexion_avant":
            flexion_gauche, flexion_droit = self.calculate_angle(epaule_gauche, hanche_gauche, genou_gauche), self.calculate_angle(epaule_droite, hanche_droite, genou_droit)
            return flexion_droit < 100 or flexion_gauche < 100

        elif exercice == "curl_porte":
            ligne_du_corps_gauche, ligne_du_corps_droit = self.calculate_angle(epaule_gauche, hanche_gauche, genou_gauche), self.calculate_angle(epaule_droite, hanche_droite, genou_droit)

            curl_porte_gauche, curl_porte_droite = self.calculate_angle(epaule_gauche, coude_gauche, poignet_gauche), self.calculate_angle(epaule_droite, coude_droit, poignet_droit)
            return (165 < ligne_du_corps_gauche < 195 or 165 < ligne_du_corps_droit < 195) and (curl_porte_droite < 70 or curl_porte_gauche < 70)

        elif exercice == "curl_de_jambe":
            curl_avec_jambe_gauche, curl_avec_jambe_droite = self.calculate_angle(epaule_gauche, coude_gauche, poignet_gauche), self.calculate_angle(epaule_droite, coude_droit, poignet_droit)
            return curl_avec_jambe_gauche > 195 or curl_avec_jambe_droite < 177

        elif exercice == "coup_de_poing":
            coup_de_poing_gauche, coup_de_poing_droit = self.calculate_angle(epaule_gauche, coude_gauche, poignet_gauche), self.calculate_angle(epaule_droite, coude_droit, poignet_droit)
            return (140 < coup_de_poing_gauche < 160) or (140 < coup_de_poing_droit < 160)

        elif "dips" in exercice:
            angle_dips_gauche = self.calculate_angle(poignet_gauche, coude_gauche, epaule_gauche)
            angle_dips_droit = self.calculate_angle(poignet_droit, coude_droit, epaule_droite)

            if exercice == "dips_chaise":
                is_good_dips_chaise = (angle_dips_gauche > 230) or (angle_dips_droit < 105)

                return is_good_dips_chaise
            elif exercice == "dips_sol":
                is_good_dips_sol = (angle_dips_gauche > 215) or (angle_dips_droit < 150)

                return is_good_dips_sol

        elif exercice == "kickback_triceps":
            kickback_gauche, kickback_droit = self.calculate_angle(poignet_gauche, coude_gauche, epaule_gauche), self.calculate_angle(poignet_gauche, coude_gauche, epaule_gauche)
            return (165 < kickback_droit < 195) or (165 < kickback_gauche < 195)

        elif exercice == "gainage":
            angle_gainage_gauche, angle_gainage_droit = self.calculate_angle(epaule_gauche, coude_gauche, poignet_gauche), self.calculate_angle(epaule_droite, coude_droit, poignet_droit)
            ligne_du_corps_gauche, ligne_du_corps_droit = self.calculate_angle(epaule_gauche, hanche_gauche, genou_gauche), self.calculate_angle(epaule_droite, hanche_droite, genou_droit)

            return (165 < ligne_du_corps_gauche < 205 and (80 < angle_gainage_gauche < 100 or 250 < angle_gainage_gauche < 280)) or (165 < ligne_du_corps_droit < 205 and (80 < angle_gainage_droit < 100 or 250 < angle_gainage_droit < 280))
        elif exercice == 'elevation_laterale_bras':
            bras_gauche, bras_droit = self.calculate_angle(coude_gauche, epaule_gauche, coude_droit), self.calculate_angle(coude_droit, epaule_droite, coude_gauche)
            return bras_gauche > 170 and bras_droit > 170
        elif exercice == "leve_de_bras":
            bras_gauche, bras_droit = self.calculate_angle(coude_gauche, epaule_gauche, hanche_gauche), self.calculate_angle(coude_droit, epaule_droite, hanche_droite)
            return (170 < bras_gauche < 195) and (170 < bras_droit < 195)

    def calculate_angle(self, a, b, c):
        """Cette fonction calcule l'angle entre les segments de droites formés par les points a, b, et c, en utilisant le concept du produit scalaire, vu plus tot cette annee.
        
        Args:
            a (list): Un tableau, comportant les coordonnees x et y du point a
            b (list): Un tableau, comportant les coordonnees x et y du point b
            c (list): Un tableau, comportant les coordonnees x et y du point c

        Returns:
            angle_deg (int): L'angle ABC^ 
        """
        angle_rad = math.atan2(c[1] - b[1], c[0] - b[0]) - math.atan2(a[1] - b[1], a[0] - b[0])
        angle_deg = abs(math.degrees(angle_rad)) # On ne veut qu des valeurs positives
        return angle_deg

    def calculate_distance(self, landmark1, landmark2):
        """Calcule la distance AB, avec A et B deux points du corps. On sait que AB = racine de (xb - xa)² + (yb - ya)², selon la formule de norme des vecteurs, 
        avec le vecteur vec.AB de coordonnees (x2-x1 ; y2-y1)

        Args:
            landmark1 (list): Un tableau, comportant les coordonnees x et y du point a
            landmark2 (list): Un tableau, comportant les coordonnees x et y du point b

        Returns:
            distance (int): La distance AB
        """
        x1 = landmark1[0]
        y1 = landmark1[1]
        x2 = landmark2[0]
        y2 = landmark2[1]

        return math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)

    def show_nerd_stats(self):
        """Inverser la valeur de can_show_nerd_stats pour afficher, selon le choix de l'utilisateur, les points du corps tels que les interpretent l'algorithme de tracking
        """
        if not self.can_show_nerd_stats:
            self.can_show_nerd_stats = True
        else:
            self.can_show_nerd_stats = False

    def close_window(self):
        """Fermez la page tracking, et libérer toutes les ressources engagees dans le calcul de la page
        """
        self.timer.stop()
        self.chronometre.stop()
        self.is_tracking = False
        self.indice_exercice = 0
        try:
            self.cap.release()
            self.video_player.release()
        except:
            pass
        self.compteur_de_video = 0
        cv2.destroyAllWindows()
        self.tracking_a_commence = False
        self.close()

if __name__ == "__main__":
    app = QApplication(sys.argv)
    page_tracking = Exercise_Tracker()
    page_tracking.showMaximized()
    sys.exit(app.exec())