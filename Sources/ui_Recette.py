from petitdej_prise_de_mas import *
from petitdej_perte_de_poid_ import *
from  repasdumidi_pour_la_perte_de_poids import *
from reprasdumidi_prise_de_masse import *
from gouter__pour_perte_de_masse import *
from  gouter_pour_prise_de_masse import *
from repasdusoir_pour_la_prise_de_masse import *   
from repas_du_soir_pour_perte_de_poids import *

from PySide6.QtCore import (QRect, QSize, Qt)
from PySide6.QtGui import (QIcon, QImage, QPixmap, QGuiApplication)
from PySide6.QtWidgets import (QApplication, QLabel, QPushButton, QWidget, QMainWindow)

from random import randint 
from marmiton import *
import webbrowser
import requests
import sys
import os 
import json
import tkinter as tk

def get_screen_dimension():
    # Créer une fenêtre factice pour obtenir les dimensions de l'écran
    root = tk.Tk()

    # Obtenir la largeur et la hauteur de l'écran
    screen_width = root.winfo_screenwidth()     #Largeur
    screen_height = root.winfo_screenheight()   #Hauteur
    if screen_width/screen_height < 1.775:
        result=screen_width/1920
    else : 
        result=screen_height/1080

    # Fermer la fenêtre factice
    root.destroy()
    return result

def adjust_widget_properties(widgets, factor):
    for widget in widgets:
        if hasattr(widget, 'setIconSize'):
            # Si le widget a une méthode setIconSize, multiplier sa taille d'icône
            size = widget.iconSize()
            widget.setIconSize(QSize(size.width() * factor, size.height() * factor))
        if hasattr(widget, 'geometry'):
            # Si le widget a une propriété geometry, ajuster sa position et sa taille
            geometry = widget.geometry()
            widget.setGeometry(QRect(geometry.x() * factor, geometry.y() * factor,
                                    geometry.width() * factor, geometry.height() * factor))
        if hasattr(widget, 'font'):
            # Si le widget a une propriété font, ajuster la taille de la police
            font = widget.font()
            font.setPointSize(int(font.pointSizeF() * factor))
            widget.setFont(font) 

def est_connecte():
    """
    Cette fonction permet de vérifier si l'utilisateur a de la connexion internet, pour pouvoir
    ensuite aller chercher en ligne les images de recettes sans faire planter le programme.
    Nous avons choisi d'utiliser google.com car il n'y a quasiment jamais de panne chez eux. 
    """
    try:
        requests.get("https://marmiton.org/recettes", timeout=5)
        return False
    except requests.exceptions.RequestException:
        return False

def lire_json(nom):
    """Retourne le contenue d'un fichier JSON s'il existe ou crée un fichier JSON avec des valeurs par defaut

    Args:
        nom (str): le nom du fichier que l'on veut lire / créer s'il n'existe pas

    Returns:
        tuple: le tuple contunue dans le fichier
    """
    # Vérifie si le fichier JSON existe
    if os.path.exists(nom):
        # Si le fichier existe, lit les données et les retourne
        with open(nom, 'r') as f:
            return json.load(f)
    else:
        # Si le fichier n'existe pas, crée un nouveau fichier avec des valeurs par défaut
        with open(nom, 'w') as f:
            if nom == "parametres.json":
                valeurs_par_defaut = [1.7, 30, "FEMME", "prise", 1, 70, "prise",0]
            else :
                valeurs_par_defaut = []
            json.dump(valeurs_par_defaut, f)
            return valeurs_par_defaut  # Retourne les valeurs par défaut après les avoir écrites dans le fichier
            
parametres=lire_json("parametres.json")
ingre_ban = lire_json("save.json")

taille = parametres[0]
age = parametres[1]
sexe = parametres[2]
poids = parametres[5]
objectif = parametres[3]
niveau_activite = parametres[4]
ancien_objetif =parametres[6]

def calculer_mb(poids, taille, age, sexe, niveau_activite, objectif):
    """
    Renvoie un coefficient multiplicateur en fonction des informations personnelles. 
    Ce coefficient s'applique dans les recettes pour adapter les quantités de nourritures à l'utilisateur.

    Args:
        poids (int): le poids de l'utilisateur
        taille (float): la taile poids de l'utilisateur
        age (int): l'age poids de l'utilisateur
        sexe (str): le sexe de l'utilisateur
        niveau_activite (int): le niveau d'activité de l'utilisateur
        objectif (str): l'objectif de l'utilisateur

    Returns:
        int: le coefficient multiplicateur 
    """
    mb=0 #mb est le multiplicateur de nouriture
    if sexe == 'HOMME':
        mb=(7.5 * poids) + (1.5 * taille) - (7 * age) + 5 
    elif  sexe == 'FEMME': 
        mb=(7.5 * poids) + (1.5 * taille) - (7 * age) - 121 
    if niveau_activite == 1: 
        mb= mb * 1.2 
    elif niveau_activite == 2: 
        mb=mb * 1.5 
    elif niveau_activite == 3:  
        mb= mb * 1.7 
    if objectif == 'perte':
        mb = mb * 0.8  
    elif  objectif == 'prise' : 
        mb = mb * 1
    mb=mb / 1000  # ajustement pour que la valeur soit dans une échelle gérable pour arrondir 
    if -1000000 <= mb < 0.75:
        multiplicateur_arrondi=0.5 
    elif 0.75 <= mb < 1.25: 
        multiplicateur_arrondi=1
    elif 1.25 <= mb < 1.75:
        multiplicateur_arrondi=1.5
    else : 
        multiplicateur_arrondi=2

    return multiplicateur_arrondi

multiplicateur_arrondi=calculer_mb(poids, taille, age, sexe, niveau_activite,objectif)

def random_repas_reco(ListRepas):
    """Renvoie le nom d'un plat tiré au hasard dans un fichier donné

    Args:
        ListRepas (tuple): le tuple contenant la liste des repas du fichier donné

    Returns:
        str: le nom d'un plat
    """
    chiffre_au_hazard =randint (0,len(ListRepas)-1)
    RepasReco=ListRepas[chiffre_au_hazard].__name__.replace("_"," ")

    return RepasReco

def random_recette(objectif):
    """
    Choisit au hasard 4 recettes en fonction de l'objectif de l'utilisateur

    Args:
        objectif (str): c'est l'objectif de l'utilisateur

    Returns:
        tuple: la liste des 4 plats 
    """
    if objectif=="perte":
        
        petitdej_pour_reco=random_repas_reco(tuplepetitdej_perte_de_poids)
        repas_du_midi_pour_reco=random_repas_reco(tuplerepaspourmidi_perte_de_poids)
        gouter_pour_reco=random_repas_reco(tuplegouter_perte_de_poids)
        repas_du_soir_pour_reco=random_repas_reco(tuple_du_soir_pour_perte_de_poids)

        tuplerepas_reco=[petitdej_pour_reco,repas_du_midi_pour_reco,gouter_pour_reco,repas_du_soir_pour_reco]
    else:
        
        petitdej_pour_reco=random_repas_reco(tuplepetitdej_prise_de_mas)
        repas_du_midi_pour_reco=random_repas_reco(tuplerepaspourmidi_prise_de_mas)
        gouter_pour_reco=random_repas_reco(tuplegouter_prise_de_mas)
        repas_du_soir_pour_reco=random_repas_reco(tuplerepasdusoir_pour_la_prise_de_mas)
        
    tuplerepas_reco=[petitdej_pour_reco,repas_du_midi_pour_reco,gouter_pour_reco,repas_du_soir_pour_reco]
    return tuplerepas_reco

def recette_plat(plat):
    """
    Renvoie les meilleurs résultats de recherche d'un plat comportant l'adresse et l'image
    :param plat: (str) Le nom du plat dont on veut la recette
    :return: (tuple) Un tuple de tuple dont chaque tuple contient l'adresse et l'image du plat.
    Ex : resultat[0][0] renvoie l'url de la recette du plat et resultat[0][1] renvoie l'adresse de l'image
    """
    query_options = {"aqt": plat}
    tuple_contenant_addresses_et_image = ()

    if est_connecte():
        
        query_result = Marmiton.search(query_options)
        url_recette = query_result[0]['url']
        image_recette = query_result[0]['image']
        nom = Marmiton.get(url_recette)
        tuple_contenant_addresses_et_image += ((f"https://marmiton.org{url_recette}", image_recette, nom["name"]),)
    else:
        return
    
    return tuple_contenant_addresses_et_image

def randomRepas(listeRepas, multiplicateur_arrondi, ingredients_ban):
    """
    Choisit au hasard une recette dans un fichier donné en fonction des ingrédients bannis par l'utilisateur

    Args:
        listeRepas (tuple): Liste des recettes contenue dans le fichier sélectionné par l'algo
        multiplicateur_arrondi (flaot): C'est une valeur qui adapte les quantités de chaque plats, Cf calculer_mb pour le détail du calcul
        ingredients_ban (tuple): La liste des ingrédients que l'utilisateur n'aime pas

    Returns:
        tuple: Toutes les informations pour la préparation d'une recette.
    """
    repasContientIngredientBan = True
    repas=listeRepas[0]
    while repasContientIngredientBan :
        rand =randint (0,len(listeRepas)-1) 
        repas=listeRepas[rand](multiplicateur_arrondi)
        repasContientIngredientBan = False
        for un_ingredient_ban_utilisateur in ingredients_ban : 
            if un_ingredient_ban_utilisateur in repas[1]:
                repasContientIngredientBan = True
                continue
    repas_nom=listeRepas[rand].__name__.replace("_"," ")
    return repas_nom ,repas

def algo (ingredients_ban,multiplicateur_arrondi,objectif):
    """L'algo choisit 4 plats pour chaque jour de la semaine parmis toutes les recettes disponiles, en fonction des paramètres saisies par l'utilisateur

    Args:
        ingredients_ban (tuple): La liste des ingrédients que l'utilisateur n'aime pas
        multiplicateur_arrondi (float): C'est une valeur qui adapte les quantités de chaque plats, Cf calculer_mb pour le détail du calcul
        objectif (str): En fonction de l'objectif, la fonction ne choisie pas les repas dans la même liste de recettes

    Returns:
        tuple: Un tuple contenant l'ensemble des informations pour les 4 repas pour chaque jour de la semaine
    """
    repas_du_midi_semaine=[]
    petit_dej_semaine=[]
    repas_du_soir_semaine=[]
    gouter_semaine=[]
    for jour in range (7):
        if objectif=="perte":
            petit_dej_semaine+=[randomRepas(tuplepetitdej_perte_de_poids, multiplicateur_arrondi, ingredients_ban),]
            repas_du_midi_semaine+=[randomRepas(tuplerepaspourmidi_perte_de_poids, multiplicateur_arrondi, ingredients_ban),]
            gouter_semaine+=[randomRepas(tuplegouter_perte_de_poids, multiplicateur_arrondi, ingredients_ban),]
            repas_du_soir_semaine+=[randomRepas(tuple_du_soir_pour_perte_de_poids, multiplicateur_arrondi, ingredients_ban),]
        else :
            petit_dej_semaine+=[randomRepas(tuplepetitdej_prise_de_mas, multiplicateur_arrondi, ingredients_ban),]
            repas_du_midi_semaine+=[randomRepas(tuplerepaspourmidi_prise_de_mas, multiplicateur_arrondi, ingredients_ban),]
            gouter_semaine+=[randomRepas(tuplegouter_prise_de_mas, multiplicateur_arrondi, ingredients_ban),]
            repas_du_soir_semaine+=[randomRepas(tuplerepasdusoir_pour_la_prise_de_mas, multiplicateur_arrondi, ingredients_ban),]


    return petit_dej_semaine,repas_du_midi_semaine,gouter_semaine, repas_du_soir_semaine 

def transformer_tuple(tuple_texte):
    """Transforme une suite de mot dans un tuple en liste (avec des retours à la ligne, comme une liste de course)

    Args:
        tuple_texte (tuple): tuple contenant la liste des ingrédients / quantités d'ingrédients

    Returns:
        (str) : La chaine de caractères, sous forme de liste de course, pour améliorer sa lisibilité
    """
    texte_transforme = ""
    for element in tuple_texte:
        if isinstance(element, int) or isinstance(element, float):
            element = str(element)
            texte_transforme += "- " + element + " "
        else:
            element_transforme = element.replace("é", "e").replace("è", "e").replace("ê", "e").replace("à", "a").replace("â", "a").replace("ô", "o").replace("û", "u").replace("ç", "c").replace("î", "i")
            texte_transforme +=  element_transforme + "\n"

    return texte_transforme

# Générer des recettes aléatoires
quatre_recettes = random_recette(objectif)

# Obtenir les informations sur chaque plat
premier_plat = recette_plat(quatre_recettes[0])
deuxieme_plat = recette_plat(quatre_recettes[1])
troisieme_plat = recette_plat(quatre_recettes[2])
quatrieme_plat = recette_plat(quatre_recettes[3])

# Définir les fonctions d'ouverture de recette
def ouvrir_recette(adresse):
    """Ouvre la page internet de la recette selectionné dans le navigateur par défault 

    Args:
        adresse (str): L'adresse url à ouvrir dans le navigateur
    """
    webbrowser.open(adresse)

factor = get_screen_dimension()

class Recette(QMainWindow):
    def __init__(self, echangeur):
        """
        Initialise l'instance et charge l'interface graphique
        """
        super().__init__()
        self.echangeur = echangeur
        self.initUI()
        
    def initUI(self):
        """
        Crée l'ensemble des widgets / boutons / labels qui composent la page Nutrition avec lesquels l'utilisateur peut interagir
        """
        self.resize(1920, 1080)
        
        #Charger à nouveau certaines variables dans la classe pour les mettres à jours
        parametres=lire_json("parametres.json")
        ingre_ban = lire_json("save.json")
        ancien_objetif =parametres[6]
        objectif = parametres[3]
        
        #Relancer L'API si l'objectif à changé
        if ancien_objetif != objectif :
            with open('parametres.json', 'r') as f:
                valeurs = json.load(f)
            with open("parametres.json", 'w') as f:
                valeurs[6] = objectif
                json.dump(valeurs, f)
            quatre_recettes = random_recette(objectif)

            self.premier_plat = recette_plat(quatre_recettes[0])
            self.deuxieme_plat = recette_plat(quatre_recettes[1])
            self.troisieme_plat = recette_plat(quatre_recettes[2])
            self.quatrieme_plat = recette_plat(quatre_recettes[3])
        else :
            self.premier_plat = premier_plat
            self.deuxieme_plat = deuxieme_plat
            self.troisieme_plat = troisieme_plat
            self.quatrieme_plat = quatrieme_plat
        
        #Créer les Widgets
        self.widget_recette = QWidget(self)
        self.widget_recette.setGeometry(QRect(330, 100, 661, 781))
        self.widget_recette.setStyleSheet("color: rgb(0, 0, 0); border-radius: 30px; width: 200px; height: 100px; background-color: #e7ffef;")
        font = self.widget_recette.font()
        font.setPointSize(30)
        self.widget_recette.setFont(font)
        self.widget_intermediaire_recette = QWidget(self.widget_recette)
        self.widget_intermediaire_recette.setGeometry(QRect(30, 120, 601, 631))
        self.widget_intermediaire_recette.setStyleSheet("border-radius: 25px; background-color: #ffffff;")
        self.widget_programmes = QWidget(self)
        self.widget_programmes.setGeometry(QRect(1090, 100, 661, 781))
        self.widget_programmes.setStyleSheet(u"color: rgb(0, 0, 0); border-radius: 30px; width: 200px; height: 100px; background-color: rgb(255, 248, 212);")
        font = self.widget_programmes.font()
        font.setPointSize(30)
        self.widget_programmes.setFont(font)
        self.widget_intermediaire_programme = QWidget(self.widget_programmes)
        self.widget_intermediaire_programme.setGeometry(QRect(30, 120, 601, 631))
        self.widget_intermediaire_programme.setStyleSheet(u"border-radius: 25px; background-color: #ffffff;")
        
        #Créer les Boutons / Labels
        
        #Bouton pour fermer la page 
        self.button_return = QPushButton("Retour", self)
        self.button_return.setGeometry(30, 20, 150, 40)
        self.button_return.setStyleSheet(u"QPushButton {border-radius: 10px; background-color: rgb(225, 225, 225);} QPushButton:hover {background: rgb(237, 255, 255);}")
        font = self.button_return.font()
        font.setPointSize(20)
        self.button_return.setFont(font)
        self.button_return.clicked.connect(self.allerpagedaccueil)        
        
        #Labels pour afficher l'intitulé des deux parties 
        self.label_titre_programme = QLabel(self.widget_recette)
        self.label_titre_programme.setGeometry(QRect(160, 10, 361, 91))
        self.label_titre_programme.setStyleSheet(u"QLabel { color: rgb(0, 0, 0); font: \"Segoe UI\"; }")
        font = self.label_titre_programme.font()
        font.setPointSize(40)
        font.setBold(True)
        self.label_titre_programme.setFont(font)
        self.label_titre_recette = QLabel(self.widget_programmes)
        self.label_titre_recette.setGeometry(QRect(230, 10, 241, 91))
        self.label_titre_recette.setStyleSheet(u"QLabel { color: rgb(0, 0, 0); font: \"Segoe UI\"; }")
        font = self.label_titre_recette.font()
        font.setPointSize(40)
        font.setBold(True)
        self.label_titre_recette.setFont(font)
        
        #Bouton pour afficher le jour dont les repas sont afficher
        self.pushButton_calendrier = QPushButton(self.widget_intermediaire_recette)
        self.pushButton_calendrier.setGeometry(QRect(130, 20, 350, 51))
        self.pushButton_calendrier.setStyleSheet(u"QPushButton { border-radius: 10px; background-color: rgb(239, 239, 239); } ")
        chemin_image_calendrier_logo = os.path.join(os.getcwd(), "Icone/calendrier_logo.png").replace("\\", "/")
        icon = QIcon()
        icon.addFile(chemin_image_calendrier_logo, QSize(), QIcon.Normal, QIcon.Off)
        self.pushButton_calendrier.setIcon(icon)
        self.pushButton_calendrier.setIconSize(QSize(40, 40))
        font = self.pushButton_calendrier.font()
        font.setPointSize(30)
        self.pushButton_calendrier.setFont(font)
        
        #Bouton pour aller au jour suivant
        self.pushButton_up = QPushButton(self.widget_intermediaire_recette)
        self.pushButton_up.clicked.connect(self.augmenter_valeur_tuple)
        self.pushButton_up.setGeometry(QRect(530, 20, 51, 51))
        self.pushButton_up.setStyleSheet(u"QPushButton { border-radius: 10px; background-color: rgb(239, 239, 239); } QPushButton:hover { background:rgb(215, 251, 255) }")
        self.pushButton_up.setText(">")
        font = self.pushButton_up.font()
        font.setPointSize(35)
        self.pushButton_up.setFont(font)
        
        #Bouton pour aller au jour précédent
        self.pushButton_down = QPushButton(self.widget_intermediaire_recette)
        self.pushButton_down.clicked.connect(self.diminuer_valeur_tuple)
        self.pushButton_down.setGeometry(QRect(20, 20, 51, 51))
        self.pushButton_down.setStyleSheet(u"QPushButton { border-radius: 10px; background-color: rgb(239, 239, 239); } QPushButton:hover { background:rgb(215, 251, 255) }")
        self.pushButton_down.setText("<")
        font = self.pushButton_down.font()
        font.setPointSize(35)
        self.pushButton_down.setFont(font)
        
        #Bouton / Label pour visionner le détails du petit déjeuner
        self.pushButton_petit_dej = QPushButton(self.widget_intermediaire_recette)
        self.pushButton_petit_dej.setGeometry(QRect(20, 90, 561, 121))
        font = self.pushButton_petit_dej.font()
        font.setPointSize(25)
        self.pushButton_petit_dej.setFont(font)
        self.pushButton_petit_dej.setStyleSheet(u"QPushButton { border-radius: 20px; text-align: left; background-color: #fafafa; } QPushButton:hover { background: rgb(237, 255, 255); }")
        chemin_image_dej_logo = os.path.join(os.getcwd(), "Icone/dej_logo.png").replace("\\", "/")
        icon1 = QIcon()
        icon1.addFile(chemin_image_dej_logo, QSize(), QIcon.Normal, QIcon.Off)
        self.pushButton_petit_dej.setIcon(icon1)
        self.pushButton_petit_dej.setIconSize(QSize(90, 90))
        self.label_petit_dej = QLabel(self.widget_intermediaire_recette)
        self.label_petit_dej.setGeometry(QRect(130, 160, 401, 41))
        font = self.label_petit_dej.font()
        font.setPointSize(15)
        self.label_petit_dej.setFont(font)
        self.label_petit_dej.setStyleSheet(u" background-color: transparent;")
        
        #Bouton / Label pour visionner le détails du déjeuner
        self.pushButton_dej = QPushButton(self.widget_intermediaire_recette)
        self.pushButton_dej.setGeometry(QRect(20, 220, 561, 121))
        font = self.pushButton_dej.font()
        font.setPointSize(25)
        self.pushButton_dej.setFont(font)
        self.pushButton_dej.setStyleSheet(u"QPushButton { border-radius: 20px; text-align: left; background-color: #fafafa; } QPushButton:hover { background: rgb(237, 255, 255); }")
        chemin_image_repas_logo = os.path.join(os.getcwd(), "Icone/repas_logo.png").replace("\\", "/")
        icon2 = QIcon()
        icon2.addFile(chemin_image_repas_logo, QSize(), QIcon.Normal, QIcon.Off)
        self.pushButton_dej.setIcon(icon2)
        self.pushButton_dej.setIconSize(QSize(90, 90))
        self.label_dej = QLabel(self.widget_intermediaire_recette)
        self.label_dej.setGeometry(QRect(130, 290, 401, 41))
        font = self.label_dej.font()
        font.setPointSize(15)
        self.label_dej.setFont(font)
        self.label_dej.setStyleSheet(u" background-color: transparent;")
        
        #Bouton / Label pour visionner le détails du gouter
        self.pushButton_gouter = QPushButton(self.widget_intermediaire_recette)
        self.pushButton_gouter.setGeometry(QRect(20, 350, 561, 121))
        font = self.pushButton_gouter.font()
        font.setPointSize(25)
        self.pushButton_gouter.setFont(font)
        self.pushButton_gouter.setStyleSheet(u"QPushButton { border-radius: 20px; text-align: left; background-color: #fafafa;} QPushButton:hover { background: rgb(237, 255, 255); }")
        chemin_image_gouter_logo = os.path.join(os.getcwd(), "Icone/gouter_logo.png").replace("\\", "/")
        icon3 = QIcon()
        icon3.addFile(chemin_image_gouter_logo, QSize(), QIcon.Normal, QIcon.Off)
        self.pushButton_gouter.setIcon(icon3)
        self.pushButton_gouter.setIconSize(QSize(90, 90))
        self.label_gouter = QLabel(self.widget_intermediaire_recette)
        self.label_gouter.setGeometry(QRect(130, 420, 401, 41))
        font = self.label_gouter.font()
        font.setPointSize(15)
        self.label_gouter.setFont(font)
        self.label_gouter.setStyleSheet(u" background-color: transparent;")
        
        #Bouton / Label pour visionner le détails du dinner
        self.pushButton_dinner = QPushButton(self.widget_intermediaire_recette)
        self.pushButton_dinner.setGeometry(QRect(20, 480, 561, 121))
        font = self.pushButton_dinner.font()
        font.setPointSize(25)
        self.pushButton_dinner.setFont(font)
        self.pushButton_dinner.setStyleSheet(u"QPushButton { border-radius: 20px; text-align: left; background-color: #fafafa; } QPushButton:hover { background: rgb(237, 255, 255); }")
        chemin_image_dinner_logo = os.path.join(os.getcwd(), "Icone/diner_logo.png").replace("\\", "/")
        icon4 = QIcon()
        icon4.addFile(chemin_image_dinner_logo, QSize(), QIcon.Normal, QIcon.Off)
        self.pushButton_dinner.setIcon(icon4)
        self.pushButton_dinner.setIconSize(QSize(90, 90))
        self.label_dinner = QLabel(self.widget_intermediaire_recette)
        self.label_dinner.setGeometry(QRect(130, 550, 401, 41))
        font = self.label_dinner.font()
        font.setPointSize(15)
        self.label_dinner.setFont(font)
        self.label_dinner.setStyleSheet(u" background-color: transparent")
        
        #Placer les label au dessus des autres éléments pour qu'on puisse les voir.
        self.label_petit_dej.raise_()
        self.label_dej.raise_()
        self.label_gouter.raise_()
        self.label_dinner.raise_()
        
        #Boutons d'arrière plan. Aucune fonction autre que visuel
        self.pushButton_arriereplan_1 = QPushButton(self.widget_intermediaire_programme)
        self.pushButton_arriereplan_1.setGeometry(QRect(30, 20, 261, 281))
        self.pushButton_arriereplan_1.setStyleSheet(u"border-radius: 20px; background-color: #fafafa;")
        self.pushButton_arriereplan_1.setIconSize(QSize(90, 90))
        self.pushButton_arriereplan_2 = QPushButton(self.widget_intermediaire_programme)
        self.pushButton_arriereplan_2.setGeometry(QRect(310, 20, 261, 281))
        self.pushButton_arriereplan_2.setStyleSheet(u"border-radius: 20px; background-color: #fafafa;")
        self.pushButton_arriereplan_2.setIconSize(QSize(90, 90))
        self.pushButton_arriereplan_3 = QPushButton(self.widget_intermediaire_programme)
        self.pushButton_arriereplan_3.setGeometry(QRect(30, 330, 261, 281))
        self.pushButton_arriereplan_3.setStyleSheet(u"border-radius: 20px; background-color: #fafafa;")
        self.pushButton_arriereplan_3.setIconSize(QSize(90, 90))
        self.pushButton_arriereplan_4 = QPushButton(self.widget_intermediaire_programme)
        self.pushButton_arriereplan_4.setGeometry(QRect(310, 330, 261, 281))
        self.pushButton_arriereplan_4.setStyleSheet(u"border-radius: 20px; background-color: #fafafa;")
        self.pushButton_arriereplan_4.setIconSize(QSize(90, 90))
        
        #Bouton pour stocker l'image Marmiton de la première recette
        self.pushButton_image_1 = QPushButton(self.widget_intermediaire_programme)
        self.pushButton_image_1.setGeometry(QRect(40, 40, 241, 141))
        self.pushButton_image_1.setStyleSheet(u"background-color: rgb(231, 231, 231);")
        self.pushButton_image_1.setIconSize(QSize(241, 141))
        #Bouton pour stocker l'image Marmiton de la deuxième recette
        self.pushButton_image_2 = QPushButton(self.widget_intermediaire_programme)
        self.pushButton_image_2.setGeometry(QRect(320, 40, 241, 141))
        self.pushButton_image_2.setStyleSheet(u"background-color: rgb(231, 231, 231);")
        self.pushButton_image_2.setIconSize(QSize(241, 141))
        #Bouton pour stocker l'image Marmiton de la troisième recette
        self.pushButton_image_3 = QPushButton(self.widget_intermediaire_programme)
        self.pushButton_image_3.setGeometry(QRect(40, 350, 241, 141))
        self.pushButton_image_3.setStyleSheet(u"background-color: rgb(231, 231, 231);")
        self.pushButton_image_3.setIconSize(QSize(241, 141))
        #Bouton pour stocker l'image Marmiton de la quatrième recette
        self.pushButton_image_4 = QPushButton(self.widget_intermediaire_programme)
        self.pushButton_image_4.setGeometry(QRect(320, 350, 241, 141))
        self.pushButton_image_4.setStyleSheet(u"background-color: rgb(231, 231, 231);")
        self.pushButton_image_4.setIconSize(QSize(241, 141))
        
        if est_connecte():
            response=requests.get(self.premier_plat[0][1])
            image_data=response.content
            self.pushButton_image_1.setIcon(QIcon(QPixmap.fromImage(QImage.fromData(image_data))))

            response2=requests.get(self.deuxieme_plat[0][1])
            image_data_2=response2.content
            self.pushButton_image_2.setIcon(QIcon(QPixmap.fromImage(QImage.fromData(image_data_2))))

            response3=requests.get(self.troisieme_plat[0][1])
            image_data_3=response3.content
            self.pushButton_image_3.setIcon(QIcon(QPixmap.fromImage(QImage.fromData(image_data_3))))

            response4=requests.get(self.quatrieme_plat[0][1])
            image_data_4=response4.content
            self.pushButton_image_4.setIcon(QIcon(QPixmap.fromImage(QImage.fromData(image_data_4))))
        else:
            chemin_image_mauvaise_connexion = os.path.join(os.getcwd(), "Icone/bad_co_logo.png").replace("\\", "/")
            self.pushButton_image_1.setIcon(QIcon(QPixmap(chemin_image_mauvaise_connexion)))
            self.pushButton_image_2.setIcon(QIcon(QPixmap(chemin_image_mauvaise_connexion)))
            self.pushButton_image_3.setIcon(QIcon(QPixmap(chemin_image_mauvaise_connexion)))
            self.pushButton_image_4.setIcon(QIcon(QPixmap(chemin_image_mauvaise_connexion)))
        
        #Label pour stocker le nom de la première recette Marmiton
        self.label_nom_recette_1 = QLabel(self.widget_intermediaire_programme)
        self.label_nom_recette_1.setGeometry(QRect(40, 190, 241, 105))
        self.label_nom_recette_1.setStyleSheet(u"font-weight: bold; background-color: transparent;")
        font = self.label_nom_recette_1.font()
        font.setPointSize(15)
        self.label_nom_recette_1.setFont(font)
        self.label_nom_recette_1.setWordWrap(True)
        #Label pour stocker le nom de la deuxième recette Marmiton
        self.label_nom_recette_2 = QLabel(self.widget_intermediaire_programme)
        self.label_nom_recette_2.setGeometry(QRect(320, 190, 241, 105))
        font = self.label_nom_recette_2.font()
        font.setPointSize(15)
        self.label_nom_recette_2.setFont(font)
        self.label_nom_recette_2.setStyleSheet(u"font-weight: bold; background-color: transparent;")
        self.label_nom_recette_2.setWordWrap(True)
        #Label pour stocker le nom de la troisième recette Marmiton
        self.label_nom_recette_3 = QLabel(self.widget_intermediaire_programme)
        self.label_nom_recette_3.setGeometry(QRect(40, 500, 241, 105))
        font = self.label_nom_recette_3.font()
        font.setPointSize(15)
        self.label_nom_recette_3.setFont(font)
        self.label_nom_recette_3.setStyleSheet(u"font-weight: bold; background-color: transparent;")
        self.label_nom_recette_3.setWordWrap(True)
        #Label pour stocker le nom de la quatrième recette Marmiton
        self.label_nom_recette_4 = QLabel(self.widget_intermediaire_programme)
        self.label_nom_recette_4.setGeometry(QRect(320, 500, 241, 105))
        font = self.label_nom_recette_4.font()
        font.setPointSize(15)
        self.label_nom_recette_4.setFont(font)
        self.label_nom_recette_4.setStyleSheet(u"font-weight: bold; background-color: transparent;")
        self.label_nom_recette_4.setWordWrap(True)
        
        #Boutons cliquables. Pour que la zone cliquable recouvre l'image + le nom de la recette on place un autre bouton par-dessus
        self.pushButton_zone_cliquable_1 = QPushButton(self.widget_intermediaire_programme)
        self.pushButton_zone_cliquable_1.setGeometry(QRect(30, 20, 261, 281))
        self.pushButton_zone_cliquable_1.setStyleSheet(u"QPushButton {border-radius: 20px; background-color: transparent;} QPushButton:hover { background-color: rgba(237, 255, 255, 0.4); }")
        self.pushButton_zone_cliquable_1.setIconSize(QSize(90, 90))
        self.pushButton_zone_cliquable_2 = QPushButton(self.widget_intermediaire_programme)
        self.pushButton_zone_cliquable_2.setGeometry(QRect(310, 40, 261, 261))
        self.pushButton_zone_cliquable_2.setStyleSheet(u"QPushButton {border-radius: 20px; background-color: transparent;} QPushButton:hover { background-color: rgba(237, 255, 255, 0.4); }")
        self.pushButton_zone_cliquable_2.setIconSize(QSize(90, 90))
        self.pushButton_zone_cliquable_3 = QPushButton(self.widget_intermediaire_programme)
        self.pushButton_zone_cliquable_3.setGeometry(QRect(30, 330, 261, 281))
        self.pushButton_zone_cliquable_3.setStyleSheet(u"QPushButton {border-radius: 20px; background-color: transparent;} QPushButton:hover { background-color: rgba(237, 255, 255, 0.4); }")
        self.pushButton_zone_cliquable_3.setIconSize(QSize(90, 90))
        self.pushButton_zone_cliquable_4 = QPushButton(self.widget_intermediaire_programme)
        self.pushButton_zone_cliquable_4.setGeometry(QRect(310, 330, 261, 281))
        self.pushButton_zone_cliquable_4.setStyleSheet(u"QPushButton {border-radius: 20px; background-color: transparent;} QPushButton:hover { background-color: rgba(237, 255, 255, 0.4); }")
        self.pushButton_zone_cliquable_4.setIconSize(QSize(90, 90))
        
        #Connecter les boutons (de la partie recette) à l'url de leur recette
        if est_connecte():
            self.pushButton_zone_cliquable_1.clicked.connect(lambda: ouvrir_recette(self.premier_plat[0][0]))
            self.pushButton_zone_cliquable_2.clicked.connect(lambda: ouvrir_recette(self.deuxieme_plat[0][0]))
            self.pushButton_zone_cliquable_3.clicked.connect(lambda: ouvrir_recette(self.troisieme_plat[0][0]))
            self.pushButton_zone_cliquable_4.clicked.connect(lambda: ouvrir_recette(self.quatrieme_plat[0][0]))
        
        #Connecter les boutons (de la partie programmes) à leur recettes détaillées
        self.pushButton_petit_dej.clicked.connect(lambda: self.create_new_widget(0))
        self.pushButton_dej.clicked.connect(lambda: self.create_new_widget(1))
        self.pushButton_gouter.clicked.connect(lambda: self.create_new_widget(2))
        self.pushButton_dinner.clicked.connect(lambda: self.create_new_widget(3))
        
        #Création de quelques variables nécessaires pour assigner le bon texte au Label
        self.recettes_choisis = algo(ingre_ban,multiplicateur_arrondi, objectif)
        self.a=0
        self.b=0
        self.jours = [" lundi"," mardi"," mercredi"," jeudi"," vendredi"," samedi"," dimanche",]
        
        #Assigner les textes à chaque élément
        self.label_titre_programme.setText("PROGRAMME")
        self.pushButton_calendrier.setText(self.jours[0])
        self.pushButton_petit_dej.setText(" Petit d\u00e9jeuner ")
        self.pushButton_dej.setText(" D\u00e9jeuner")
        self.pushButton_gouter.setText(" Gouter")
        self.pushButton_dinner.setText(" Dîner")
        
        self.label_petit_dej.setText(self.recettes_choisis[0][0][0])
        self.label_dej.setText(self.recettes_choisis[1][0][0])
        self.label_gouter.setText(self.recettes_choisis[2][0][0])
        self.label_dinner.setText(self.recettes_choisis[3][0][0])

        self.label_titre_recette.setText("RECETTES")
        self.label_nom_recette_1.setAlignment(Qt.AlignCenter)
        self.label_nom_recette_2.setAlignment(Qt.AlignCenter)
        self.label_nom_recette_3.setAlignment(Qt.AlignCenter)
        self.label_nom_recette_4.setAlignment(Qt.AlignCenter)
        if est_connecte():
            self.label_nom_recette_1.setText(self.premier_plat[0][2])
            self.label_nom_recette_2.setText(self.deuxieme_plat[0][2])
            self.label_nom_recette_3.setText(self.troisieme_plat[0][2])
            self.label_nom_recette_4.setText(self.quatrieme_plat[0][2])
        else:
            self.label_nom_recette_1.setText("Pas De Connexion")
            self.label_nom_recette_2.setText("Pas De Connexion")
            self.label_nom_recette_3.setText("Pas De Connexion")
            self.label_nom_recette_4.setText("Pas De Connexion")
        
        # Liste des widgets à traiter
        widgets_to_adjust = [
            self.pushButton_calendrier,
            self.pushButton_petit_dej,
            self.pushButton_dej,
            self.pushButton_gouter,
            self.pushButton_dinner,
            self.pushButton_arriereplan_1,
            self.pushButton_arriereplan_2,
            self.pushButton_arriereplan_3,
            self.pushButton_arriereplan_4,
            self.pushButton_image_1,
            self.pushButton_image_2,
            self.pushButton_image_3,
            self.pushButton_image_4,
            self.pushButton_zone_cliquable_1,
            self.pushButton_zone_cliquable_2,
            self.pushButton_zone_cliquable_3,
            self.pushButton_zone_cliquable_4,
            
            self.widget_recette,
            self.widget_intermediaire_recette,
            self.widget_programmes,
            self.widget_intermediaire_programme,
            self.button_return,
            self.label_titre_programme,
            self.label_titre_recette,
            self.pushButton_up,
            self.pushButton_down,
            self.label_petit_dej,
            self.label_dej,
            self.label_gouter,
            self.label_dinner,
            self.label_nom_recette_1,
            self.label_nom_recette_2,
            self.label_nom_recette_3,
            self.label_nom_recette_4, 
        ]
        # Appel de la fonction pour ajuster les propriétés des widgets
        adjust_widget_properties(widgets_to_adjust, factor)

    def augmenter_valeur_tuple(self):
        """
        Actualise le texte des Labels / boutons pour le jour suivant
        """
        if self.a >=0 and self.b >=0 and self.b <6: 
            self.a+=1
            self.b+=1
        self.label_petit_dej.setText(self.recettes_choisis[0][self.a][0])
        self.label_dej.setText(self.recettes_choisis[1][self.a][0])
        self.label_dinner.setText(self.recettes_choisis[3][self.a][0])
        self.label_gouter.setText(self.recettes_choisis[2][self.a][0])
        self.pushButton_calendrier.setText(self.jours[self.b])
    
    def diminuer_valeur_tuple(self):
        """
        Actualise le texte des Labels / boutons pour le jour précedent
        """
        if self.a >0 and self.b >0 : 
            self.a-=1
            self.b-=1
        self.label_petit_dej.setText(self.recettes_choisis[0][self.a][0])
        self.label_dej.setText(self.recettes_choisis[1][self.a][0])
        self.label_dinner.setText(self.recettes_choisis[3][self.a][0])
        self.label_gouter.setText(self.recettes_choisis[2][self.a][0])
        self.pushButton_calendrier.setText(self.jours[self.b])
       
    def create_new_widget(self,indice):
        """
        Crée un pop-up pour afficher le détail de la recette
        
        Args:
            indice (int): Valeur alant de 1 à 4 correspond dans le même ordre à : petit dèj, dèj, gouter et dinner
        """
        # Créez le nouveau widget sans parent
        self.new_widget = QWidget()
        self.new_widget.setGeometry(950, 30, 970, 1010)
        
        # Créez un widget intermédiaire pour contenir les élements
        self.widget_container = QWidget(self.new_widget)
        self.widget_container.setGeometry(30, 65, 930, 940)
        self.widget_container.setStyleSheet(u"background-color: rgb(239, 239, 239);")
        
        #Créer les boutons
        self.button_return = QPushButton("Retour", self.new_widget)
        self.button_return.setGeometry(30, 15, 150, 40)
        self.button_return.setStyleSheet(u"QPushButton {border-radius: 10px; background-color: rgb(225, 225, 225);} QPushButton:hover {background: rgb(237, 255, 255);}")
        self.button_return.clicked.connect(self.close_new_widget)
        font = self.button_return.font()
        font.setPointSize(15)
        self.button_return.setFont(font)
    
        # Créer Labels :
        self.label = QLabel(self.widget_container)
        self.label.setGeometry(QRect(0, 0, 460, 320))
        self.label.setText(transformer_tuple(self.recettes_choisis[indice][self.a][1][0]))
        self.label.setAlignment(Qt.AlignTop)
        font = self.label.font()
        font.setPointSize(12)
        self.label.setFont(font)
        self.label_2 = QLabel(self.widget_container)
        self.label_2.setGeometry(QRect(470, 0, 460, 320))
        self.label_2.setText(transformer_tuple(self.recettes_choisis[indice][self.a][1][1])) 
        self.label_2.setAlignment(Qt.AlignTop)
        font = self.label_2.font()
        font.setPointSize(12)
        self.label_2.setFont(font)
        self.label_3 = QLabel(self.widget_container)
        self.label_3.setGeometry(QRect(0, 330, 930, 620))
        self.label_3.setText(self.recettes_choisis[indice][self.a][1][2][0])
        self.label_3.setAlignment(Qt.AlignTop)
        font = self.label_3.font()
        font.setPointSize(12)
        self.label_3.setFont(font)
        self.label_2.setStyleSheet(u"background-color: rgb(239, 239, 239);")
        self.label_3.setStyleSheet(u"background-color: rgb(239, 239, 239);")
        self.label.setStyleSheet(u"background-color: rgb(239, 239, 239);")
        self.label.setWordWrap(True)
        self.label_2.setWordWrap(True)
        self.label_3.setWordWrap(True)

        #Connecter les boutons
        self.button_return.clicked.connect(self.close_new_widget)
        
        # Liste des widgets à traiter
        widgets_to_adjust = [
            self.new_widget,
            self.label,
            self.label_2,
            self.label_3,
            self.button_return,
            self.widget_container
        ]
        
        # Appel de la fonction pour ajuster les propriétés des widgets
        adjust_widget_properties(widgets_to_adjust, factor)

        # Affichez le nouveau widget
        self.new_widget.show()

    def close_new_widget(self):
        """
        Ferme le pop-up du détail de la recette
        """     
        # Fermez le nouveau widget
        self.new_widget.close()
      
    def allerpagedaccueil(self):
        """
        Change la valeur de l'indice pour afficher la page d'accueil
        (à chaque page est attribué un numéro,  pour faciliter le déplacement entre celles-ci)
        """
        self.echangeur.setCurrentIndex(0)
        #Si un widget pop-up est encore ouvert quand vous quittez la fenetre nutrition, le widget est fermé.
        self.new_widget.close()
       
if __name__ == "__main__":
    app = QApplication(sys.argv)
    page_recette = Recette()
    page_recette.show()
    sys.exit(app.exec())
