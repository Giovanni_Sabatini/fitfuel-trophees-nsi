from PySide6.QtCore import (QRect, QSize)
from PySide6.QtGui import (QIcon, Qt, QImage, QPixmap)
from PySide6.QtWidgets import (QApplication, QLabel, QPushButton, QWidget, QMainWindow, QStackedWidget, QComboBox, QLineEdit, QListWidget)

from ui_Recette import Recette, lire_json, adjust_widget_properties, factor
from ui_Sport_page import Sport

import sys
import json
import os

class Ui_MainWindow(QMainWindow):
        
    def __init__(self):
        """
        Initialise l'instance UIMainWindow et charge l'interface utilisateur.
        """
        self.nombre_page_recette = 1
        super().__init__()
        self.initUI()
    
    def initUI(self):
        """
        Crée l'ensemble des widgets / boutons / labels qui composent la page d'accueil avec lesquels l'utilisateur pourra interagir
        """
        #Initialiser le widget qui va contenir toute la page d'accueil
        self.setEnabled(True)
        self.resize(1920, 1080)
        self.setStyleSheet(u"background-color: rgb(204, 204, 204);")
        self.centralwidget = QWidget(self)
        self.setCentralWidget(self.centralwidget)
        
        #INITIALISER LES BOUTONS INFORMATIONS / PARAMETRES
        self.pushButton_arrierplan_gauche = QPushButton(self.centralwidget)
        self.pushButton_arrierplan_gauche.setMouseTracking(False)
        self.pushButton_arrierplan_gauche.setStyleSheet(u"color: rgb(0, 0, 0);border-radius: 10px; background-color: rgb(240, 240, 240);")
                
        self.pushButton_info = QPushButton(self.pushButton_arrierplan_gauche)
        self.pushButton_info.setMouseTracking(True)
        self.pushButton_info.setStyleSheet(u"QPushButton {color: rgb(0, 0, 0); border-radius: 20px; background-color: #ffffff;} QPushButton:hover {background: rgb(237, 255, 255);}")
        chemin_image_info_logo = os.path.join(os.getcwd(), "Icone/info_logo.png").replace("\\", "/")
        icon3 = QIcon()
        icon3.addFile(chemin_image_info_logo, QSize(), QIcon.Normal, QIcon.Off)
        self.pushButton_info.setIcon(icon3)
                
        self.pushButton_setings = QPushButton(self.pushButton_arrierplan_gauche)
        self.pushButton_setings.setMouseTracking(True)
        self.pushButton_setings.setStyleSheet(u"QPushButton {color: rgb(0, 0, 0); border-radius: 20px; background-color: #ffffff;} QPushButton:hover {background: rgb(237, 255, 255);}")
        chemin_image_setings_logo = os.path.join(os.getcwd(), "Icone/settings_logo.png").replace("\\", "/")
        icon4 = QIcon()
        icon4.addFile(chemin_image_setings_logo, QSize(), QIcon.Normal, QIcon.Off)
        self.pushButton_setings.setIcon(icon4)
        
        #INITIALISER LE CENTRE DE LA PAGE
        #Créer le widget parent
        self.widget_parent = QWidget(self.centralwidget)
        self.widget_parent.setStyleSheet("background-color: rgba(0, 0, 0, 0);")
        
        self.label_back = QLabel(self.centralwidget)
        self.label_back.setGeometry(QRect(0, 0, 2500, 2500))
        chemin_image_dos = os.path.join(os.getcwd(), "Icone/backgournd.png").replace("\\", "/")
        self.label_back.setPixmap(QPixmap(chemin_image_dos))
        #Créer les boutons
        self.pushButton_nutrition_bis = QPushButton(self.widget_parent)
                    
        self.pushButton_nutrition_bis.setMouseTracking(True)
        self.pushButton_nutrition_bis.setStyleSheet(u"QPushButton {color: rgb(0, 0, 0); border-radius: 20px; background-color: #ffffff;} QPushButton:hover {background: rgb(237, 255, 255);}")
        self.pushButton_nutrition_bis.setText("NUTRITION")
        self.pushButton_sport_bis = QPushButton(self.widget_parent)
        font = self.pushButton_nutrition_bis.font()
        font.setPointSize(30)
        self.pushButton_nutrition_bis.setFont(font)
        
        self.pushButton_sport_bis.setMouseTracking(True)
        self.pushButton_sport_bis.setStyleSheet(u"QPushButton {color: rgb(0, 0, 0); border-radius: 20px; background-color: #ffffff;} QPushButton:hover {background: rgb(237, 255, 255);}")
        self.pushButton_sport_bis.setText("SPORT")
        font = self.pushButton_sport_bis.font()
        font.setPointSize(30)
        self.pushButton_sport_bis.setFont(font)
        
        self.label_nom_app = QLabel(self.widget_parent)
        self.label_nom_app.setText("BIENVENUE SUR FITFUEL")
        self.label_nom_app.setStyleSheet("background-color: rgba(0, 0, 0, 0);")
        font = self.label_nom_app.font()
        font.setPointSize(50)
        font.setFamily('Segoe Print')
        font.setBold(True)
        self.label_nom_app.setFont(font)

        #Connecter les boutons
        self.pushButton_nutrition_bis.clicked.connect(self.allerpagedapres)
        self.pushButton_sport_bis.clicked.connect(self.allerpagesport)
        self.pushButton_setings.clicked.connect(self.create_new_widget)
        self.pushButton_info.clicked.connect(self.create_infos)

        #Créer le tuple pour permettre des résultats par défault avec des valeurs arbitraires
        self.element_pour_multiplicateur_nouriture = ['160', '30', 'FEMME', 'PERTE DE MASSE', "NIVEAU 1 (pratique du sport moins d'une fois par semaine)", '80']

        #Mettre chaque Item à la bonne taille
        self.pushButton_arrierplan_gauche.setIconSize(QSize(50, 50))
        self.pushButton_info.setIconSize(QSize(50, 50))
        self.pushButton_setings.setIconSize(QSize(70, 70))

        self.widget_parent.setGeometry(QRect(150, 0, 3000, 3000))
        self.pushButton_setings.setGeometry(QRect(10, 90, 71, 71))
        self.pushButton_info.setGeometry(QRect(10, 10, 71, 71))
        self.pushButton_arrierplan_gauche.setGeometry(QRect(10, 10, 90, 170))
        self.pushButton_nutrition_bis.setGeometry(QRect(530, 380, 300, 110))
        self.pushButton_sport_bis.setGeometry(QRect(930, 380, 300, 110))
        self.label_nom_app.setGeometry(QRect(430, 70, 931, 131))
        
        self.pushButton_arrierplan_gauche.raise_()
        self.pushButton_info.raise_()
        self.pushButton_setings.raise_()
        self.pushButton_sport_bis.raise_()
        self.pushButton_nutrition_bis.raise_()
        self.label_nom_app.raise_()
        self.widget_parent.raise_()
        
        # Liste des widgets à traiter
        widgets_to_adjust = [
            self.pushButton_arrierplan_gauche,
            self.pushButton_info,
            self.pushButton_setings,
            self.pushButton_nutrition_bis,
            self.pushButton_sport_bis,
            self.label_nom_app,
            self.label_back
        ]
        # Appel de la fonction pour ajuster les propriétés des widgets
        adjust_widget_properties(widgets_to_adjust, factor)     
        
    def create_infos_parmètre(self):
        """
        Crée une fenêtre pop-up pour informer l'utilisateur de rentrer les paramètres
        """
        self.new_widget_cam = QWidget()
        self.new_widget_cam.setGeometry(800, 400, 500, 250)
        
        #Créer les boutons
        self.button_return = QPushButton("OK", self.new_widget_cam)
        self.button_return.setGeometry(200, 190, 100, 50)
        self.button_return.setStyleSheet(u"QPushButton {border-radius: 10px; background-color: rgb(225, 225, 225);} QPushButton:hover {background: rgb(237, 255, 255);}")
        font = self.button_return.font()
        font.setPointSize(15)
        self.button_return.setFont(font)
        #Créer les Labels
        self.label_cam = QLabel(self.new_widget_cam)
        self.label_cam.setObjectName(u"label")
        self.label_cam.setGeometry(QRect(20, 0, 440, 200))
        self.label_cam.setWordWrap(True)
        font = self.label_cam.font()
        font.setPointSize(19)
        self.label_cam.setFont(font)
        self.label_cam.setAlignment(Qt.AlignCenter)
        
        #Assigner les textes à chaque élément
        self.label_cam.setText("VEUILLEZ SAISIR VOS PARAMETRES")
        self.button_return.clicked.connect(self.close_infos_cam_widget)
        
        # Liste des widgets à traiter
        widgets_to_adjust = [
            self.new_widget_cam,
            self.button_return,
            self.label_cam,
            ]
        
        # Appel de la fonction pour ajuster les propriétés des widgets
        adjust_widget_properties(widgets_to_adjust, factor)
                
        #Afficher le Widget 
        self.new_widget_cam.show()
       
    def create_new_widget(self):
        """
        Crée une fenêtre pop-up pour afficher les paramètres que l'utilisateur peut modifier.
        """
        # PARTIE 1 : INFOS PERSONNELLES
        # Créez le nouveau widget sans parent pour qu'il apparaisse en pop-up
        self.new_widget = QWidget()
        self.new_widget.setGeometry(110, 30, 900, 1010)
        
        # Créez un widget intermédiaire pour contenir les élements
        self.widget_container = QWidget(self.new_widget)
        self.widget_container.setGeometry(30, 80, 840, 900)
        self.widget_container.setStyleSheet(u"background-color: rgb(239, 239, 239);")
        
        #Créer les boutons
        self.button_return = QPushButton("Retour", self.new_widget)
        self.button_return.setGeometry(30, 20, 150, 40)
        self.button_return.setStyleSheet(u"QPushButton {border-radius: 10px; background-color: rgb(225, 225, 225);} QPushButton:hover {background: rgb(237, 255, 255);}")
        font = self.button_return.font()
        font.setPointSize(20)
        self.button_return.setFont(font)
        
        #Créer les Labels
        self.label = QLabel(self.widget_container)
        self.label.setGeometry(QRect(10, 10, 461, 61))
        font = self.label.font()
        font.setPointSize(24)
        self.label.setFont(font)
        self.label_2 = QLabel(self.widget_container)
        self.label_2.setGeometry(QRect(10, 100, 121, 21))
        font = self.label_2.font()
        font.setPointSize(15)
        self.label_2.setFont(font)
        self.label_3 = QLabel(self.widget_container)
        self.label_3.setGeometry(QRect(240, 100, 71, 21))
        font = self.label_3.font()
        font.setPointSize(15)
        self.label_3.setFont(font)
        self.label_4 = QLabel(self.widget_container)
        self.label_4.setGeometry(QRect(470, 100, 71, 21))
        font = self.label_4.font()
        font.setPointSize(15)
        self.label_4.setFont(font)
        self.label_5 = QLabel(self.widget_container)
        self.label_5.setGeometry(QRect(10, 210, 111, 21))
        font = self.label_5.font()
        font.setPointSize(15)
        self.label_5.setFont(font)
        self.label_6 = QLabel(self.widget_container)
        self.label_6.setGeometry(QRect(420, 210, 321, 21))
        font = self.label_6.font()
        font.setPointSize(15)
        self.label_6.setFont(font)       
        self.label_7 = QLabel(self.widget_container)
        self.label_7.setGeometry(QRect(700, 100, 110, 30))
        font = self.label_7.font()
        font.setPointSize(15)
        self.label_7.setFont(font)
        
        #Créer les ComboBox
        self.comboBox = QComboBox(self.widget_container)        
        self.comboBox.setGeometry(QRect(10, 130, 69, 28))
        self.comboBox_2 = QComboBox(self.widget_container)        
        self.comboBox_2.setGeometry(QRect(240, 130, 69, 28))
        self.comboBox_3 = QComboBox(self.widget_container)
        self.comboBox_3.addItem("")
        self.comboBox_3.addItem("")
        self.comboBox_3.setGeometry(QRect(470, 130, 69, 28))
        self.comboBox_4 = QComboBox(self.widget_container)
        self.comboBox_4.addItem("")
        self.comboBox_4.addItem("")
        self.comboBox_4.setGeometry(QRect(10, 240, 136, 28))
        self.comboBox_5 = QComboBox(self.widget_container)
        self.comboBox_5.addItem("")
        self.comboBox_5.addItem("")
        self.comboBox_5.addItem("")
        self.comboBox_5.setGeometry(QRect(420, 240, 351, 28))
        self.comboBox_6 = QComboBox(self.widget_container)
        self.comboBox_6.setGeometry(QRect(700, 130, 69, 28))

        # Ajouter les éléments au QComboBox
        for i in range(10, 100):
                text = str(i)
                self.comboBox_2.addItem(text)
        
        for i in range(100, 221):
                text = str(i)
                self.comboBox.addItem(text)
        
        for i in range(40, 205, 5):
                text = str(i)
                self.comboBox_6.addItem(text)
        
        #Assigner les textes à chaque élément
        self.label.setText("INFORMATIONS PERSONNELLES")
        self.label_2.setText("TAILLE (cm) :")
        self.label_3.setText("AGE :")
        self.label_4.setText("SEXE :")
        self.label_5.setText("OBJECTIF :")
        self.label_6.setText("NIVEAU D'ACTIVITE PHYSIQUE :")
        self.label_7.setText("POIDS (kg) :")

        self.comboBox_3.setItemText(0,"FEMME")
        self.comboBox_3.setItemText(1,"HOMME")
        self.comboBox_4.setItemText(0,"PERTE DE MASSE")
        self.comboBox_4.setItemText(1,"PRISE DE MASSE")
        self.comboBox_5.setItemText(0,"NIVEAU 1 (pratique du sport moins d'une fois par semaine)")
        self.comboBox_5.setItemText(1,"NIVEAU 2 (pratique du port une a deux par semaine)")
        self.comboBox_5.setItemText(2,"NIVEAU 3 (pratique du sport de deux fois par semaine)")

        #PARTIE 2 : INGREDIENTS A BANNIR
        self.elements = self.lire_save_json()
        
        # Titre de la partie
        self.label_t2 = QLabel(self.widget_container)
        self.label_t2.setGeometry(QRect(10, 300, 441, 61))
        font = self.label_t2.font()
        font.setPointSize(22)
        self.label_t2.setFont(font)
        self.label_t2.setText("INGREDIENTS INDESIRABLES")
                
        # Zone de saisie
        self.input_field = QLineEdit(self.widget_container)
        self.input_field.setGeometry(QRect(210, 390, 200, 25))

        # Bouton d'ajout
        self.add_button = QPushButton(self.widget_container)
        self.add_button.clicked.connect(self.add_item)
        self.add_button.setGeometry(QRect(440, 390, 70, 25))
        self.add_button.setText("Ajouter")

        # Liste des éléments
        self.list_widget = QListWidget(self.widget_container)
        self.list_widget.setGeometry(QRect(210, 425, 200, 150))
        
        for element in self.elements:
            self.list_widget.addItem(element)

        # Bouton de suppression
        self.delete_button = QPushButton(self.widget_container)
        self.delete_button.clicked.connect(self.delete_item)
        self.delete_button.setGeometry(QRect(440, 425, 70, 25))
        self.delete_button.setText("Supprimer")
        
        # Bouton de sauvegarde 
        self.button_save = QPushButton("Sauvegarder", self.widget_container)
        self.button_save.setGeometry(QRect(600, 800, 150, 40))
        self.button_save.setStyleSheet(u"QPushButton {border-radius: 10px; background-color: rgb(225, 225, 225);} QPushButton:hover {background: rgb(237, 255, 255);}")
        font = self.button_save.font()
        font.setPointSize(20)
        self.button_save.setFont(font)
        # Affichez le nouveau widget
        
        # PARTIE 3 : LA CAMERA
        #Créer les Labels
        self.label_cam_titre = QLabel(self.widget_container)
        self.label_cam_titre.setGeometry(QRect(10, 610, 441, 61))
        font = self.label_cam_titre.font()
        font.setPointSize(22)
        self.label_cam_titre.setFont(font)
        self.label_cam = QLabel(self.widget_container)
        self.label_cam.setGeometry(QRect(10, 680, 400, 21))
        font = self.label_cam.font()
        font.setPointSize(15)
        self.label_cam.setFont(font)
        
        #Créer un bouton information pour expliquer comment fonctionne ce paramètre.
        # Bouton de sauvegarde 
        self.button_info_cam = QPushButton("?", self.widget_container)
        self.button_info_cam.setGeometry(QRect(180, 710, 22, 28))
        font = self.button_info_cam.font()
        font.setPointSize(12)
        self.button_info_cam.setFont(font)
        
        #Assigner les textes à chaque élément
        self.label_cam_titre.setText("CAMERA / WEB-CAM")
        self.label_cam.setText("NUMERO DE LA CAMERA A UTILISER :")
        
        #Créer la ComboBox 
        self.comboBox_cam = QComboBox(self.widget_container)        
        self.comboBox_cam.setGeometry(QRect(10, 710, 150, 28))
        
        for i in range(0,6):
                    text = str(i)
                    self.comboBox_cam.addItem(text)
           
        #Connecter les boutons
        self.button_return.clicked.connect(self.close_new_widget)
        self.button_save.clicked.connect(self.associer_variable_algo)
        self.button_info_cam.clicked.connect(self.create_infos_cam)
        
        # Liste des widgets à traiter
        widgets_to_adjust = [
            self.new_widget,            
            self.widget_container,
            self.button_return,
            self.label,
            self.label_2,
            self.label_3,
            self.label_4,
            self.label_5,
            self.label_6,
            self.label_7,
            self.comboBox,
            self.comboBox_2,
            self.comboBox_3,
            self.comboBox_4,
            self.comboBox_5,
            self.comboBox_6,
            self.label_t2,
            self.input_field,
            self.add_button,
            self.list_widget,
            self.delete_button,
            self.button_save,
            self.label_cam_titre,
            self.label_cam,
            self.button_info_cam,
            self.comboBox_cam,
            ]
        
        # Appel de la fonction pour ajuster les propriétés des widgets
        adjust_widget_properties(widgets_to_adjust, factor)
        
        #Afficher le Widget 
        self.new_widget.show()
    
    def create_infos_cam(self):
        """
        Crée une fenêtre pop-up pour afficher les informations pour guider l'utilisateur
        """
        self.new_widget_cam = QWidget()
        self.new_widget_cam.setGeometry(380, 700, 500, 250)
        
        #Créer les boutons
        self.button_return = QPushButton("OK", self.new_widget_cam)
        self.button_return.setGeometry(200, 190, 100, 50)
        self.button_return.setStyleSheet(u"QPushButton {border-radius: 10px; background-color: rgb(225, 225, 225);} QPushButton:hover {background: rgb(237, 255, 255);}")
        font = self.button_return.font()
        font.setPointSize(15)
        self.button_return.setFont(font)
        #Créer les Labels
        self.label_cam = QLabel(self.new_widget_cam)
        self.label_cam.setObjectName(u"label")
        self.label_cam.setGeometry(QRect(20, 0, 440, 200))
        self.label_cam.setWordWrap(True)
        font = self.label_cam.font()
        font.setPointSize(12)
        self.label_cam.setFont(font)
        self.label_cam.setAlignment(Qt.AlignCenter)
        
        #Assigner les textes à chaque élément
        self.label_cam.setText("Avec ce menu déroulant, vous pouvez sélectionner la caméra que vous souhaitez. \n Commencez par l'indice 0 puis augmentez la valeur si besoin pour changer de caméra."
                               " \n \n Si vous lancez une série d'exercice et que la vidéo de démonstration semble Freeze, C'est que l'indice de la caméra est trop élevé.")
        #Connecter le bouton
        self.button_return.clicked.connect(self.close_infos_cam_widget)
        
        # Liste des widgets à traiter
        widgets_to_adjust = [
            self.new_widget_cam,
            self.button_return,
            self.label_cam,
            ]
        
        # Appel de la fonction pour ajuster les propriétés des widgets
        adjust_widget_properties(widgets_to_adjust, factor)
                
        #Afficher le Widget 
        self.new_widget_cam.show()
    
    def create_infos(self):
        """
        Crée une fenêtre pop-up pour afficher les informations pour guider l'utilisateur
        """
        self.new_widget = QWidget()
        self.new_widget.setGeometry(110, 30, 970, 1010)
        
        #Créer les boutons
        self.button_return = QPushButton("Retour", self.new_widget)
        self.button_return.setGeometry(30, 20, 150, 40)
        self.button_return.setStyleSheet(u"QPushButton {border-radius: 10px; background-color: rgb(225, 225, 225);} QPushButton:hover {background: rgb(237, 255, 255);}")
        font = self.button_return.font()
        font.setPointSize(15)
        self.button_return.setFont(font)
        
        #Créer les Labels
        self.label = QLabel(self.new_widget)
        self.label.setObjectName(u"label")
        self.label.setGeometry(QRect(30, 80, 901, 831))
        self.label.setWordWrap(True)
        font = self.label.font()
        font.setPointSize(12)
        self.label.setFont(font)
        
        #Assigner les textes à chaque élément
        self.label.setText("<html><head/><body><p>Cette page r\u00e9pertorie l\u2019utilit\u00e9 de chaque bouton de l\u2019application. <br/>Si vous rencontrez un probl\u00e8me avec Fitfuel, vous pouvez nous contacter par mail : </span><span style=\"; font-style:italic;\">fitfuel.officiel@gmail.com</span></p><p><br/></p><p><span style=\" text-decoration: underline;\">Page d'accueil :</span></p><p>- Le bouton param\u00e8tre, avec une ic\u00f4ne d\u2019engrenage, sert \u00e0 saisir ou modifier vos informations personnelles dans le but d'am\u00e9liorer les recommandations de Fitfuel. (Aucune donnée n'est collectée. L'ensemble des données est stocké localement).</span></p><p><br/></p><p><span style=\" text-decoration: underline;\">Nutrition :</span></p><p><span style=\"font-weight:700;\">Section Programmes :</span></p><p>- Les boutons fl\u00e9ch\u00e9s permettent"
                        " de naviguer entre les 7 jours de la semaine. </span></p><p>- Les 4 boutons repas (petit d\u00e9jeuner, d\u00e9jeuner, go\u00fbter et d\u00eener) affichent toutes les informations du plat s\u00e9lectionn\u00e9 (le temps de pr\u00e9paration, le temps de cuisson, la recette, etc.).</span></p><p><span style=\" font-weight:700;\">Section Recette : </span></p><p>- Les 4 boutons avec des photos de plats, permettent d'ouvrir une page dans votre navigateur la recette du plat s\u00e9lectionn\u00e9.</span></p><p><br/></p><p><span style=\" text-decoration: underline;\">Sport :</span></p><p><span style=\" font-weight:700;\">Section Mon programme :</span></p><p>- Les 6 boutons permettent chacun de lancer une série d'exercice composée par l'algorithme de Fitfuel.</span></p><p><span style=\" font-weight:700;\">Section Tous les programmes :</span></p><p>"
                        "- Les 6 boutons avec des images permettent d'afficher l'ensemble des programmes disponibles dans notre application.</span></p></body></html>")
        
        #Connecter le bouton
        self.button_return.clicked.connect(self.close_new_widget)
        
        # Liste des widgets à traiter
        widgets_to_adjust = [
            self.new_widget,
            self.button_return,
            self.label
            ]
        
        # Appel de la fonction pour ajuster les propriétés des widgets
        adjust_widget_properties(widgets_to_adjust, factor)
                
        #Afficher le Widget 
        self.new_widget.show()

    def add_item(self):
        """
        Créer un nouvel item portant le nom de l'ingrédient que l'utilisateur a saisi
        """
        # Récupérer le texte saisi
        item_text = self.input_field.text()

        # Ajouter l'élément à la liste et à la vue
        self.elements.append(item_text)
        self.list_widget.addItem(item_text)

        # Effacer la zone de saisie
        self.input_field.clear()

    def delete_item(self):
        """
        Supprimer l'item sélectionner par l'utilisateur. 
        (Chaque item correspond à un ingrédient)
        """
        # Récupérer l'élément sélectionné dans la liste
        selected_item = self.list_widget.currentItem()

        if selected_item:
            # Supprimer l'élément de la liste et de la vue
            item_text = selected_item.text()
            self.elements.remove(item_text)
            self.list_widget.takeItem(self.list_widget.row(selected_item))

    def lire_save_json(self):
        """
        Si le fichier JSON existe, lit les données et les retourne

        Returns:
            tuple: la liste des ingrédients que l'utilisateur n'aime pas
        """

        if os.path.exists('save.json'):
            with open('save.json', 'r') as f:
                return json.load(f)
        else:
            return None

    def close_new_widget(self):
        """
        Ferme la fenêtre pop-up des paramètres
        """        
        self.new_widget.close()
        self.new_widget_cam.close()
        
    def close_infos_cam_widget(self):
        """
        Ferme la fenêtre pop-up d'informations sur la caméra
        """        
        self.new_widget_cam.close()
    
    def associer_variable_algo(self):
        """
        Sauvegarde toutes les donnés saisies par l'utilisateur dans un fichier JSON
        le but étant de conserver les choix de l'utilisateur entre deux utilisations
        """
        ### SAUVEGARDER LES INFOS PERSONNELLES ET LA CAMERA CHOISIE###
        # Créer un tuple pour exploitable par l'algorithme qui choisit/adapte les recettes
        taille = int(self.comboBox.currentText())
        age = int(self.comboBox_2.currentText())
        sexe = self.comboBox_3.currentText()
        objectifs = self.comboBox_4.currentText()
        niveaux = self.comboBox_5.currentText()
        poids = int(self.comboBox_6.currentText())
        camera = int(self.comboBox_cam.currentText())
        if objectifs == "PERTE DE MASSE":
            objectif="perte"
        else:
            objectif="prise"
            
        if niveaux == "NIVEAU 1 (pratique du sport moins d'une fois par semaine)":
            niveau=1
        elif niveaux ==  "NIVEAU 2 (pratique du port une a deux par semaine)":
            niveau=2
        else :
            niveau=3
        self.element_pour_multiplicateur_nouriture = [taille,age,sexe,objectif,niveau,poids]
        
        # Si le fichier JSON existe, lit les données
        if os.path.exists('parametres.json'):
            with open('parametres.json', 'r') as f:
                parametres_json = json.load(f)
        else:
            parametres_json = None

        # Si les paramètres existent déjà dans le fichier JSON, les met à jour avec les nouvelles valeurs
        if parametres_json:
            self.element_pour_multiplicateur_nouriture = parametres_json
            ancien_objectif = self.element_pour_multiplicateur_nouriture[3]
            self.element_pour_multiplicateur_nouriture = [taille, age, sexe, objectif, niveau, poids,ancien_objectif,camera]
        # Sinon, crée le tuple avec les paramètres fournis
        else:
            self.element_pour_multiplicateur_nouriture = [taille, age, sexe, objectif, niveau, poids, objectif, camera]
        
        # Écrit le tuple dans le fichier JSON
        with open('parametres.json', 'w') as f:
            json.dump(self.element_pour_multiplicateur_nouriture, f)
            
        ### SAUVEGARDER LES INGREDIENTS INDESIRES ###
        
        liste_ingredients = tuple(self.elements)
        # Si le fichier JSON existe, lit les données
        if os.path.exists('save.json'):
            with open('save.json', 'r') as f:
                save_json = json.load(f)
        else:
            save_json = None

        # Si les paramètres existent déjà dans le fichier JSON, les met à jour avec les nouvelles valeurs
        if save_json:
            ingredients_ban = save_json
            ingredients_ban = liste_ingredients
        # Sinon, crée le tuple avec les paramètres fournis
        else:
            ingredients_ban = liste_ingredients
        
        # Écrit le tuple dans le fichier JSON
        with open('save.json', 'w') as f:
            json.dump(ingredients_ban, f)
        #Fermer le Widget
        self.new_widget.close()
     
    def allerpagedapres(self):
        """
        Change la valeur de l'indice de la page affichée pour afficher la page Recette 
        (à chaque page est attribué un numéro,  pour faciliter le déplacement entre celles-ci) 
        """
        parametres=lire_json("parametres.json")
        ancien_objetif =parametres[6]
        objectif = parametres[3]
        if parametres == [1.7, 30, "FEMME", "prise", 1, 70, "prise",0]:
            self.create_infos_parmètre()
        else :            
            if ancien_objetif != objectif : #Inutile d'actualiser les recommandations de recettes si l'utilisateur ne change pas d'objectif
                page_recette = Recette(changeur_de_page)
                changeur_de_page.addWidget(page_recette)
                self.nombre_page_recette +=1
                changeur_de_page.setCurrentIndex(self.nombre_page_recette + 1)
            elif self.nombre_page_recette == 1: # S'il ne change jamais d'objectif (self.nombre_page_recette est alors égal à 1 ) il est inutile de changer de page
                changeur_de_page.setCurrentIndex(1)
            else:
                changeur_de_page.setCurrentIndex(self.nombre_page_recette+1) # Derniere option, il a modifie ses objectifs, donc on change de page
            
    def allerpagesport(self):
        """
        Change la valeur de l'indice pour afficher la page Sport
        (à chaque page est attribué un numéro,  pour faciliter le déplacement entre celles-ci)
        """
        parametres=lire_json("parametres.json")
        if parametres == [1.7, 30, "FEMME", "prise", 1, 70, "prise",0]:
            self.create_infos_parmètre()
        else :
            changeur_de_page.setCurrentIndex(2)

if __name__ == "__main__":
    app = QApplication(sys.argv)
    changeur_de_page = QStackedWidget()
    page_acceuil = Ui_MainWindow()
    page_recette = Recette(changeur_de_page)
    page_sport = Sport(changeur_de_page)
    changeur_de_page.addWidget(page_acceuil)
    changeur_de_page.addWidget(page_recette)
    changeur_de_page.addWidget(page_sport)

    changeur_de_page.showMaximized()
    sys.exit(app.exec())